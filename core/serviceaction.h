/*
    This file is part of the KDE project.
    Copyright (c) 2006-2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#ifndef LEUTE_SERVICEACTION_H
#define LEUTE_SERVICEACTION_H

// lib
#include "leutecore_export.h"
#include "roles.h"
#include "serviceactionadapter.h"
// KDE
#include <ksharedptr.h>
// Qt
#include <QtCore/QVariant>

namespace Leute
{

/**
 * Comfort class for easy handling of actions.
 */
class LEUTECORE_EXPORT ServiceAction
{
public:
    ServiceAction( ServiceActionAdapter* adapter );

public:
    QVariant data( int role = DisplayTextRole ) const;

protected:
    KSharedPtr<ServiceActionAdapter> mAdapter;
};


inline ServiceAction::ServiceAction( ServiceActionAdapter* adapter ) : mAdapter( adapter ) {}
inline QVariant ServiceAction::data( int role ) const { return mAdapter->data( role ); }

}

#endif
