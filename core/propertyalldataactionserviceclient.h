/*
    This file is part of the Leute Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#ifndef LEUTE_PROPERTYALLDATAACTIONSERVICECLIENT_H
#define LEUTE_PROPERTYALLDATAACTIONSERVICECLIENT_H

// lib
#include "leutecore_export.h"
#include "propertydataactionserviceclient.h"

namespace Leute
{

class LEUTECORE_EXPORT PropertyAllDataActionServiceClient : public PropertyDataActionServiceClient
{
public:
    PropertyAllDataActionServiceClient();
    virtual ~PropertyAllDataActionServiceClient();

public:
    bool serviceAvailableForData() const;

public: // interface
    /** */
    virtual int itemIndex() const = 0;
    virtual QString propertyId() const = 0;

public: // slots interface
    /** called if the default switched */
    virtual void onAllDataActionServiceChange() = 0;
};


inline PropertyAllDataActionServiceClient::PropertyAllDataActionServiceClient() {}
inline PropertyAllDataActionServiceClient::~PropertyAllDataActionServiceClient() {}

}

#endif
