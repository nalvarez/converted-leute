/*
    This file is part of the KDE project.
    Copyright (c) 2006-2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#ifndef KIDPTRVECTOR_H
#define KIDPTRVECTOR_H

// Qt
#include <QtCore/QVector>
#include <QtCore/QString>
#include <QtCore/QStringList>


template<class T>
class KIdPtrVector : public QVector<T*>
{
public:
    KIdPtrVector() {}
    KIdPtrVector( const KIdPtrVector<T> &O ) : QVector<T*>( O ) {}
public:
    typename QVector<T*>::Iterator find( const QString &id );
    T *operator[]( const QString &id ) const;
    void setOrder( const QStringList &Ids );
    void remove( const QString &Id, bool Delete );
};


template<class T>
T *KIdPtrVector<T>::operator[]( const QString &id ) const
{
    T* result = 0;
    for( typename QVector<T*>::ConstIterator it = this->begin(); it != this->end(); ++it )
        if( (*it)->id() == id )
        {
            result = *it;
            break;
        }

    return result;
}

template<class T>
typename QVector<T*>::Iterator KIdPtrVector<T>::find( const QString &id )
{
    typename QVector<T*>::Iterator it = this->begin();
    for( ; it != this->end(); ++it )
        if( (*it)->id() == id )
            break;

    return it;
}

template<class T>
inline void KIdPtrVector<T>::setOrder( const QStringList &Ids )
{
    // insert in the order of Ids
    typename QVector<T*>::Iterator startIt = this->begin();
    typename QVector<T*>::Iterator endIt = this->end();
    for( QStringList::ConstIterator sit = Ids.begin(); sit != Ids.end(); ++sit )
    {
        // find in the left the matching id and swap to current place
        for( typename QVector<T*>::Iterator it = startIt; it != endIt; ++it )
            if( (*it)->id() == *sit )
            {
                if( it != startIt )
                    qSwap( *it, *startIt );
                ++startIt;
                break;
            }
    }
}

template<class T>
inline void KIdPtrVector<T>::remove( const QString &Id, bool Delete )
{
    // remove all?
    if( Id.isNull() )
    {
        if( Delete )
            for( typename QVector<T*>::Iterator it = this->begin(); it != this->end(); ++it )
                delete *it;
        this->clear();
    }
    else
    {
        typename QVector<T*>::Iterator it = this->find( Id );
        T *t = *it;

        this->erase( it );
        if( Delete )
            delete t;
    }
}

#endif
