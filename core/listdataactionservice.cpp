/*
    This file is part of the KDE project.
    Copyright (c) 2006-2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#include "listdataactionservice.h"


namespace Leute
{

ListDataActionService::ListDataActionService(QObject* parent, const QStringList& arguments )
  : DataActionService( parent, arguments )
{}


bool ListDataActionService::supports( const QMimeData* data,
                                      const KABC::AddresseeList& personList ) const
{
    Q_UNUSED( personList );
    return supports( data );
}

bool ListDataActionService::isAvailableFor( const QMimeData* data,
                                            const KABC::AddresseeList& personList ) const
{
    Q_UNUSED( personList );
    return isAvailableFor( data );
}


void ListDataActionService::registerClient( ListDataActionServiceClient* client ) { Q_UNUSED(client); }
void ListDataActionService::unregisterClient( ListDataActionServiceClient* client ) { Q_UNUSED(client); }

ListDataActionService::~ListDataActionService() {}

}
