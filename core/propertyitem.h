/*
    This file is part of the KDE project.
    Copyright (c) 2006-2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


#ifndef LEUTE_PROPERTYITEM_H
#define LEUTE_PROPERTYITEM_H


// qt specific
#include <qvariant.h>
// kde specific
#include <ksharedptr.h>
// lib specific
#include "roles.h"
#include "propertyitemadapter.h"


namespace Leute {

class KDE_EXPORT PropertyItem
{
public:
    PropertyItem( PropertyItemAdapter* Adapter );

public:
    QVariant data( int Role = DisplayTextRole ) const;

protected:
    KSharedPtr<PropertyItemAdapter> Adapter;
};


inline PropertyItem::PropertyItem( PropertyItemAdapter* A ) : Adapter( A ) {}
inline QVariant PropertyItem::data( int Role ) const { return Adapter->data( Role ); }

}

#endif
