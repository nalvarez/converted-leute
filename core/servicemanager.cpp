/*
    This file is part of the Leute Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

// lib
#include "listactionservice.h"
#include "listdataactionservice.h"
#include "servicemanager.h"
// Qt
#include <QtCore/QString>


namespace Leute
{

ServiceManager::ServiceManager()
{}

// const StatusServiceList &ServiceManager::statusServices()                const { return StatusServices; }
const ActionServiceList &ServiceManager::actionServices()          const { return ActionServices; }
const DataActionServiceList &ServiceManager::dataActionServices() const { return DataActionServices; }

void ServiceManager::registerClient( StatusServiceClient *Client )
{
    for( StatusServiceList::Iterator ServiceIt = StatusServices.begin(); ServiceIt != StatusServices.end(); ++ServiceIt )
        (*ServiceIt)->registerClient( Client );

    StatusServiceClients.append( Client );
}


void ServiceManager::unregisterClient( StatusServiceClient *Client )
{
    for( StatusServiceList::Iterator ServiceIt = StatusServices.begin(); ServiceIt != StatusServices.end(); ++ServiceIt )
        (*ServiceIt)->unregisterClient( Client );

    StatusServiceClients.removeAll( Client );
}

void ServiceManager::addStatusService( StatusService *StatusService )
{
    StatusServices.append( StatusService );

    for( StatusServiceClientList::Iterator ClientIt = StatusServiceClients.begin();
         ClientIt != StatusServiceClients.end(); ++ClientIt )
        StatusService->registerClient( *ClientIt );

    informStatusServiceClients();
}

void ServiceManager::removeStatusService( const QString &ServiceId, bool Delete )
{
    StatusServices.remove( ServiceId, Delete );

    informStatusServiceClients();
}

void ServiceManager::setStatusServiceSorting( const QStringList &ServiceIds )
{
    StatusServices.setOrder( ServiceIds );
}

void ServiceManager::informStatusServiceClients()
{
// TODO: 
    // inform clients
//    for( StatusServiceClientList::Iterator ClientIt = StatusServiceClients.begin();
//         ClientIt != StatusServiceClients.end(); ++ClientIt )
//        (*ClientIt)->onAllServiceChange();
}


void ServiceManager::execute( const KABC::Addressee &Person, const QString &ServiceId ) const
{
    ActionService *Service = ActionServices[ServiceId];

    if( Service )
        Service->execute( Person );
}
void ServiceManager::execute( const KABC::AddresseeList &List, const QString &ServiceId ) const
{
    ListActionService *Service = qobject_cast<ListActionService*>( ActionServices[ServiceId] );

    if( Service )
        Service->execute( List );
}


void ServiceManager::addActionService( ActionService *Service/*, bool AsDefault*/ )
{
    ActionServices.append( Service );
//     if( AsDefault ) DefaultActionServices[Service->id()] = Service;

    for( ActionServiceClientList::Iterator ClientIt = ActionServiceClients.begin();
         ClientIt != ActionServiceClients.end(); ++ClientIt )
        Service->registerClient( *ClientIt );

    ListActionService *ListService = qobject_cast<ListActionService*>( Service );
    if( ListService )
        for( ListActionServiceClientList::Iterator ClientIt = ListActionServiceClients.begin();
             ClientIt != ListActionServiceClients.end(); ++ClientIt )
            ListService->registerClient( *ClientIt );

    informActionServiceClients();
}


void ServiceManager::removeActionService( const QString &ServiceId, bool Delete )
{
//     bool DefaultChanged = false;

    ActionServices.remove( ServiceId, Delete );

//    if( DefaultChanged )
//        setDefaultService( -1 );
    informActionServiceClients();
}

void ServiceManager::setActionServiceSorting( const QStringList &ServiceIds )
{
    ActionServices.setOrder( ServiceIds );
}

void ServiceManager::informActionServiceClients()
{
#if 0
    // inform clients
    for( ActionServiceClientList::Iterator ClientIt = ActionServiceClients.begin();
         ClientIt != ActionServiceClients.end(); ++ClientIt )
        (*ClientIt)->onAllActionServiceChange();
#endif
}

void ServiceManager::registerClient( ActionServiceClient *Client )
{
    for( ActionServiceList::Iterator ServiceIt = ActionServices.begin(); ServiceIt != ActionServices.end(); ++ServiceIt )
        (*ServiceIt)->registerClient( Client );

    ActionServiceClients.append( Client );
}


void ServiceManager::unregisterClient( ActionServiceClient *Client )
{
    for( ActionServiceList::Iterator ServiceIt = ActionServices.begin(); ServiceIt != ActionServices.end(); ++ServiceIt )
        (*ServiceIt)->unregisterClient( Client );

    ActionServiceClients.removeAll( Client );
}

void ServiceManager::registerClient( ListActionServiceClient *Client )
{
    for( ActionServiceList::Iterator ServiceIt = ActionServices.begin(); ServiceIt != ActionServices.end(); ++ServiceIt )
    {
        ListActionService *ListService = qobject_cast<ListActionService*>( *ServiceIt );
        if( ListService )
            ListService->registerClient( Client );
    }

    ListActionServiceClients.append( Client );
}


void ServiceManager::unregisterClient( ListActionServiceClient *Client )
{
    for( ActionServiceList::Iterator ServiceIt = ActionServices.begin(); ServiceIt != ActionServices.end(); ++ServiceIt )
    {
        ListActionService *ListService = qobject_cast<ListActionService*>( *ServiceIt );
        if( ListService )
            ListService->unregisterClient( Client );
    }

    ListActionServiceClients.removeAll( Client );
}


/*
void ServiceManager::setDefaultService( int ServiceId )
{
    PropertyActionService *NewDefaultService = (ServiceId==-1) ? 0 : ActionServices[ServiceId];

    // no change?
    if( DefaultService == NewDefaultService )
        return;

    // unregister all defaultclients from old default service
    if( DefaultService )
        for( DefaultServiceClientList::Iterator ClientIt = AllPropertiesDefaultActionServiceClients.begin();
             ClientIt != AllPropertiesDefaultActionServiceClients.end(); ++ClientIt )
            DefaultService->unregisterClient( *ClientIt );

    // set new default service
    DefaultService = NewDefaultService;

    // register 
    if( DefaultService )
        for( DefaultServiceClientList::Iterator ClientIt = AllPropertiesDefaultActionServiceClients.begin();
             ClientIt != AllPropertiesDefaultActionServiceClients.end(); ++ClientIt )
            DefaultService->registerClient( *ClientIt );

    // inform clients
    for( DefaultServiceClientList::Iterator ClientIt = AllPropertiesDefaultActionServiceClients.begin();
         ClientIt != AllPropertiesDefaultActionServiceClients.end(); ++ClientIt )
        (*ClientIt)->onDefaultServiceSwitch( propertyId() );
}


int ServiceManager::defaultServiceId() const
{
    return ActionServices.findIndex( DefaultService );
}
*/
#if 0

void ServiceManager::registerClient( AllPropertiesDefaultActionServiceClient *Client )
{
    for( ActionServiceList::Iterator ServiceIt = DefaultActionServices.begin(); ServiceIt != DefaultActionServices.end(); ++ServiceIt )
        (*ServiceIt)->registerClient( Client );

    AllPropertiesDefaultActionServiceClients.append( Client );
}


void ServiceManager::unregisterClient( AllPropertiesDefaultActionServiceClient *Client )
{
    for( ActionServiceList::Iterator ServiceIt = DefaultActionServices.begin(); ServiceIt != DefaultActionServices.end(); ++ServiceIt )
        (*ServiceIt)->unregisterClient( Client );

    AllPropertiesDefaultActionServiceClients.remove( Client );
}
#endif

void ServiceManager::addDataActionService( DataActionService *Service/*, bool AsDefault*/ )
{
    DataActionServices.append( Service );
//     if( AsDefault ) DefaultDataActionServices[Service->id()] = Service;

    for( DataActionServiceClientList::Iterator ClientIt = DataActionServiceClients.begin();
         ClientIt != DataActionServiceClients.end(); ++ClientIt )
        Service->registerClient( *ClientIt );

    ListDataActionService *ListService = qobject_cast<ListDataActionService*>( Service );
    if( ListService )
        for( ListDataActionServiceClientList::Iterator ClientIt = ListDataActionServiceClients.begin();
             ClientIt != ListDataActionServiceClients.end(); ++ClientIt )
            ListService->registerClient( *ClientIt );

    informDataActionServiceClients();
}


void ServiceManager::removeDataActionService( const QString &ServiceId, bool Delete )
{
//     bool DefaultChanged = false;

    DataActionServices.remove( ServiceId, Delete );

//     if( DefaultChanged )
//         setDefaultDropService( -1 );

    informDataActionServiceClients();
}


void ServiceManager::informDataActionServiceClients()
{
    // inform clients
//     for( PropertyAllDataActionServiceClientList::Iterator ClientIt = PropertyAllDataActionServiceClients.begin();
//          ClientIt != PropertyAllDataActionServiceClients.end(); ++ClientIt )
//         (*ClientIt)->onAllDataActionServiceChange();
}


void ServiceManager::setDataActionServiceSorting( const QStringList &ServiceIds )
{
    DataActionServices.setOrder( ServiceIds );
}

void ServiceManager::registerClient( DataActionServiceClient *Client )
{
    for( DataActionServiceList::Iterator ServiceIt = DataActionServices.begin();
         ServiceIt != DataActionServices.end(); ++ServiceIt )
        (*ServiceIt)->registerClient( Client );

    DataActionServiceClients.append( Client );
}


void ServiceManager::unregisterClient( DataActionServiceClient *Client )
{
    for( DataActionServiceList::Iterator ServiceIt = DataActionServices.begin();
         ServiceIt != DataActionServices.end(); ++ServiceIt )
        (*ServiceIt)->unregisterClient( Client );

    DataActionServiceClients.removeAll( Client );
}

void ServiceManager::registerClient( ListDataActionServiceClient *Client )
{
    for( DataActionServiceList::Iterator ServiceIt = DataActionServices.begin();
         ServiceIt != DataActionServices.end(); ++ServiceIt )
    {
        ListDataActionService *ListService = qobject_cast<ListDataActionService*>( *ServiceIt );
        if( ListService )
            ListService->registerClient( Client );
    }

    ListDataActionServiceClients.append( Client );
}


void ServiceManager::unregisterClient( ListDataActionServiceClient *Client )
{
    for( DataActionServiceList::Iterator ServiceIt = DataActionServices.begin();
         ServiceIt != DataActionServices.end(); ++ServiceIt )
    {
        ListDataActionService *ListService = qobject_cast<ListDataActionService*>( *ServiceIt );
        if( ListService )
            ListService->unregisterClient( Client );
    }

    ListDataActionServiceClients.removeAll( Client );
}

/*
void ServiceManager::setDefaultDropService( int DropServiceId )
{
    PropertyDataActionService *NewDefaultDropService = (DropServiceId==-1) ? 0 : DataActionServices[DropServiceId];

    // no change?
    if( DefaultDropService == NewDefaultDropService )
        return;

    // unregister all defaultclients from old default service
    if( DefaultDropService )
        for( DefaultDropServiceClientList::Iterator ClientIt = AllPropertiesDefaultDataActionServices.begin();
             ClientIt != AllPropertiesDefaultDataActionServices.end(); ++ClientIt )
            DefaultDropService->unregisterDropClient( *ClientIt );

    // set new default service
    DefaultDropService = NewDefaultDropService;

    // register 
    if( DefaultDropService )
        for( DefaultDropServiceClientList::Iterator ClientIt = AllPropertiesDefaultDataActionServices.begin();
             ClientIt != AllPropertiesDefaultDataActionServices.end(); ++ClientIt )
            DefaultDropService->registerDropClient( *ClientIt );

    // inform clients
    for( DefaultDropServiceClientList::Iterator ClientIt = AllPropertiesDefaultDataActionServices.begin();
         ClientIt != AllPropertiesDefaultDataActionServices.end(); ++ClientIt )
        (*ClientIt)->onDefaultDropServiceSwitch( propertyId() );
}
*/

void ServiceManager::execute( const KABC::Addressee &Person, const QMimeData* data,
                              const QString &ServiceId ) const
{
    DataActionService *Service = DataActionServices[ServiceId];

    if( Service )
        Service->execute( Person, data );
}

void ServiceManager::execute( const KABC::AddresseeList &List, const QMimeData* data,
                              const QString &ServiceId ) const
{
    ListDataActionService *Service = qobject_cast<ListDataActionService*>( DataActionServices[ServiceId] );

    if( Service )
        Service->execute( List, data );
}

enum { ActionServiceId = 0, DataActionServiceId = 1, StatusServiceId = 2 };

void ServiceManager::reloadConfig( int ServiceType, const QString &ServiceId )
{
    Service *Service = 0;
    switch( ServiceType )
    {
        case ActionServiceId: 
            Service = ActionServices[ServiceId];
            break;
        case DataActionServiceId:
            Service = DataActionServices[ServiceId];
            break;
        case StatusServiceId: 
            Service = StatusServices[ServiceId];
    }
    if( Service )
        Service->reloadConfig();
}

ServiceManager::~ServiceManager()
{
    removeActionService( QString::null, true );
    removeDataActionService( QString::null, true );
//     removeStatusService( QString::null, true );
}

}
