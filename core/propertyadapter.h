/*
    This file is part of the Leute Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#ifndef LEUTE_PROPERTYADAPTER_H
#define LEUTE_PROPERTYADAPTER_H

// lib
#include "leutecore_export.h"
#include "propertyitem.h"
// Qt
#include <QtCore/QObject>

class QString;
namespace KABC {
class Addressee;
class AddresseeList;
}


namespace Leute
{

class LEUTECORE_EXPORT PropertyAdapter : public QObject
{
    Q_OBJECT
protected:
    PropertyAdapter( QObject* parent, const QStringList& arguments );
public:
    virtual ~PropertyAdapter();
public:
    const QString& id() const;

public:
    // if some TODO: or better only all?
    virtual bool haveProperty( const KABC::AddresseeList& personList ) const;

    /** returns  */
    virtual int numberOfItems( const KABC::Addressee& person ) const = 0;

    virtual PropertyItem propertyItemOf( const KABC::Addressee& person, int itemIndex = 0 ) const = 0;

private:
    const QString Id;
};

}

#endif
