/*
    This file is part of the Leute Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


#ifndef LEUTE_SERVICE_P_H
#define LEUTE_SERVICE_P_H

#include "service.h"
// Qt
#include <QtCore/QStringList>
// #include <QtCore/QString>


namespace Leute
{

class ServicePrivate
{
public:
    ServicePrivate( const QStringList& arguments );

public:
    bool fitsIn( const QString& context ) const;
    const QString& id() const;

protected:
    QString mId;
    QStringList mOnlyShowInContext;
    QStringList mNotShowInContext;
};


inline ServicePrivate::ServicePrivate( const QStringList& arguments )
{
    const int argumentsCount = arguments.size();

    if( argumentsCount > 0 )
    {
        mId = arguments[0];
    if( argumentsCount > 1 )
    {
       mOnlyShowInContext = arguments[1].split( ';', QString::SkipEmptyParts );
    if( argumentsCount > 2 )
        mNotShowInContext = arguments[2].split( ';', QString::SkipEmptyParts );
    }}
}

inline bool ServicePrivate::fitsIn( const QString& context ) const
{
    return !mNotShowInContext.contains(context) &&
           (mOnlyShowInContext.isEmpty() || mOnlyShowInContext.contains(context));
}

inline const QString& ServicePrivate::id() const { return mId; }

}

#endif
