/*
    This file is part of the Leute Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#ifndef LEUTE_PROPERTYMANAGER_H
#define LEUTE_PROPERTYMANAGER_H

// lib
#include "kidptrvector.h"
#include "propertyitem.h"
#include "propertyitemadapter.h"
#include "propertystatusservice.h"
#include "propertyactionservice.h"
#include "propertydataactionservice.h"
#include "propertystatusserviceclient.h"
#include "propertyallactionserviceclient.h"
#include "allpropertiesglobalactionserviceclient.h"
#include "listallpropertiesglobalactionserviceclient.h"
#include "propertyalldataactionserviceclient.h"
#include "allpropertiesglobaldataactionserviceclient.h"
#include "listallpropertiesglobaldataactionserviceclient.h"
// Qt
#include <QtCore/QList>
#include <QtCore/QString>

namespace KABC {
class Addressee;
class AddresseeList;
}
class QMimeData;


namespace Leute
{

class PropertyAdapter;

typedef KIdPtrVector<PropertyStatusService> PropertyStatusServiceList;
typedef KIdPtrVector<PropertyActionService> PropertyActionServiceList;
typedef KIdPtrVector<PropertyDataActionService> PropertyDataActionServiceList;
typedef QList<PropertyAllActionServiceClient*> PropertyAllActionServiceClientList;
typedef QList<PropertyAllDataActionServiceClient*> PropertyAllDataActionServiceClientList;


class LEUTECORE_EXPORT PropertyManager
{
    friend class Services;
    friend class ServicesPrivate;
public:
    PropertyManager( PropertyAdapter *PropertyAdapter );
    ~PropertyManager();

public: // status interface
    void registerClient( PropertyStatusServiceClient *Client );
    void unregisterClient( PropertyStatusServiceClient *Client );

    const PropertyStatusServiceList &statusServices() const;

public: // action service interface
    void registerClient( PropertyAllActionServiceClient *Client );
    void unregisterClient( PropertyAllActionServiceClient *Client );

    void registerClient( AllPropertiesGlobalActionServiceClient *Client );
    void unregisterClient( AllPropertiesGlobalActionServiceClient *Client );

    void registerClient( ListAllPropertiesGlobalActionServiceClient *Client );
    void unregisterClient( ListAllPropertiesGlobalActionServiceClient *Client );

    void execute( const KABC::Addressee &Person, int ItemIndex, const QString &ServiceId ) const;
    void execute( const KABC::AddresseeList &List, const QString &ServiceId ) const;

    const PropertyActionServiceList &actionServices() const;
    const PropertyActionServiceList &mainActionServices() const;

public: // data action service interface
    void registerClient( PropertyAllDataActionServiceClient *Client );
    void unregisterClient( PropertyAllDataActionServiceClient *Client );

    void registerClient( AllPropertiesGlobalDataActionServiceClient *Client );
    void unregisterClient( AllPropertiesGlobalDataActionServiceClient *Client );

    void registerClient( ListAllPropertiesGlobalDataActionServiceClient *Client );
    void unregisterClient( ListAllPropertiesGlobalDataActionServiceClient *Client );

    void execute( const KABC::Addressee &Person, int ItemIndex, const QMimeData* data, const QString &ServiceId ) const;
    void execute( const KABC::AddresseeList& List, const QMimeData* data, const QString &ServiceId ) const;

    const PropertyDataActionServiceList &dataActionServices() const;
    const PropertyDataActionServiceList &mainDataActionServices() const;

public: // status service interface
    const PropertyAdapter *propertyAdapter() const;
    QString id() const;

public: // management interface
    void addStatusService( PropertyStatusService *StatusService  );
    void removeStatusService( const QString &StatusServiceId, bool Delete = false );

    void addActionService( PropertyActionService* S, bool AsMain ); // TODO: addActionServices(), whole list in one step?
    // TODO: does ServiceId do it? Better pointer to the service?
     /** deletes the service with the id 
     * @param ServiceId the id of the service. If -1 all services are deleted
     * @param Delete If true also delete the removed service(s)
     */
   void removeActionService( const QString &ServiceId, bool Delete = false );

    void addDataActionService( PropertyDataActionService* S, bool AsMain );
    void removeDataActionService( const QString &ServiceId, bool Delete = false );

protected:
    void setActionServiceSorting( const QStringList &ServiceIds );
    void setDataActionServiceSorting( const QStringList &ServiceIds );
    void setStatusServiceSorting( const QStringList &ServiceIds );

    void reloadConfig( int ServiceType, const QString &ServiceId );


protected:
    void informStatusServiceClients();
    void informPropertyAllActionServiceClients(); // TODO: move to ClientForItemList
    void informPropertyAllDataActionServiceClients();

protected:
    /** the adapter for the supported property */
    PropertyAdapter *Adapter;

    /** */
    PropertyStatusServiceList StatusServices;
    /** list of all clients interested in status services */
    PropertyStatusServiceClientList StatusServiceClients;

    /** */
    PropertyActionServiceList MainActionServices;
    PropertyActionServiceList ActionServices;
    /** */
    AllPropertiesGlobalActionServiceClientList AllPropertiesGlobalActionServiceClients;
    /** */
    PropertyAllActionServiceClientList PropertyAllActionServiceClients;
    ListAllPropertiesGlobalActionServiceClientList ListAllPropertiesGlobalActionServiceClients;
    /** */
    PropertyDataActionServiceList MainDataActionServices;
    PropertyDataActionServiceList DataActionServices;

    /** */
    AllPropertiesGlobalDataActionServiceClientList AllPropertiesGlobalDataActionServiceClients;
    ListAllPropertiesGlobalDataActionServiceClientList ListAllPropertiesGlobalDataActionServiceClients;
    /** */
    PropertyAllDataActionServiceClientList PropertyAllDataActionServiceClients;
};

}

#endif
