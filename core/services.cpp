/*
    This file is part of the Leute Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#include "services.h"
#include "services_p.h"

// lib
#include "propertyallactionserviceclient.h"
#include "allpropertiesglobalactionserviceclient.h"
// KDE
#include <KGlobal>


namespace Leute
{

class ServicesSingleton
{
  public:
    Services self;
};

K_GLOBAL_STATIC( ServicesSingleton, servicesSelf )


Services* Services::self()
{
    return &servicesSelf->self;
}


Services::Services()
: d( new ServicesPrivate(this) )
{
    d->load();
}

const ServiceManager &Services::serviceManager() const
{ return d->serviceManager(); }

const PropertyManagerList &Services::propertyManagers() const
{ return d->PropertyManagers; }


void Services::registerClient( StatusServiceClient* client )
{
    d->serviceManager().registerClient( client );
}

void Services::unregisterClient( StatusServiceClient* client )
{
    d->serviceManager().unregisterClient( client );
}

void Services::registerClient( ActionServiceClient* client )
{
    d->serviceManager().registerClient( client );
}

void Services::unregisterClient( ActionServiceClient* client )
{
    d->serviceManager().unregisterClient( client );
}

void Services::registerClient( ListActionServiceClient* client )
{
    d->serviceManager().registerClient( client );
}

void Services::unregisterClient( ListActionServiceClient* client )
{
    d->serviceManager().unregisterClient( client );
}

void Services::execute( const KABC::Addressee &Person, const QString &serviceId )
{
    d->serviceManager().execute( Person, serviceId );
}
void Services::execute( const KABC::AddresseeList &personList, const QString &serviceId )
{
    d->serviceManager().execute( personList, serviceId );
}


void Services::registerClient( DataActionServiceClient* client )
{
    d->serviceManager().registerClient( client );
}

void Services::unregisterClient( DataActionServiceClient* client )
{
    d->serviceManager().unregisterClient( client );
}
void Services::registerClient( ListDataActionServiceClient* client )
{
    d->serviceManager().registerClient( client );
}

void Services::unregisterClient( ListDataActionServiceClient* client )
{
    d->serviceManager().unregisterClient( client );
}

void Services::execute( const KABC::Addressee& person,
                                 const QString& serviceId, const QMimeData* data )
{
    d->serviceManager().execute( person, data, serviceId );
}
void Services::execute( const KABC::AddresseeList &personList,
                                 const QString &serviceId, const QMimeData* data )
{
    d->serviceManager().execute( personList, data, serviceId );
}


void Services::registerClient( PropertyStatusServiceClient* client )
{
    for( PropertyManagerList::Iterator ManagerIt = d->PropertyManagers.begin();
         ManagerIt != d->PropertyManagers.end(); ++ManagerIt )
        (*ManagerIt)->registerClient( client );

    d->StatusServiceClients.append( client );
}

void Services::unregisterClient( PropertyStatusServiceClient* client )
{
    for( PropertyManagerList::Iterator ManagerIt = d->PropertyManagers.begin();
         ManagerIt != d->PropertyManagers.end(); ++ManagerIt )
        (*ManagerIt)->unregisterClient( client );
	
    d->StatusServiceClients.removeOne( client );
}

void Services::registerClient( AllPropertiesGlobalActionServiceClient* client )
{
    for( PropertyManagerList::Iterator ManagerIt = d->PropertyManagers.begin();
         ManagerIt != d->PropertyManagers.end(); ++ManagerIt )
        (*ManagerIt)->registerClient( client );

    d->GlobalActionServiceClients.append( client );
}

void Services::unregisterClient( AllPropertiesGlobalActionServiceClient* client )
{
    for( PropertyManagerList::Iterator ManagerIt = d->PropertyManagers.begin();
         ManagerIt != d->PropertyManagers.end(); ++ManagerIt )
        (*ManagerIt)->unregisterClient( client );
	
    d->GlobalActionServiceClients.removeOne( client );
}

void Services::registerClient( ListAllPropertiesGlobalActionServiceClient* client )
{
    for( PropertyManagerList::Iterator ManagerIt = d->PropertyManagers.begin();
         ManagerIt != d->PropertyManagers.end(); ++ManagerIt )
        (*ManagerIt)->registerClient( client );

    d->ListGlobalActionServiceClients.append( client );
}

void Services::unregisterClient( ListAllPropertiesGlobalActionServiceClient* client )
{
    for( PropertyManagerList::Iterator ManagerIt = d->PropertyManagers.begin();
         ManagerIt != d->PropertyManagers.end(); ++ManagerIt )
        (*ManagerIt)->unregisterClient( client );

    d->ListGlobalActionServiceClients.removeOne( client );
}


void Services::registerClient( PropertyAllActionServiceClient* client )
{
    PropertyManager *Manager = d->PropertyManagers[client->propertyId()];

    if( Manager )
        Manager->registerClient( client );
}

void Services::unregisterClient( PropertyAllActionServiceClient* client )
{
    PropertyManager *Manager = d->PropertyManagers[client->propertyId()];

    if( Manager )
        Manager->unregisterClient( client );
}

void Services::execute( const KABC::Addressee &Person, const QString &PropertyId, int ItemIndex,
                                 const QString &serviceId )
{
    PropertyManager *Manager = d->PropertyManagers[PropertyId];

    if( Manager )
        Manager->execute( Person, ItemIndex, serviceId );
}
void Services::execute( const KABC::AddresseeList &personList, const QString &PropertyId,
                                 const QString &serviceId )
{
    PropertyManager *Manager = d->PropertyManagers[PropertyId];

    if( Manager )
        Manager->execute( personList, serviceId );
}


void Services::registerClient( AllPropertiesGlobalDataActionServiceClient* client )
{
    for( PropertyManagerList::Iterator ManagerIt = d->PropertyManagers.begin();
         ManagerIt != d->PropertyManagers.end(); ++ManagerIt )
        (*ManagerIt)->registerClient( client );

    d->GlobalDataActionServiceClients.append( client );
}

void Services::unregisterClient( AllPropertiesGlobalDataActionServiceClient* client )
{
    for( PropertyManagerList::Iterator ManagerIt = d->PropertyManagers.begin();
         ManagerIt != d->PropertyManagers.end(); ++ManagerIt )
        (*ManagerIt)->unregisterClient( client );

    d->GlobalDataActionServiceClients.removeOne( client );
}

void Services::registerClient( ListAllPropertiesGlobalDataActionServiceClient* client )
{
    for( PropertyManagerList::Iterator ManagerIt = d->PropertyManagers.begin();
         ManagerIt != d->PropertyManagers.end(); ++ManagerIt )
        (*ManagerIt)->registerClient( client );

    d->ListGlobalDataActionServiceClients.append( client );
}

void Services::unregisterClient( ListAllPropertiesGlobalDataActionServiceClient* client )
{
    for( PropertyManagerList::Iterator ManagerIt = d->PropertyManagers.begin();
         ManagerIt != d->PropertyManagers.end(); ++ManagerIt )
        (*ManagerIt)->unregisterClient( client );

    d->ListGlobalDataActionServiceClients.removeOne( client );
}


void Services::registerClient( PropertyAllDataActionServiceClient* client )
{
    PropertyManager *Manager = d->PropertyManagers[client->propertyId()];

    if( Manager )
        Manager->registerClient( client );
}

void Services::unregisterClient( PropertyAllDataActionServiceClient* client )
{
    PropertyManager *Manager = d->PropertyManagers[client->propertyId()];

    if( Manager )
        Manager->unregisterClient( client );
}


void Services::execute( const KABC::Addressee &Person, const QString &PropertyId, int ItemIndex,
                                 const QString &serviceId, const QMimeData* data )
{
    PropertyManager *Manager = d->PropertyManagers[PropertyId];

    if( Manager )
        Manager->execute( Person, ItemIndex, data, serviceId );
}

void Services::execute( const KABC::AddresseeList &personList, const QString &PropertyId,
                                 const QString &serviceId, const QMimeData* data )
{
    PropertyManager *Manager = d->PropertyManagers[PropertyId];

    if( Manager )
        Manager->execute( personList, data, serviceId );
}

Services::~Services() {}

}
