/*
    This file is part of the Leute Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#include "statusservice.h"


namespace Leute
{

StatusService::StatusService( QObject* parent, const QStringList& arguments )
  : Service( parent, arguments )
{}


bool StatusService::isAvailable() const
{
    return true;
}


bool StatusService::isAvailableFor( const KABC::Addressee& person ) const
{
    return supports( person ) && isAvailable();
}


bool StatusService::supports( const KABC::Addressee& person ) const
{
    Q_UNUSED( person );
    return true;
}

bool StatusService::fitsIn( const QString& context ) const
{
    Q_UNUSED( context );
    return true;
}

void StatusService::registerClient( StatusServiceClient* client ) { Q_UNUSED(client); }
void StatusService::unregisterClient( StatusServiceClient* client ) { Q_UNUSED(client); }

StatusService::~StatusService() {}

}

#include "statusservice.moc"
