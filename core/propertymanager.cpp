/*
    This file is part of the Leute Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#include "propertyadapter.h"

// lib
#include "listpropertyactionservice.h"
#include "listpropertydataactionservice.h"
#include "propertymanager.h"
// Qt
#include <QtCore/QString>


namespace Leute
{

PropertyManager::PropertyManager( PropertyAdapter* propertyAdapter )
  : Adapter( propertyAdapter )
{}

const PropertyAdapter* PropertyManager::propertyAdapter()       const { return Adapter; }
const PropertyStatusServiceList& PropertyManager::statusServices()             const { return StatusServices; }
const PropertyActionServiceList& PropertyManager::actionServices()             const { return ActionServices; }
const PropertyActionServiceList& PropertyManager::mainActionServices()         const { return MainActionServices; }
const PropertyDataActionServiceList& PropertyManager::dataActionServices()     const { return DataActionServices; }
const PropertyDataActionServiceList& PropertyManager::mainDataActionServices() const { return MainDataActionServices; }
QString PropertyManager::id() const { return Adapter->id(); }


void PropertyManager::registerClient( PropertyStatusServiceClient* client )
{
    //TODO: if( Adapter->isContained(Client->person()) )
    foreach( PropertyStatusService* propertyStatusService, StatusServices )
        propertyStatusService->registerClient( client );

    StatusServiceClients.append( client );
}


void PropertyManager::unregisterClient( PropertyStatusServiceClient* client )
{
    for( PropertyStatusServiceList::ConstIterator it = StatusServices.constBegin(); it != StatusServices.constEnd(); ++it )
        (*it)->unregisterClient( client );

    StatusServiceClients.removeAll( client );
}

void PropertyManager::addStatusService( PropertyStatusService* statusService )
{
    StatusServices.append( statusService );
    statusService->setAdapter( Adapter );

    for( PropertyStatusServiceClientList::ConstIterator it = StatusServiceClients.constBegin();
         it != StatusServiceClients.constEnd(); ++it )
        statusService->registerClient( *it );

    informStatusServiceClients();
}

void PropertyManager::removeStatusService( const QString& statusServiceId, bool doDelete )
{
    StatusServices.remove( statusServiceId, doDelete );

    informStatusServiceClients();
}

void PropertyManager::informStatusServiceClients()
{
// TODO: 
    // inform clients
//    for( PropertyStatusServiceClientList::Iterator ClientIt = StatusServiceClients.begin();
//         ClientIt != StatusServiceClients.end(); ++ClientIt )
//        (*ClientIt)->onAllServiceChange();
}



void PropertyManager::execute( const KABC::Addressee& person, int itemIndex, const QString& serviceId ) const
{
    PropertyActionService* service = ActionServices[serviceId];

    if( service )
        service->execute( person, itemIndex );
}

void PropertyManager::execute( const KABC::AddresseeList& personList, const QString& serviceId ) const
{
    ListPropertyActionService* service = qobject_cast<ListPropertyActionService*>( ActionServices[serviceId] );

    if( service )
        service->execute( personList );
}


void PropertyManager::addActionService( PropertyActionService* service, bool asMain )
{
    ActionServices.append( service );
    if( asMain ) MainActionServices.append( service );
    service->setAdapter( Adapter );

    for( PropertyAllActionServiceClientList::ConstIterator it = PropertyAllActionServiceClients.constBegin();
         it != PropertyAllActionServiceClients.constEnd(); ++it )
        service->registerClient( *it );

    informPropertyAllActionServiceClients();
}


void PropertyManager::removeActionService( const QString& serviceId, bool doDelete )
{
//     bool GlobalChanged = false;

    ActionServices.remove( serviceId, doDelete );

//    if( GlobalChanged )
//        setGlobalService( -1 );
    informPropertyAllActionServiceClients();
}

void PropertyManager::informPropertyAllActionServiceClients()
{
    // inform clients
    for( PropertyAllActionServiceClientList::ConstIterator it = PropertyAllActionServiceClients.constBegin();
         it != PropertyAllActionServiceClients.constEnd(); ++it )
        (*it)->onAllActionServiceChange();
}

void PropertyManager::registerClient( PropertyAllActionServiceClient* client )
{
    for( PropertyActionServiceList::ConstIterator it = ActionServices.constBegin(); it != ActionServices.constEnd(); ++it )
        (*it)->registerClient( client, client->itemIndex() );

    PropertyAllActionServiceClients.append( client );
}


void PropertyManager::unregisterClient( PropertyAllActionServiceClient* client )
{
    for( PropertyActionServiceList::ConstIterator it = ActionServices.constBegin(); it != ActionServices.constEnd(); ++it )
        (*it)->unregisterClient( client, client->itemIndex() );

    PropertyAllActionServiceClients.removeOne( client );
}


/*
void PropertyManager::setGlobalService( int ServiceId )
{
    PropertyActionService *NewGlobalService = (ServiceId==-1) ? 0 : ActionServices[ServiceId];

    // no change?
    if( GlobalService == NewGlobalService )
        return;

    // unregister all defaultclients from old default service
    if( GlobalService )
        for( GlobalServiceClientList::Iterator ClientIt = AllPropertiesGlobalActionServiceClients.begin();
             ClientIt != AllPropertiesGlobalActionServiceClients.end(); ++ClientIt )
            GlobalService->unregisterClient( *ClientIt );

    // set new default service
    GlobalService = NewGlobalService;

    // register 
    if( GlobalService )
        for( GlobalServiceClientList::Iterator ClientIt = AllPropertiesGlobalActionServiceClients.begin();
             ClientIt != AllPropertiesGlobalActionServiceClients.end(); ++ClientIt )
            GlobalService->registerClient( *ClientIt );

    // inform clients
    for( GlobalServiceClientList::Iterator ClientIt = AllPropertiesGlobalActionServiceClients.begin();
         ClientIt != AllPropertiesGlobalActionServiceClients.end(); ++ClientIt )
        (*ClientIt)->onGlobalServiceSwitch( propertyId() );
}


int PropertyManager::defaultServiceId() const
{
    return ActionServices.findIndex( GlobalService );
}
*/

void PropertyManager::registerClient( AllPropertiesGlobalActionServiceClient* client )
{
    for( PropertyActionServiceList::ConstIterator ServiceIt = MainActionServices.constBegin(); ServiceIt != MainActionServices.constEnd(); ++ServiceIt )
        (*ServiceIt)->registerClient( client );

    AllPropertiesGlobalActionServiceClients.append( client );
}


void PropertyManager::unregisterClient( AllPropertiesGlobalActionServiceClient* client )
{
    for( PropertyActionServiceList::ConstIterator ServiceIt = MainActionServices.constBegin(); ServiceIt != MainActionServices.constEnd(); ++ServiceIt )
        (*ServiceIt)->unregisterClient( client );

    AllPropertiesGlobalActionServiceClients.removeOne( client );
}

void PropertyManager::registerClient( ListAllPropertiesGlobalActionServiceClient* client )
{
    for( PropertyActionServiceList::ConstIterator ServiceIt = MainActionServices.constBegin(); ServiceIt != MainActionServices.constEnd(); ++ServiceIt )
    {
        ListPropertyActionService *ListService = qobject_cast<ListPropertyActionService*>( *ServiceIt );
        if( ListService )
            ListService->registerClient( client );
    }

    ListAllPropertiesGlobalActionServiceClients.append( client );
}


void PropertyManager::unregisterClient( ListAllPropertiesGlobalActionServiceClient* client )
{
    for( PropertyActionServiceList::ConstIterator ServiceIt = MainActionServices.constBegin(); ServiceIt != MainActionServices.constEnd(); ++ServiceIt )
    {
        ListPropertyActionService *ListService = qobject_cast<ListPropertyActionService*>( *ServiceIt );
        if( ListService )
            ListService->unregisterClient( client );
    }

    ListAllPropertiesGlobalActionServiceClients.removeOne( client );
}


void PropertyManager::addDataActionService( PropertyDataActionService *Service,
                                                   bool AsMain )
{
    DataActionServices.append( Service );
    if( AsMain ) MainDataActionServices.append( Service );
    Service->setAdapter( Adapter );

    for( PropertyAllDataActionServiceClientList::Iterator ClientIt = PropertyAllDataActionServiceClients.begin();
         ClientIt != PropertyAllDataActionServiceClients.end(); ++ClientIt )
        Service->registerClient( *ClientIt );

    informPropertyAllDataActionServiceClients();
}


void PropertyManager::removeDataActionService( const QString &ServiceId, bool Delete )
{
//     bool GlobalChanged = false;
    DataActionServices.remove( ServiceId, Delete );

//     if( GlobalChanged )
//         setGlobalDropService( -1 );

    informPropertyAllDataActionServiceClients();
}


void PropertyManager::informPropertyAllDataActionServiceClients()
{
    // inform clients
    for( PropertyAllDataActionServiceClientList::ConstIterator ClientIt = PropertyAllDataActionServiceClients.constBegin();
         ClientIt != PropertyAllDataActionServiceClients.constEnd(); ++ClientIt )
        (*ClientIt)->onAllDataActionServiceChange();
}


void PropertyManager::registerClient( PropertyAllDataActionServiceClient* client )
{
    for( PropertyDataActionServiceList::ConstIterator ServiceIt = DataActionServices.constBegin();
         ServiceIt != DataActionServices.constEnd(); ++ServiceIt )
        (*ServiceIt)->registerClient( client, client->itemIndex() );

    PropertyAllDataActionServiceClients.append( client );
}


void PropertyManager::unregisterClient( PropertyAllDataActionServiceClient* client )
{
    for( PropertyDataActionServiceList::ConstIterator ServiceIt = DataActionServices.begin();
         ServiceIt != DataActionServices.constEnd(); ++ServiceIt )
        (*ServiceIt)->unregisterClient( client, client->itemIndex() );

    PropertyAllDataActionServiceClients.removeOne( client );
}

void PropertyManager::registerClient( AllPropertiesGlobalDataActionServiceClient* client )
{
    for( PropertyDataActionServiceList::ConstIterator ServiceIt = MainDataActionServices.constBegin();
         ServiceIt != MainDataActionServices.constEnd(); ++ServiceIt )
    {
        ListPropertyDataActionService* ListService =
            qobject_cast<ListPropertyDataActionService*>( *ServiceIt );
        if( ListService )
            ListService->registerClient( client );
    }

    AllPropertiesGlobalDataActionServiceClients.append( client );
}


void PropertyManager::unregisterClient( AllPropertiesGlobalDataActionServiceClient* client )
{
    for( PropertyDataActionServiceList::ConstIterator ServiceIt = MainDataActionServices.constBegin();
         ServiceIt != MainDataActionServices.constEnd(); ++ServiceIt )
    {
        ListPropertyDataActionService* ListService =
            qobject_cast<ListPropertyDataActionService*>( *ServiceIt );
        if( ListService )
            ListService->unregisterClient( client );
    }

    AllPropertiesGlobalDataActionServiceClients.removeOne( client );
}

void PropertyManager::registerClient( ListAllPropertiesGlobalDataActionServiceClient* client )
{
    for( PropertyDataActionServiceList::ConstIterator ServiceIt = MainDataActionServices.constBegin();
         ServiceIt != MainDataActionServices.constEnd(); ++ServiceIt )
    {
        ListPropertyDataActionService *ListService = qobject_cast<ListPropertyDataActionService*>( *ServiceIt );
        if( ListService )
            ListService->registerClient( client );
    }

    ListAllPropertiesGlobalDataActionServiceClients.append( client );
}


void PropertyManager::unregisterClient( ListAllPropertiesGlobalDataActionServiceClient* client )
{
    for( PropertyDataActionServiceList::ConstIterator ServiceIt = MainDataActionServices.constBegin();
         ServiceIt != MainDataActionServices.constEnd(); ++ServiceIt )
    {
        ListPropertyDataActionService *ListService = qobject_cast<ListPropertyDataActionService*>( *ServiceIt );
        if( ListService )
            ListService->unregisterClient( client );
    }

    ListAllPropertiesGlobalDataActionServiceClients.removeOne( client );
}

/*
void PropertyManager::setGlobalDropService( int DropServiceId )
{
    PropertyDataActionService *NewGlobalDropService = (DropServiceId==-1) ? 0 : DataActionServices[DropServiceId];

    // no change?
    if( GlobalDropService == NewGlobalDropService )
        return;

    // unregister all defaultclients from old default service
    if( GlobalDropService )
        for( GlobalDropServiceClientList::Iterator ClientIt = AllPropertiesMainDataActionServices.begin();
             ClientIt != AllPropertiesMainDataActionServices.end(); ++ClientIt )
            GlobalDropService->unregisterDropClient( *ClientIt );

    // set new default service
    GlobalDropService = NewGlobalDropService;

    // register 
    if( GlobalDropService )
        for( GlobalDropServiceClientList::Iterator ClientIt = AllPropertiesMainDataActionServices.begin();
             ClientIt != AllPropertiesMainDataActionServices.end(); ++ClientIt )
            GlobalDropService->registerDropClient( *ClientIt );

    // inform clients
    for( GlobalDropServiceClientList::Iterator ClientIt = AllPropertiesMainDataActionServices.begin();
         ClientIt != AllPropertiesMainDataActionServices.end(); ++ClientIt )
        (*ClientIt)->onGlobalDropServiceSwitch( propertyId() );
}
*/

void PropertyManager::execute( const KABC::Addressee &Person, int ItemIndex, const QMimeData* data,
                                      const QString &ServiceId ) const
{
    PropertyDataActionService* service = DataActionServices[ServiceId];

    if( service )
        service->execute( Person, ItemIndex, data );
}

void PropertyManager::execute( const KABC::AddresseeList& PersonList, const QMimeData* data,
                                      const QString &ServiceId ) const
{
    ListPropertyDataActionService* service =
        qobject_cast<ListPropertyDataActionService*>( DataActionServices[ServiceId] );

    if( service )
        service->execute( PersonList, data );
}

void PropertyManager::setActionServiceSorting( const QStringList& serviceIds )
{
    ActionServices.setOrder( serviceIds );
    MainActionServices.setOrder( serviceIds );
}

void PropertyManager::setDataActionServiceSorting( const QStringList& serviceIds )
{
    DataActionServices.setOrder( serviceIds );
    MainDataActionServices.setOrder( serviceIds );
}

void PropertyManager::setStatusServiceSorting( const QStringList& serviceIds )
{
    StatusServices.setOrder( serviceIds );
}

enum { ActionServiceId = 0, DataActionServiceId = 1, StatusServiceId = 2 };

void PropertyManager::reloadConfig( int ServiceType, const QString& serviceId )
{
    Service* service = 0;
    switch( ServiceType )
    {
        case ActionServiceId: 
            service = ActionServices[serviceId];
            break;
        case DataActionServiceId:
            service = DataActionServices[serviceId];
            break;
        case StatusServiceId: 
            service = StatusServices[serviceId];
    }
    if( service )
        service->reloadConfig();
}

PropertyManager::~PropertyManager()
{
    removeActionService( QString(), true );
    removeDataActionService( QString(), true );
    removeStatusService( QString(), true );
}

}
