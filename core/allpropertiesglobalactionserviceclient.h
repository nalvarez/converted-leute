/*
    This file is part of the KDE project.
    Copyright (c) 2006-2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#ifndef ALLPROPERTIESGLOBALACTIONSERVICECLIENT_H
#define ALLPROPERTIESGLOBALACTIONSERVICECLIENT_H

// lib
#include "propertyactionserviceclient.h"
// Qt
#include <QtCore/QList>


namespace Leute
{

class AllPropertiesGlobalActionServiceClient : public PropertyActionServiceClient
{
public:
    AllPropertiesGlobalActionServiceClient();
    virtual ~AllPropertiesGlobalActionServiceClient();

public: // slots interface
    /** called if the default switched */
    virtual void onGlobalActionServiceSwitch( const QString& propertyId ) = 0;
    virtual void onPropertyManagerChange() = 0;
};


inline AllPropertiesGlobalActionServiceClient::AllPropertiesGlobalActionServiceClient() {}
inline AllPropertiesGlobalActionServiceClient::~AllPropertiesGlobalActionServiceClient() {}

typedef QList<AllPropertiesGlobalActionServiceClient*> AllPropertiesGlobalActionServiceClientList;

}

#endif
