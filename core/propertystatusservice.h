/*
    This file is part of the Leute Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#ifndef LEUTE_PROPERTYSTATUSSERVICE_H
#define LEUTE_PROPERTYSTATUSSERVICE_H

// lib
#include "status.h"
#include "propertyservice.h"

namespace KABC { class Addressee; }


namespace Leute
{

class PropertyStatusServiceClient;


// TODO: add possibility to determine if/how dynamic service can be invoked on the type
// perhaps by adding return bool to fillMenu
// better show unavailability instead of ignoring? Or both, chosen by service?
class LEUTECORE_EXPORT PropertyStatusService : public PropertyService
{
    Q_OBJECT
public:
    PropertyStatusService( QObject* parent, const QStringList& arguments );
    virtual ~PropertyStatusService();

public: // interface
    virtual Status status( const KABC::Addressee& person, int itemIndex, int flags ) const = 0;

    /** @returns if the service is possible for the item; defaults to return true */
    virtual bool supports( const KABC::Addressee& person, int itemIndex ) const;

    /** if ItemIndex = -1 register all items */
    virtual void registerClient( PropertyStatusServiceClient* client, int itemIndex = -1 );
    virtual void unregisterClient( PropertyStatusServiceClient* client, int itemIndex = -1 );
};


inline PropertyStatusService::PropertyStatusService(QObject* parent, const QStringList& arguments )
  : PropertyService( parent, arguments )
{}
inline PropertyStatusService::~PropertyStatusService() {}

}

#endif
