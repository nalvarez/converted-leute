/*
    This file is part of the KDE project.
    Copyright (c) 2006-2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#ifndef LEUTE_DATAACTIONSERVICECLIENT_H
#define LEUTE_DATAACTIONSERVICECLIENT_H

namespace KABC { class Addressee; }
class QString;
class QMimeData;


namespace Leute
{

class DataActionService;


class DataActionServiceClient
{
public:
    enum ChangeType { NoChange = 0, Available = 1, Unavailable = 2, Unknown = 3, CustomBase = 1024, _Max = 1<<31 };

public:
    DataActionServiceClient();
    virtual ~DataActionServiceClient();

public: // interface
    virtual const QMimeData* dataSource() const = 0;
    /** returns the person for which the services are requested */
    virtual const KABC::Addressee& person() const = 0;

public: // slots interface
    /** called if the service switched */
    virtual void onDataActionServiceStateChange( const DataActionService& service, int change ) = 0;
};


inline DataActionServiceClient::DataActionServiceClient() {}
inline DataActionServiceClient::~DataActionServiceClient() {}

}

#endif
