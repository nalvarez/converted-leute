/*
    This file is part of the KDE project.
    Copyright (c) 2006-2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/



#ifndef LEUTE_ACTIONSERVICECLIENT_H
#define LEUTE_ACTIONSERVICECLIENT_H


class QString;
namespace KABC { class Addressee; }

namespace Leute {

class ActionService;


class ActionServiceClient
{
public:
    enum ChangeType { NoChange = 0, Available = 1, Unavailable = 2, Unknown = 3, CustomBase = 1024, _Max = 1<<31 };

public:
    ActionServiceClient();
    virtual ~ActionServiceClient();

public: // interface
    /** returns the person for which the services are requested */
    virtual const KABC::Addressee &person() const = 0;

public: // slots interface
    /** called if the service switched */
    virtual void onActionServiceStateChange( const ActionService &Service, int Change ) = 0;
};


inline ActionServiceClient::ActionServiceClient() {}
inline ActionServiceClient::~ActionServiceClient() {}

}

#endif
