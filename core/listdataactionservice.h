/*
    This file is part of the Leute Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#ifndef LEUTE_LISTDATAACTIONSERVICE_H
#define LEUTE_LISTDATAACTIONSERVICE_H

// lib specific
#include "dataactionservice.h"

namespace KABC { class AddresseeList; }


namespace Leute
{

class ListDataActionServiceClient;


class LEUTECORE_EXPORT ListDataActionService : public DataActionService
{
    Q_OBJECT
public:
    ListDataActionService( QObject* parent, const QStringList& arguments );
    virtual ~ListDataActionService();

public: // interface
    virtual ServiceAction action( const KABC::AddresseeList& personList,
                                  const QMimeData* data, int flags = NoFlags ) const = 0;
    using DataActionService::action;

    /** does the service */
    virtual void execute( const KABC::AddresseeList& personList, const QMimeData* data ) = 0;
    using DataActionService::execute;

    /** @returns if the service is possible for the item; defaults to return true */
    virtual bool supports( const QMimeData* data, const KABC::AddresseeList& personList ) const;
    using DataActionService::supports;

    /** @returns if the service is available currently for the item; defaults to return true */
    virtual bool isAvailableFor( const QMimeData* data, const KABC::AddresseeList& personList ) const;
    using DataActionService::isAvailableFor;

    virtual void registerClient( ListDataActionServiceClient* client );
    virtual void unregisterClient( ListDataActionServiceClient* client );
    using DataActionService::registerClient;
    using DataActionService::unregisterClient;
};

}

#endif
