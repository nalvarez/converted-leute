/*
    This file is part of the KDE project.
    Copyright (c) 2006-2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


#ifndef LEUTE_ROLES_H
#define LEUTE_ROLES_H


namespace Leute {

// TODO: How can one pass parameters in? Like Imagesize?

    enum Roles {
        /**  */
        IdRole = 0,
        /**  */
        DisplayTextRole = 1,
        /**  */
        DisplayIconRole = 2,
        /**  */
        DescriptionTextRole = 3,
        /**  */
        DescriptionIconRole = 4,
        /**  */
        LongDisplayTextRole = 5,
        /**  */
        LongDisplayIconRole = 6,
        /** */
        LabelTextRole = 7,
        /**  */
        LabelIconRole = 8,
        /**  */
        EnabledRole = 9,
        /** first role useable for application specific purposes */
        UserRole = 128 };

}

#endif
