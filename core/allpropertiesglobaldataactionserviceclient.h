/*
    This file is part of the KDE project.
    Copyright (c) 2006-2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#ifndef ALLPROPERTIESDEFAULTDATAACTIONSERVICECLIENT_H
#define ALLPROPERTIESDEFAULTDATAACTIONSERVICECLIENT_H

// lib
#include "leutecore_export.h"
#include "propertydataactionserviceclient.h"
// Qt
#include <QtCore/QList>


namespace Leute
{

class LEUTECORE_EXPORT AllPropertiesGlobalDataActionServiceClient : public PropertyDataActionServiceClient
{
public:
    AllPropertiesGlobalDataActionServiceClient();
    virtual ~AllPropertiesGlobalDataActionServiceClient();

public:
    /** @return true if at least one service is available for the data */
    bool serviceAvailableForData() const;

public: // slots interface
    /** called if the default switched */
    virtual void onGlobalDataActionServiceSwitch( const QString& propertyId ) = 0;
    virtual void onPropertyManagerChange() = 0;
};


inline AllPropertiesGlobalDataActionServiceClient::AllPropertiesGlobalDataActionServiceClient() {}
inline AllPropertiesGlobalDataActionServiceClient::~AllPropertiesGlobalDataActionServiceClient() {}

typedef QList<AllPropertiesGlobalDataActionServiceClient*> AllPropertiesGlobalDataActionServiceClientList;

}

#endif
