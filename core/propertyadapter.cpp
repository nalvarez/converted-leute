/*
    This file is part of the Leute Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#include "propertyadapter.h"

// KDE
#include <kabc/addresseelist.h>
#include <kabc/addressee.h>
// Qt
#include <QtCore/QStringList>


namespace Leute
{

PropertyAdapter::PropertyAdapter( QObject* parent, const QStringList& arguments )
  : QObject( parent ),
    Id( !arguments.empty() ? arguments[0] : QString() )
{}

const QString& PropertyAdapter::id() const { return Id; }

bool PropertyAdapter::haveProperty( const KABC::AddresseeList& personList ) const
{
    bool Result = false;
    for( KABC::AddresseeList::ConstIterator it = personList.begin(); it != personList.end(); ++it )
        if( numberOfItems(*it) != 0 )
        {
            Result = true;
            break;
        }
    return Result;
}

PropertyAdapter::~PropertyAdapter() {}

}
