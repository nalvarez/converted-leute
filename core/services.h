/*
    This file is part of the Leute Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#ifndef LEUTE_SERVICES_H
#define LEUTE_SERVICES_H

// lib
#include "kidptrvector.h"
#include "propertymanager.h"
#include "servicemanager.h"

class QMimeData;


namespace Leute
{

class ServicesPrivate;
class ServiceManager;
class PropertyManager;

typedef KIdPtrVector<PropertyManager> PropertyManagerList;


class LEUTECORE_EXPORT Services : public QObject
{
    friend class ServicesPrivate;
    friend class ServicesSingleton;

    Q_OBJECT

public:
    static Services* self();

protected:
    Services();
public:
    virtual ~Services();

public: // status interface
    void registerClient( StatusServiceClient* client );
    void unregisterClient( StatusServiceClient* client );

public: // action service interface
    /** does the service id */
    void execute( const KABC::Addressee& person, const QString& serviceId );
    void execute( const KABC::AddresseeList& personList, const QString& serviceId );

    void registerClient( ActionServiceClient* client );
    void unregisterClient( ActionServiceClient* client );
    void registerClient( ListActionServiceClient* client );
    void unregisterClient( ListActionServiceClient* client );

public: // data action service interface
    /** does the service id */
    void execute( const KABC::Addressee& person, const QString& serviceId, const QMimeData* data );
    void execute( const KABC::AddresseeList& personList, const QString& serviceId, const QMimeData* data );

    void registerClient( DataActionServiceClient* client );
    void unregisterClient( DataActionServiceClient* client );
    void registerClient( ListDataActionServiceClient* client );
    void unregisterClient( ListDataActionServiceClient* client );

public: // status interface
    void registerClient( PropertyStatusServiceClient* client );
    void unregisterClient( PropertyStatusServiceClient* client );

public: // action service interface
    /** does the service id on the item with the given index of the supported type */
    void execute( const KABC::Addressee& person, const QString& PropertyId, int itemIndex,
                  const QString& serviceId );
    void execute( const KABC::AddresseeList& personList, const QString& propertyId,
                  const QString& serviceId );

    void registerClient( PropertyAllActionServiceClient* client );
    void unregisterClient( PropertyAllActionServiceClient* client );

    void registerClient( AllPropertiesGlobalActionServiceClient* client );
    void unregisterClient( AllPropertiesGlobalActionServiceClient* client );

    void registerClient( ListAllPropertiesGlobalActionServiceClient* client );
    void unregisterClient( ListAllPropertiesGlobalActionServiceClient* client );

public: // data action service interface
    void execute( const KABC::Addressee& person, const QString& PropertyId, int itemIndex,
                  const QString& serviceId, const QMimeData* data );
    void execute( const KABC::AddresseeList& personList, const QString& propertyId,
                  const QString& serviceId, const QMimeData* data );

    void registerClient( PropertyAllDataActionServiceClient* client );
    void unregisterClient( PropertyAllDataActionServiceClient* client );

    void registerClient( AllPropertiesGlobalDataActionServiceClient* client );
    void unregisterClient( AllPropertiesGlobalDataActionServiceClient* client );

    void registerClient( ListAllPropertiesGlobalDataActionServiceClient* client );
    void unregisterClient( ListAllPropertiesGlobalDataActionServiceClient* client );

public: // management interface
    const ServiceManager& serviceManager() const;
    const PropertyManagerList& propertyManagers() const;

signals:
    /** emitted whenever something in the configuration changes */
    void changed();

protected:
    ServicesPrivate* d;
};

}

#endif
