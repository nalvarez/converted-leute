/*
    This file is part of the Leute Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#ifndef LEUTE_SERVICES_P_H
#define LEUTE_SERVICES_P_H

// lib
#include "services.h"
// Qt
#include <QtCore/QObject>

class KConfig;

namespace Leute
{

class ServicesPrivate : public QObject//, virtual public DCOPObject
{
//     K_DCOP
    Q_OBJECT

public:
    ServicesPrivate( Services * );
    virtual ~ServicesPrivate();

public:
// k_dcop:
    void onConfigChange();
    void onServiceConfigChange( int ServiceType, QString PropertyId, QString ServiceId );

public:
    ServiceManager& serviceManager();
    const ServiceManager& serviceManager() const;

public:
    void informStatusServiceClients();
    void informAllPropertiesGlobalActionServiceClients();

public:
    void load();
    void loadActionServices( const QStringList &SortingIds, const QStringList &HiddenIds );
    void loadDataActionServices( const QStringList &SortingIds, const QStringList &HiddenIds );
    void loadStatusServices( const QStringList &SortingIds, const QStringList &HiddenIds );
    void loadProperties( const QStringList &SortedPropertyIds, const QStringList &HiddenPropertyIds );
    void loadActionServices( const QMap<QString,QStringList> &ActionSortingIds,
                             const QMap<QString,QStringList> &HiddenActionIds,
                             const QMap<QString,QStringList> &MainActionIds );
    void loadDataActionServices( const QMap<QString,QStringList> &DataActionSortingIds,
                                 const QMap<QString,QStringList> &HiddenDataActionIds,
                                 const QMap<QString,QStringList> &MainDataActionIds );
    void loadStatusServices( const QMap<QString,QStringList> &StatesSortingIds,
                             const QMap<QString,QStringList> &HiddenStatesIds );

    void addManager( PropertyManager* Manager );
    void removeManager( const QString &PropertyId, bool Delete = false );

public:
    /** */
    PropertyManagerList PropertyManagers;

    PropertyStatusServiceClientList StatusServiceClients;
    AllPropertiesGlobalActionServiceClientList GlobalActionServiceClients;
    ListAllPropertiesGlobalActionServiceClientList ListGlobalActionServiceClients;
    AllPropertiesGlobalDataActionServiceClientList GlobalDataActionServiceClients;
    ListAllPropertiesGlobalDataActionServiceClientList ListGlobalDataActionServiceClients;

protected slots:
    void onKSyCoCaChange();

protected:
    Services *Base;

    ServiceManager Manager;
};

inline ServiceManager &ServicesPrivate::serviceManager() { return Manager; }
inline const ServiceManager &ServicesPrivate::serviceManager() const { return Manager; }

}

#endif
