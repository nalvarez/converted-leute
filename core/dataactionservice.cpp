/*
    This file is part of the Leute Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#include "dataactionservice.h"


namespace Leute
{

DataActionService::DataActionService( QObject* parent, const QStringList& arguments )
  : Service( parent, arguments )
{}


bool DataActionService::supports( const QMimeData* data ) const
{
    Q_UNUSED( data );
    return true;
}

bool DataActionService::isAvailableFor( const QMimeData* data ) const
{
    return supports( data );
}

bool DataActionService::supports( const QMimeData* data, const KABC::Addressee& person ) const
{
    Q_UNUSED( person );
    return supports( data );
}


bool DataActionService::isAvailableFor( const QMimeData* data, const KABC::Addressee& person ) const
{
    Q_UNUSED( person );
    return isAvailableFor( data );
}

void DataActionService::registerClient( DataActionServiceClient* client ) { Q_UNUSED(client); }
void DataActionService::unregisterClient( DataActionServiceClient* client ) { Q_UNUSED(client); }

DataActionService::~DataActionService() {}

}
