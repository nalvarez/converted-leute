/*
    This file is part of the Leute Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#ifndef STATUSSERVICECLIENT_H
#define STATUSSERVICECLIENT_H

namespace KABC { class Addressee; }
class QString;
template<class C> class QList;


namespace Leute
{

class PropertyStatusService;
class Status;
class StatusChange;


class PropertyStatusServiceClient
{
public:
    PropertyStatusServiceClient();
    virtual ~PropertyStatusServiceClient();

public: // interface
    /** returns the person for which the services are requested */
    virtual const KABC::Addressee& person() const = 0;

public: // slots interface
    /** called if the service switched */
    virtual void onStateChange( const PropertyStatusService& service, const StatusChange& change,
                                const Status& newStatus, int ItemIndex ) = 0;
    virtual void onPropertyManagerChange() = 0;
};


inline PropertyStatusServiceClient::PropertyStatusServiceClient() {}
inline PropertyStatusServiceClient::~PropertyStatusServiceClient() {}

typedef QList<PropertyStatusServiceClient*> PropertyStatusServiceClientList;

}

#endif
