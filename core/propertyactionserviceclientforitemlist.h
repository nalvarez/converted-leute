/*
    This file is part of the KDE project.
    Copyright (c) 2006-2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


#ifndef ACTIONSERVICECLIENTFORITEMLIST_H
#define ACTIONSERVICECLIENTFORITEMLIST_H


// qt specific
#include <qvaluelist.h>
// lib specific
#include "propertyactionserviceclient.h"

namespace Leute {


struct PropertyActionServiceClientForItem
{
    PropertyActionServiceClient* Client;
    int ItemIndex;

    PropertyActionServiceClientForItem() : Client( 0 ), ItemIndex( -1 ) {}
    PropertyActionServiceClientForItem( PropertyActionServiceClient* C, int I ) : Client( C ), ItemIndex( I ) {}
    bool operator==( const PropertyActionServiceClientForItem& Other ) const
        { return Client == Other.Client && ItemIndex == Other.ItemIndex; }
};

typedef QValueList<PropertyActionServiceClientForItem> PropertyActionServiceClientForItemList;

}

#endif
