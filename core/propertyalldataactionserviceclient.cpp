/*
    This file is part of the Leute Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#include "propertyalldataactionserviceclient.h"

// lib
#include "services.h"


namespace Leute
{

bool PropertyAllDataActionServiceClient::serviceAvailableForData() const
{
    bool result = false;
    PropertyManager *Manager = Services::self()->propertyManagers()[propertyId()];

    if( Manager )
    {
        const PropertyDataActionServiceList &Services = Manager->dataActionServices();

        PropertyDataActionServiceList::ConstIterator ServiceIt = Services.begin();
        for( ; ServiceIt != Services.end(); ++ServiceIt )
            if( (*ServiceIt)->isAvailableFor(data(),person(),itemIndex()) )
            {
                result = true;
                break;
            }
    }

    return result;
}

}
