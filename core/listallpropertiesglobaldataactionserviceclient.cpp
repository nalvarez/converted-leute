/*
    This file is part of the Leute Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#include "listallpropertiesglobaldataactionserviceclient.h"

// lib
#include "services.h"
#include "listpropertydataactionservice.h"


namespace Leute
{

bool ListAllPropertiesGlobalDataActionServiceClient::serviceAvailableForData() const
{
    const PropertyManagerList &PropertyManagers = Services::self()->propertyManagers();

    bool Result = false;
    PropertyManagerList::ConstIterator ManagerIt = PropertyManagers.begin();
    for( ; ManagerIt != PropertyManagers.end(); ++ManagerIt )
    {
        const PropertyManager &Manager = **ManagerIt;
        const PropertyDataActionServiceList &DataActionServices = Manager.mainDataActionServices();

        PropertyDataActionServiceList::ConstIterator ServiceIt = DataActionServices.begin();
        for( ; ServiceIt != DataActionServices.end(); ++ServiceIt )
        {
            ListPropertyDataActionService* ListService =
                qobject_cast<ListPropertyDataActionService*>( *ServiceIt );
            if( ListService && ListService->isAvailableFor(data(),personList()) )
            {
                Result = true;
                break;
            }
        }
    }
    return Result;
}

}
