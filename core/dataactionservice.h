/*
    This file is part of the Leute Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#ifndef LEUTE_DATAACTIONSERVICE_H
#define LEUTE_DATAACTIONSERVICE_H

// lib specific
#include "serviceaction.h"
#include "service.h"

namespace KABC { class Addressee; }
class QMimeData;


namespace Leute
{
class DataActionServiceClient;


// TODO: show unavailability instead of ignoring? Or both, chosen by service?
/**
 * Base class for all data action services.
 *
 * A service operates on an item of a given property of a person.
 * 
 */
class LEUTECORE_EXPORT DataActionService : public Service
{
    Q_OBJECT
public:
    DataActionService( QObject* parent, const QStringList& arguments );
    virtual ~DataActionService();

public: // interface
    virtual ServiceAction action( const KABC::Addressee& person,
                                  const QMimeData* data, int flags = NoFlags ) const = 0;

    /** does the service */
    virtual void execute( const KABC::Addressee& person, const QMimeData* data ) = 0;

    /** @returns if the service is possible for the item; defaults to return true */
    virtual bool supports( const QMimeData* data ) const;
    virtual bool supports( const QMimeData* data, const KABC::Addressee& person ) const;

    /** @returns if the service is available currently; defaults to return true */
    virtual bool isAvailableFor( const QMimeData* data ) const;
    virtual bool isAvailableFor( const QMimeData* data, const KABC::Addressee& person ) const;

    /** if ItemIndex = -1 register all items */
    virtual void registerClient( DataActionServiceClient* client );
    virtual void unregisterClient( DataActionServiceClient* client );
};

}

#endif
