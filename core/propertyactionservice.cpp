/*
    This file is part of the KDE project.
    Copyright (c) 2006-2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#include "propertyactionservice.h"

namespace Leute
{

PropertyActionService::PropertyActionService(QObject* parent, const QStringList& arguments )
 : PropertyService( parent, arguments )
{}

bool PropertyActionService::isAvailable() const
{
    return true;
}

bool PropertyActionService::isAvailableFor( const KABC::Addressee& person, int itemIndex ) const
{
    return supports( person, itemIndex ) && isAvailable();
}


bool PropertyActionService::supports( const KABC::Addressee& person, int itemIndex ) const
{
    Q_UNUSED( person );
    Q_UNUSED( itemIndex );
    return true;
}

void PropertyActionService::registerClient( PropertyActionServiceClient* client, int itemIndex )
{
    Q_UNUSED( client );
    Q_UNUSED( itemIndex );
}

void PropertyActionService::unregisterClient(PropertyActionServiceClient* client, int itemIndex )
{
    Q_UNUSED( client );
    Q_UNUSED( itemIndex );
}

PropertyActionService::~PropertyActionService() {}

}
