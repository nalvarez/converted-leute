/*
    This file is part of the Leute Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#ifndef LEUTE_PROPERTYACTIONSERVICE_H
#define LEUTE_PROPERTYACTIONSERVICE_H

// lib
#include "serviceaction.h"
#include "propertyservice.h"

namespace KABC { class Addressee; }


namespace Leute
{

class PropertyActionServiceClient;


// TODO: show unavailability instead of ignoring? Or both, chosen by service?
/**
 * Base class for all action services.
 *
 * A service operates on an item of a given property of a person.
 * 
 */
class LEUTECORE_EXPORT PropertyActionService : public PropertyService
{
    Q_OBJECT
protected:
    PropertyActionService( QObject* parent, const QStringList& arguments );
public:
    virtual ~PropertyActionService();

public: // interface
    /** @return action which describes the service */
    virtual ServiceAction action( const KABC::Addressee& person, int itemIndex, int Flags ) const = 0;

    /** does the service on the item with the given index from the supported person type */
    virtual void execute( const KABC::Addressee& person, int itemIndex ) = 0;
    /**
     * Reports the general support of the service for the given item of the supported property.
     * Default implementation returns true.
     * @param Person
     * @param ItemIndex index of the item. If -1 the query is for any item
     * @returns if the service is generally possible for the given or any item, otherwise false
     */
    virtual bool supports( const KABC::Addressee& person, int itemIndex = -1 ) const;

    /**
     * Reports the current availability of the service 
     * Default implementation returns true.
     * @param Person
     * @param ItemIndex index of the item. If -1 at least one item.
     * @returns true if the service is currently available, otherwise false
     */
    virtual bool isAvailable() const;
    /**
     * Reports the current availability of the service for the given item of the supported property.
     * Default implementation returns "supports( Person, ItemIndex ) && isAvailable()".
     * @param Person
     * @param ItemIndex index of the item
     * @returns true if the service is currently available for the item, otherwise false
     */
    virtual bool isAvailableFor( const KABC::Addressee& person, int itemIndex ) const;

    /** if ItemIndex = -1 register all items
     * only items for which the service is possible are registered
     * if client data changes it has to unregister and register again
     */
    virtual void registerClient( PropertyActionServiceClient* client, int itemIndex = -1 );
    virtual void unregisterClient( PropertyActionServiceClient* client, int itemIndex = -1 );
    // KDE4: think about adding support for updating registration on data change
    //virtual void updateClient( ActionServiceClient* Client, int ItemIndex = -1 );
};

}

#endif
