/*
    This file is part of the Leute Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#ifndef LEUTE_SERVICEMANAGER_H
#define LEUTE_SERVICEMANAGER_H

// lib
#include "kidptrvector.h"
#include "statusservice.h"
#include "statusserviceclient.h"
#include "actionservice.h"
#include "actionserviceclient.h"
#include "listactionserviceclient.h"
#include "dataactionservice.h"
#include "dataactionserviceclient.h"
#include "listdataactionserviceclient.h"
// Qt
#include <QtCore/QList>

class QMimeData;


namespace Leute
{

typedef KIdPtrVector<ActionService> ActionServiceList;
typedef KIdPtrVector<DataActionService> DataActionServiceList;
typedef KIdPtrVector<StatusService> StatusServiceList;
typedef QList<ActionServiceClient*> ActionServiceClientList;
typedef QList<ListActionServiceClient*> ListActionServiceClientList;
typedef QList<DataActionServiceClient*> DataActionServiceClientList;
typedef QList<ListDataActionServiceClient*> ListDataActionServiceClientList;
typedef QList<StatusServiceClient*> StatusServiceClientList;

// TODO: hide by iterators in services API
class LEUTECORE_EXPORT ServiceManager
{
    friend class Services;
    friend class ServicesPrivate;
public:
    ServiceManager();
    ~ServiceManager();

public: // status interface
    void registerClient( StatusServiceClient *Client );
    void unregisterClient( StatusServiceClient *Client );

    const StatusServiceList &statusServices() const;

public: // service interface
    void registerClient( ActionServiceClient *Client );
    void unregisterClient( ActionServiceClient *Client );
    void registerClient( ListActionServiceClient *Client );
    void unregisterClient( ListActionServiceClient *Client );

    void execute( const KABC::Addressee &Person, const QString &ServiceId ) const;
    void execute( const KABC::AddresseeList &List, const QString &ServiceId ) const;

    const ActionServiceList &actionServices() const;

public: // data service interface
    void registerClient( DataActionServiceClient *Client );
    void unregisterClient( DataActionServiceClient *Client );
    void registerClient( ListDataActionServiceClient *Client );
    void unregisterClient( ListDataActionServiceClient *Client );

    void execute( const KABC::Addressee &Person , const QMimeData* data, const QString &ServiceId ) const;
    void execute( const KABC::AddresseeList &List, const QMimeData* data, const QString &ServiceId ) const;

    const DataActionServiceList &dataActionServices() const;

public: // management interface
    void addStatusService( StatusService *StatusService  );
    void removeStatusService( const QString &StatusServiceId, bool Delete = false );

    void addActionService( ActionService* S/*, bool AsDefault*/ ); // TODO: addActionServices(), whole list in one step?
    // TODO: does ServiceId do it? Better pointer to the service?
     /** deletes the service with the id 
     * @param ServiceId the id of the service. If -1 all services are deleted
     * @param Delete If true also delete the removed service(s)
     */
   void removeActionService( const QString &ServiceId, bool Delete = false );

    void addDataActionService( DataActionService* S/*, bool AsDefault*/ );
    void removeDataActionService( const QString &ServiceId, bool Delete = false );

protected:
    void setActionServiceSorting( const QStringList &ServiceIds );
    void setDataActionServiceSorting( const QStringList &ServiceIds );
    void setStatusServiceSorting( const QStringList &ServiceIds );

    void reloadConfig( int ServiceType, const QString &ServiceId );

protected:
//     void informStatusServiceClients();
    void informActionServiceClients(); // TODO: move to ClientForItemList
    void informDataActionServiceClients();
    void informStatusServiceClients();

protected:
    /** */
    StatusServiceList StatusServices;
    /** list of all clients interested in status services */
    StatusServiceClientList StatusServiceClients;

    /** */
    ActionServiceList ActionServices;
    /** */
    ActionServiceClientList ActionServiceClients;
    ListActionServiceClientList ListActionServiceClients;

    /** */
    DataActionServiceList DataActionServices;
    /** */
    DataActionServiceClientList DataActionServiceClients;
    ListDataActionServiceClientList ListDataActionServiceClients;
};

}

#endif
