/*
    This file is part of the Leute Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#include "services_p.h"

// lib
#include "propertyadapter.h"
// KDE
#include <kabc/addressee.h>
// #include <dcopclient.h>
#include <KConfigGroup>
#include <KConfig>
#include <ktrader.h>
#include <kparts/componentfactory.h>
#include <ksycoca.h>

#include <kdebug.h>


namespace Leute
{

static const char DCOPName[] = "Services";
static const char DCOPConfigObjectName[] = "LeuteConfig";
static const char DCOPChangeSignalName[] = "changed()";
static const char DCOPServiceConfigChangeSignalName[] = "changed(int,QString,QString)";

static const char ConfigFileName[] =      "leuterc";
static const char GeneralGroupId[] =      "General";
static const char PropertiesGroupId[] =   "Properties";
static const char PropertiesSortingId[] = "Sorting";
static const char PropertiesHiddenId[] =  "Hidden";

static const char PropertyGroupIdTemplate[] = "Property:%1";
static const char ActionsSortingId[] =        "ActionsSorting";
static const char ActionsHiddenId[] =         "HiddenActions";
static const char ActionsMainId[] =           "MainActions";
static const char DataActionsSortingId[] =    "DataActionsSorting";
static const char DataActionsHiddenId[] =     "HiddenDataActions";
static const char DataActionsMainId[] =       "MainDataActions";
static const char StatesSortingId[] =         "StatesSorting";
static const char StatesHiddenId[] =          "HiddenStates";

static const char LeuteActionServiceId[] =      "leute/actionservice";
static const char LeuteDataActionServiceId[] =  "leute/dataactionservice";
static const char LeuteStatusServiceId[] =      "leute/statusservice";

static const char ActionServiceNameId[] =     "X-KDE-ActionService";
static const char DataActionServiceNameId[] = "X-KDE-DataActionService";
static const char StatusServiceNameId[] =     "X-KDE-StatusService";

static const char ABIVersionId[] =            "X-KDE-ABI-Version";
static const char OnlyShowInContextId[] =     "X-KDE-OnlyShowInContext";
static const char NotShowInContextId[] =      "X-KDE-NotShowInContext";
static const char CategoriesId[] =            "X-KDE-Categories";

static const char ServiceABIVersion[] = "1";

ServicesPrivate::ServicesPrivate( Services *S )
:
// DCOPObject( DCOPName ),
Base( S )
{
//     connectDCOPSignal( 0, DCOPConfigObjectName, DCOPChangeSignalName,
//                        "onConfigChange()", false );
//     connectDCOPSignal( 0, DCOPConfigObjectName, DCOPServiceConfigChangeSignalName,
//                        "onServiceConfigChange(int,QString,QString)", false );

    connect( KSycoca::self(), SIGNAL(databaseChanged()), SLOT(onKSyCoCaChange()) );
}

void ServicesPrivate::onConfigChange()
{
    Manager.removeActionService( QString::null, true );
    Manager.removeDataActionService( QString::null, true );

    for( PropertyManagerList::Iterator ManagerIt = PropertyManagers.begin();
         ManagerIt != PropertyManagers.end(); ++ManagerIt )
    {
        (*ManagerIt)->removeActionService( QString::null, true );
        (*ManagerIt)->removeDataActionService( QString::null, true );
        (*ManagerIt)->removeStatusService( QString::null, true );
    }
    PropertyManagers.clear();//TODO: clear client lists, too?

    load();

    emit Base->changed();
}

void ServicesPrivate::onServiceConfigChange( int ServiceType, QString PropertyId, QString ServiceId )
{
    if( PropertyId.isNull() )
    {
        Manager.reloadConfig( ServiceType, ServiceId );
    }
    else
    {
        PropertyManager *Manager = PropertyManagers[PropertyId];

        if( Manager )
            Manager->reloadConfig( ServiceType, ServiceId );
    }
}

void ServicesPrivate::loadActionServices( const QStringList &SortingIds,
                                                          const QStringList &HiddenIds )
{
    KService::List ServiceOffers = KServiceTypeTrader::self()->query( LeuteActionServiceId );
    for( KService::List::ConstIterator it = ServiceOffers.constBegin(); it != ServiceOffers.constEnd(); ++it )
    {
        const KService::Ptr service = *it;

        const QString serviceId = service->property( QString::fromLatin1(ActionServiceNameId) ).toString();
        kDebug() << "ActionService "<<serviceId<<" found: "<<service->library() << endl;

        const QString version = service->property( QString::fromLatin1(ABIVersionId) ).toString();
        if( version != QString::fromLatin1(ServiceABIVersion) )
        {
            kDebug() << "ActionService version incompatible: "
                <<version<< "!="<< QString::fromLatin1(ServiceABIVersion)<< endl;
            continue;
        }

        if( HiddenIds.contains(serviceId) )
        {
            kDebug() << "ActionService hidden: "<<serviceId << endl;
            continue;
        }

        QVariantList arguments;
        arguments.append( serviceId );
        arguments.append( service->property( QString::fromLatin1(OnlyShowInContextId)).toString().split(";",QString::SkipEmptyParts).join(";") );
        arguments.append( service->property( QString::fromLatin1(NotShowInContextId)).toString().split(";",QString::SkipEmptyParts).join(";") );

        ActionService* actionService =
            service->createInstance<ActionService>( 0, arguments );

        if( actionService )
        {
            kDebug() << "Plugin loaded with Arguments: "<< arguments<< endl;
//             bool AsGlobal = MainActionIds[PropertyId].contains( ServiceId );
            Manager.addActionService( actionService/*, AsGlobal*/ );
        }
        else
            kDebug() << "Plugin not loadable: "<<serviceId << endl;
    }
    Manager.setActionServiceSorting( SortingIds );
}

void ServicesPrivate::loadDataActionServices( const QStringList &SortingIds,
                                              const QStringList &HiddenIds )
{
    KService::List ServiceOffers = KServiceTypeTrader::self()->query( LeuteDataActionServiceId );
    for( KService::List::ConstIterator it = ServiceOffers.begin(); it != ServiceOffers.end(); ++it )
    {
        const KService::Ptr service = *it;

        const QString serviceId = service->property( QString::fromLatin1(DataActionServiceNameId) ).toString();
        kDebug() << "DataActionService "<<serviceId<<" found: "<<service->library() << endl;

        const QString version = service->property( QString::fromLatin1(ABIVersionId) ).toString();
        if( version != QString::fromLatin1(ServiceABIVersion) )
        {
            kDebug() << "DataActionService version incompatible: "
                <<version<< "!="<< QString::fromLatin1(ServiceABIVersion)<< endl;
            continue;
        }

        if( HiddenIds.contains(serviceId) )
        {
            kDebug() << "DataActionService hidden: "<<serviceId << endl;
            continue;
        }

        QVariantList arguments;
        arguments.append( serviceId );
        arguments.append( service->property( QString::fromLatin1(OnlyShowInContextId)).toString().split(";",QString::SkipEmptyParts).join(";") );
        arguments.append( service->property( QString::fromLatin1(NotShowInContextId)).toString().split(";",QString::SkipEmptyParts).join(";") );

        DataActionService *dataActionService =
            service->createInstance<DataActionService>( 0, arguments );

        if( dataActionService )
        {
            kDebug() << "Plugin loaded: "<<dataActionService->id() << endl;
//             bool AsGlobal = MainActionIds[PropertyId].contains( ServiceId );
            Manager.addDataActionService( dataActionService/*, AsGlobal*/ );
        }
        else
            kDebug() << "Plugin not loadable: "<<serviceId << endl;
    }
    Manager.setDataActionServiceSorting( SortingIds );
}

void ServicesPrivate::loadStatusServices( const QStringList &SortingIds,
                                                          const QStringList &HiddenIds )
{
    KService::List ServiceOffers = KServiceTypeTrader::self()->query( LeuteStatusServiceId );
    for( KService::List::ConstIterator it = ServiceOffers.begin(); it != ServiceOffers.end(); ++it )
    {
        const KService::Ptr service = *it;

        const QString serviceId = service->property( QString::fromLatin1(StatusServiceNameId) ).toString();
        kDebug() << "StatusService "<<serviceId<<" found: "<<service->library() << endl;

        const QString version = service->property( QString::fromLatin1(ABIVersionId) ).toString();
        if( version != QString::fromLatin1(ServiceABIVersion) )
        {
            kDebug() << "StatusService version incompatible: "
                <<version<< "!="<< QString::fromLatin1(ServiceABIVersion)<< endl;
            continue;
        }

        if( HiddenIds.contains(serviceId) )
        {
            kDebug() << "StatusService hidden: "<<serviceId << endl;
            continue;
        }

        QVariantList arguments;
        arguments.append( serviceId );
        arguments.append( service->property( QString::fromLatin1(OnlyShowInContextId)).toString().split(";",QString::SkipEmptyParts).join(";") );
        arguments.append( service->property( QString::fromLatin1(NotShowInContextId)).toString().split(";",QString::SkipEmptyParts).join(";") );

        StatusService *statusService =
            service->createInstance<StatusService>( 0, arguments );

        if( statusService )
        {
            kDebug() << "Plugin loaded: "<<statusService->id() << endl;
//             bool AsGlobal = MainActionIds[PropertyId].contains( ServiceId );
            Manager.addStatusService( statusService/*, AsGlobal*/ );
        }
        else
            kDebug() << "Plugin not loadable: "<<serviceId << endl;
    }
    Manager.setStatusServiceSorting( SortingIds );
}

// TODO: versioning support, by writing the v no from the header into a call, which gets tested
void ServicesPrivate::load()
{
    KConfig config( ConfigFileName );

    KConfigGroup propertiesConfigGroup = config.group( PropertiesGroupId );
    const QStringList SortedPropertyIds =
        propertiesConfigGroup.readEntry<QStringList>( QString::fromLatin1(PropertiesSortingId), QStringList() );
    const QStringList HiddenPropertyIds =
        propertiesConfigGroup.readEntry<QStringList>( QString::fromLatin1(PropertiesHiddenId), QStringList() );
    loadProperties( SortedPropertyIds, HiddenPropertyIds );

    KConfigGroup generalConfigGroup = config.group( GeneralGroupId );
    const QStringList ActionSortingIds =
        generalConfigGroup.readEntry<QStringList>( QString::fromLatin1(ActionsSortingId), QStringList() );
    const QStringList HiddenActionIds =
        generalConfigGroup.readEntry<QStringList>(QString::fromLatin1( ActionsHiddenId), QStringList() );
    loadActionServices( ActionSortingIds, HiddenActionIds );
    const QStringList DataActionSortingIds =
        generalConfigGroup.readEntry<QStringList>( QString::fromLatin1(DataActionsSortingId), QStringList() );
    const QStringList HiddenDataActionIds =
        generalConfigGroup.readEntry<QStringList>( QString::fromLatin1(DataActionsHiddenId), QStringList() );
    loadDataActionServices( DataActionSortingIds, HiddenDataActionIds );
    const QStringList StatusSortingIds =
        generalConfigGroup.readEntry<QStringList>( QString::fromLatin1(StatesSortingId), QStringList() );
    const QStringList HiddenStatusIds =
        generalConfigGroup.readEntry<QStringList>( QString::fromLatin1(StatesHiddenId), QStringList() );
    loadStatusServices( StatusSortingIds, HiddenStatusIds );

    QMap<QString,QStringList> ActionSortingIdsMap;
    QMap<QString,QStringList> HiddenActionIdsMap;
    QMap<QString,QStringList> MainActionIdsMap;

    QMap<QString,QStringList> DataActionSortingIdsMap;
    QMap<QString,QStringList> HiddenDataActionIdsMap;
    QMap<QString,QStringList> MainDataActionIdsMap;

    QMap<QString,QStringList> StatesSortingIdsMap;
    QMap<QString,QStringList> HiddenStatesIdsMap;

    for( PropertyManagerList::ConstIterator ManagerIt = PropertyManagers.begin();
         ManagerIt != PropertyManagers.end(); ++ManagerIt )
    {
        const QString& propertyId = (*ManagerIt)->id();

        KConfigGroup propertyConfigGroup =
            config.group( QString::fromLatin1(PropertyGroupIdTemplate).arg(propertyId) );

        ActionSortingIdsMap[propertyId] =
            propertyConfigGroup.readEntry<QStringList>( QString::fromLatin1(ActionsSortingId), QStringList() );
        HiddenActionIdsMap[propertyId] =
            propertyConfigGroup.readEntry<QStringList>( QString::fromLatin1(ActionsHiddenId), QStringList() );
        MainActionIdsMap[propertyId] =
            propertyConfigGroup.readEntry<QStringList>( QString::fromLatin1(ActionsMainId), QStringList() );

        DataActionSortingIdsMap[propertyId] =
            propertyConfigGroup.readEntry<QStringList>( QString::fromLatin1(DataActionsSortingId), QStringList() );
        HiddenDataActionIdsMap[propertyId] =
            propertyConfigGroup.readEntry<QStringList>( QString::fromLatin1(DataActionsHiddenId), QStringList() );
        MainDataActionIdsMap[propertyId] =
            propertyConfigGroup.readEntry<QStringList>( QString::fromLatin1(DataActionsMainId), QStringList() );

        StatesSortingIdsMap[propertyId] =
            propertyConfigGroup.readEntry<QStringList>( QString::fromLatin1(StatesSortingId), QStringList() );
        HiddenStatesIdsMap[propertyId] =
            propertyConfigGroup.readEntry<QStringList>( QString::fromLatin1(StatesHiddenId), QStringList() );
    }
    loadActionServices( ActionSortingIdsMap, HiddenActionIdsMap, MainActionIdsMap );
    loadDataActionServices( DataActionSortingIdsMap, HiddenDataActionIdsMap, MainDataActionIdsMap );
    loadStatusServices( StatesSortingIdsMap, HiddenStatesIdsMap );
}


void ServicesPrivate::loadProperties( const QStringList &SortedPropertyIds, const QStringList &HiddenPropertyIds )
{
    KService::List ServiceOffers = KServiceTypeTrader::self()->query( "leute/property" );
    for( KService::List::ConstIterator it = ServiceOffers.constBegin(); it != ServiceOffers.constEnd(); ++it )
    {
        const KService::Ptr service = *it;
        kDebug() << "Property found: "<<(**it).library() << endl;

        const QString propertyId = service->property( QString::fromLatin1("X-KDE-LeuteProperty") ).toString();

        const QString Version = service->property( QString::fromLatin1(ABIVersionId) ).toString();
        if( Version != QString::fromLatin1(ServiceABIVersion) )
        {
            kDebug() << "Property version incompatible: "
                <<Version<< "!="<< QString::fromLatin1(ServiceABIVersion)<< endl;
            continue;
        }

        if( HiddenPropertyIds.contains(propertyId) )
            kDebug() << "Property hidden: "<<propertyId << endl;
        else
        {
            QVariantList arguments;
            arguments.append( propertyId );
            PropertyAdapter* propertyAdapter =
                service->createInstance<PropertyAdapter>( 0, arguments );

            if( propertyAdapter )
            {
                kDebug() << "Property loaded: "<<propertyAdapter->id() << endl;
                PropertyManagers.append( new PropertyManager(propertyAdapter) );
            }
            else
                kDebug() << "Property not loadable: "<<propertyId << endl;
        }
    }
    PropertyManagers.setOrder( SortedPropertyIds );
}

void ServicesPrivate::loadActionServices( const QMap<QString,QStringList> &ActionSortingIds,
                                                   const QMap<QString,QStringList> &HiddenActionIds,
                                                   const QMap<QString,QStringList> &MainActionIds )
{
    KService::List ServiceOffers = KServiceTypeTrader::self()->query( "leute/propertyactionservice" );
    for( KService::List::ConstIterator it = ServiceOffers.constBegin(); it != ServiceOffers.constEnd(); ++it )
    {
        const KService::Ptr service = *it;

        const QString PropertyId = service->property( QString::fromLatin1("X-KDE-LeuteProperty") ).toString();
        const QString serviceId = service->property( QString::fromLatin1("X-KDE-ActionService") ).toString();
        kDebug() << "ActionService "<<serviceId<<" found: "<<service->library() << " for " << PropertyId << endl;

        const QString Version = service->property( QString::fromLatin1(ABIVersionId) ).toString();
        if( Version != QString::fromLatin1(ServiceABIVersion) )
        {
            kDebug() << "ActionService version incompatible: "
                <<Version<< "!="<< QString::fromLatin1(ServiceABIVersion)<< endl;
            continue;
        }

        PropertyManagerList::ConstIterator ManagerIt = PropertyManagers.find( PropertyId );
        if( ManagerIt == PropertyManagers.end() )
        {
            kDebug() << "Property not loaded: "<<PropertyId << endl;
            continue;
        }
        if( HiddenActionIds[PropertyId].contains(serviceId) )
        {
            kDebug() << "ActionService hidden: "<<serviceId << endl;
            continue;
        }

        QVariantList arguments;
        arguments.append( serviceId );
        arguments.append( service->property( QString::fromLatin1(OnlyShowInContextId)).toString().split(";",QString::SkipEmptyParts).join(";") );
        arguments.append( service->property( QString::fromLatin1(NotShowInContextId)).toString().split(";",QString::SkipEmptyParts).join(";") );

        PropertyActionService *propertyActionService =
            service->createInstance<PropertyActionService>( 0, arguments );

        if( propertyActionService )
        {
            // hacky
            const bool AsMain = 
                ActionSortingIds[PropertyId].contains( serviceId ) ? // already configured?
                    MainActionIds[PropertyId].contains( serviceId ) :
                    !service->property( QString::fromLatin1(CategoriesId) ).toStringList().isEmpty(); // abuse categories
            (*ManagerIt)->addActionService( propertyActionService, AsMain );
            kDebug() << (AsMain?"Plugin loaded as Main: ":"Plugin loaded: ") <<propertyActionService->id() << endl;
        }
        else
            kDebug() << "Plugin not loadable: "<<serviceId << endl;
    }
    for( PropertyManagerList::Iterator ManagerIt = PropertyManagers.begin();
         ManagerIt != PropertyManagers.end(); ++ManagerIt )
        (*ManagerIt)->setActionServiceSorting( ActionSortingIds[(*ManagerIt)->id()] );
}

void ServicesPrivate::loadDataActionServices( const QMap<QString,QStringList> &DataActionSortingIds,
                                                       const QMap<QString,QStringList> &HiddenDataActionIds,
                                                       const QMap<QString,QStringList> &MainDataActionIds )
{
    KService::List ServiceOffers = KServiceTypeTrader::self()->query( "leute/propertydataactionservice" );
    for( KService::List::ConstIterator it = ServiceOffers.constBegin(); it != ServiceOffers.constEnd(); ++it )
    {
        const KService::Ptr service = *it;

        const QString PropertyId = service->property( QString::fromLatin1("X-KDE-LeuteProperty") ).toString();
        const QString serviceId = service->property( QString::fromLatin1("X-KDE-DataActionService") ).toString();
        kDebug() << "DataActionService "<<serviceId<<" found: "<<service->library() << " for " << PropertyId << endl;

        const QString Version = service->property( QString::fromLatin1(ABIVersionId) ).toString();
        if( Version != QString::fromLatin1(ServiceABIVersion) )
        {
            kDebug() << "DataActionService version incompatible: "
                <<Version<< "!="<< QString::fromLatin1(ServiceABIVersion)<< endl;
            continue;
        }

        PropertyManagerList::ConstIterator ManagerIt = PropertyManagers.find( PropertyId );
        if( ManagerIt == PropertyManagers.end() )
        {
            kDebug() << "Property not loaded: "<<PropertyId << endl;
            continue;
        }
        if( HiddenDataActionIds[PropertyId].contains(serviceId) )
        {
            kDebug() << "DataActionService hidden: "<<serviceId << endl;
            continue;
        }

        QVariantList arguments;
        arguments.append( serviceId );
        arguments.append( service->property( QString::fromLatin1(OnlyShowInContextId)).toString().split(";",QString::SkipEmptyParts).join(";") );
        arguments.append( service->property( QString::fromLatin1(NotShowInContextId)).toString().split(";",QString::SkipEmptyParts).join(";") );

        PropertyDataActionService* propertyDataActionService =
            service->createInstance<PropertyDataActionService>( 0, arguments );

        if( propertyDataActionService )
        {
            // hacky
            const bool AsMain = 
                DataActionSortingIds[PropertyId].contains( serviceId ) ? // already configured?
                    MainDataActionIds[PropertyId].contains( serviceId ) :
                    !service->property( QString::fromLatin1(CategoriesId) ).toStringList().isEmpty(); // abuse categories
            (*ManagerIt)->addDataActionService( propertyDataActionService, AsMain );
            kDebug() << (AsMain?"Plugin loaded as Main: ":"Plugin loaded: ")<<propertyDataActionService->id() << endl;
        }
        else
            kDebug() << "Plugin not loadable: "<<serviceId << endl;
    }
    for( PropertyManagerList::Iterator ManagerIt = PropertyManagers.begin();
         ManagerIt != PropertyManagers.end(); ++ManagerIt )
        (*ManagerIt)->setDataActionServiceSorting( DataActionSortingIds[(*ManagerIt)->id()] );
}

void ServicesPrivate::loadStatusServices( const QMap<QString,QStringList> &StatesSortingIds,
                                                   const QMap<QString,QStringList> &HiddenStatesIds )
{
    KService::List ServiceOffers = KServiceTypeTrader::self()->query( "leute/propertystatusservice" );
    for( KService::List::ConstIterator it = ServiceOffers.constBegin(); it != ServiceOffers.constEnd(); ++it )
    {
        const KService::Ptr service = *it;

        const QString PropertyId = service->property( QString::fromLatin1("X-KDE-LeuteProperty") ).toString();
        const QString serviceId = service->property( QString::fromLatin1("X-KDE-StatusService") ).toString();
        kDebug() << "StatusService "<<serviceId<<" found: "<<service->library() << " for " << PropertyId << endl;

        const QString Version = service->property( QString::fromLatin1(ABIVersionId) ).toString();
        if( Version != QString::fromLatin1(ServiceABIVersion) )
        {
            kDebug() << "StatusService version incompatible: "
                <<Version<< "!="<< QString::fromLatin1(ServiceABIVersion)<< endl;
            continue;
        }

        PropertyManagerList::ConstIterator ManagerIt = PropertyManagers.find( PropertyId );
        if( ManagerIt == PropertyManagers.end() )
        {
            kDebug() << "Property not loaded: "<<PropertyId << endl;
            continue;
        }
        if( HiddenStatesIds[PropertyId].contains(serviceId) )
        {
            kDebug() << "StatusService hidden: "<<serviceId << endl;
            continue;
        }

        QVariantList arguments;
        arguments.append( serviceId );
        arguments.append( service->property( QString::fromLatin1(OnlyShowInContextId)).toString().split(";",QString::SkipEmptyParts).join(";") );
        arguments.append( service->property( QString::fromLatin1(NotShowInContextId)).toString().split(";",QString::SkipEmptyParts).join(";") );

        PropertyStatusService* propertyStatusService =
            service->createInstance<PropertyStatusService>( 0, arguments );

        if( propertyStatusService )
        {
            kDebug() << "StatusService loaded: "<<propertyStatusService->id() << endl;
            (*ManagerIt)->addStatusService( propertyStatusService );
        }
        else
            kDebug() << "StatusService not loadable: "<<serviceId << endl;
    }
    for( PropertyManagerList::Iterator ManagerIt = PropertyManagers.begin();
         ManagerIt != PropertyManagers.end(); ++ManagerIt )
        (*ManagerIt)->setStatusServiceSorting( StatesSortingIds[(*ManagerIt)->id()] );
}

void ServicesPrivate::addManager( PropertyManager* Manager )
{
    PropertyManagers.append( Manager );

    for( PropertyStatusServiceClientList::Iterator ClientIt = StatusServiceClients.begin();
         ClientIt != StatusServiceClients.end(); ++ClientIt )
        Manager->registerClient( *ClientIt );

    for( AllPropertiesGlobalActionServiceClientList::Iterator ClientIt = GlobalActionServiceClients.begin();
         ClientIt != GlobalActionServiceClients.end(); ++ClientIt )
        Manager->registerClient( *ClientIt );

    informStatusServiceClients();
    informAllPropertiesGlobalActionServiceClients();
}


void ServicesPrivate::removeManager( const QString &PropertyId, bool Delete )
{
    PropertyManagers.remove( PropertyId, Delete );

    informStatusServiceClients();
    informAllPropertiesGlobalActionServiceClients();
}


void ServicesPrivate::informStatusServiceClients()
{
    // inform clients
    for( PropertyStatusServiceClientList::Iterator ClientIt = StatusServiceClients.begin();
         ClientIt != StatusServiceClients.end(); ++ClientIt )
        (*ClientIt)->onPropertyManagerChange();
}

void ServicesPrivate::informAllPropertiesGlobalActionServiceClients()
{
    // inform clients
    for( AllPropertiesGlobalActionServiceClientList::Iterator ClientIt = GlobalActionServiceClients.begin();
         ClientIt != GlobalActionServiceClients.end(); ++ClientIt )
        (*ClientIt)->onPropertyManagerChange();
}

void ServicesPrivate::onKSyCoCaChange()
{
#if 0
    if( KSycoca::self()->isChanged( "services" ) )
    {
    // ksycoca has new KService objects for us, make sure to update
    // our 'copies' to be in sync with it. Not important for OK, but
    // important for Apply (how to differentiate those 2?).
    // See BR 35071.
    QValueList<TypesListItem *>::Iterator it = m_itemsModified.begin();
    for( ; it != m_itemsModified.end(); ++it ) {
        QString name = (*it)->name();
        if ( removedList.find( name ) == removedList.end() ) // if not deleted meanwhile
            (*it)->refresh();
    }
    m_itemsModified.clear();
    }
#endif
}


ServicesPrivate::~ServicesPrivate()
{
}

}
