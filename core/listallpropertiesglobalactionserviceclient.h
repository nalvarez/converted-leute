/*
    This file is part of the Leute Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#ifndef LISTALLPROPERTIESDEFAULTACTIONSERVICECLIENT_H
#define LISTALLPROPERTIESDEFAULTACTIONSERVICECLIENT_H

// lib
#include "listpropertyactionserviceclient.h"

class QString;
template<class T> class QList;


namespace Leute
{

class ListAllPropertiesGlobalActionServiceClient : public ListPropertyActionServiceClient
{
public:
    ListAllPropertiesGlobalActionServiceClient();
    virtual ~ListAllPropertiesGlobalActionServiceClient();

public: // slots interface
    /** called if the default switched */
    virtual void onGlobalActionServiceSwitch( const QString& propertyId ) = 0;
    virtual void onPropertyManagerChange() = 0;
};


inline ListAllPropertiesGlobalActionServiceClient::ListAllPropertiesGlobalActionServiceClient() {}
inline ListAllPropertiesGlobalActionServiceClient::~ListAllPropertiesGlobalActionServiceClient() {}

typedef QList<ListAllPropertiesGlobalActionServiceClient*> ListAllPropertiesGlobalActionServiceClientList;

}

#endif
