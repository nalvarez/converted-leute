/*
    This file is part of the Leute Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#ifndef LISTDATAACTIONSERVICECLIENT_H
#define LISTDATAACTIONSERVICECLIENT_H

namespace KABC { class AddresseeList; }
class QMimeData;


namespace Leute
{

class PropertyDataActionService;


class ListPropertyDataActionServiceClient
{
public:
    enum ChangeType { NoChange = 0, Available = 1, Unavailable = 2, Unknown = 3, CustomBase = 1024, _Max = 1<<31 };

public:
    ListPropertyDataActionServiceClient();
    virtual ~ListPropertyDataActionServiceClient();

public: // interface
    virtual const QMimeData* data() const = 0;
    /** returns the list of persons for which the services are requested */
    virtual const KABC::AddresseeList& personList() const = 0;

public: // slots interface
    /** called if the service switched */
//     virtual void onActionServiceStateChange( const PropertyActionService &Service, int Change,
//                                              int ItemIndex = -1 ) = 0;
};


inline ListPropertyDataActionServiceClient::ListPropertyDataActionServiceClient() {}
inline ListPropertyDataActionServiceClient::~ListPropertyDataActionServiceClient() {}

}

#endif
