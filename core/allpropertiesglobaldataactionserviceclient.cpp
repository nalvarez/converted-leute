/*
    This file is part of the KDE project.
    Copyright (c) 2006-2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#include "allpropertiesglobaldataactionserviceclient.h"

// lib
#include "propertyadapter.h"
#include "services.h"


namespace Leute
{

bool AllPropertiesGlobalDataActionServiceClient::serviceAvailableForData() const
{
    const PropertyManagerList &PropertyManagers = Services::self()->propertyManagers();

    bool Result = false;
    PropertyManagerList::ConstIterator ManagerIt = PropertyManagers.begin();
    for( ; ManagerIt != PropertyManagers.end(); ++ManagerIt )
    {
        const PropertyManager &Manager = **ManagerIt;
        const PropertyDataActionServiceList &DataActionServices = Manager.mainDataActionServices();
        const PropertyAdapter *Adapter = Manager.propertyAdapter();
        const int ItemsSize = Adapter->numberOfItems( person() );

        PropertyDataActionServiceList::ConstIterator ServiceIt = DataActionServices.begin();
        for( ; ServiceIt != DataActionServices.end(); ++ServiceIt )
        {
            for( int i=0; i<ItemsSize; ++i )
                if( (*ServiceIt)->isAvailableFor(data(),person(),i) )
                {
                    Result = true;
                    break;
                }
        }
    }
    return Result;
}

}
