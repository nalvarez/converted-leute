/*
    This file is part of the Leute Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#ifndef LEUTE_PROPERTYSERVICE_H
#define LEUTE_PROPERTYSERVICE_H

// lib
#include "service.h"


namespace Leute
{

class PropertyAdapter;

// TODO: store here ability to support lists?

// TODO: show unavailability instead of ignoring? Or both, chosen by service?
/**
 * Base class for all services.
 *
 * A service operates on an item of a given property of a person.
 * 
 */
class LEUTECORE_EXPORT PropertyService : public Service
{
protected:
    PropertyService( QObject* parent, const QStringList& arguments );
public:
    virtual ~PropertyService();

public: // interface
    /** */
    virtual void setAdapter( PropertyAdapter* adapter );

public:
    /** @return the servicetype unique identifier of the service */
    const PropertyAdapter* adapter() const;

protected:
    PropertyAdapter* mAdapter;
};


inline PropertyService::PropertyService( QObject* parent, const QStringList& arguments )
: Service( parent, arguments ) {}
inline PropertyService::~PropertyService() {}

inline const PropertyAdapter* PropertyService::adapter() const { return mAdapter; }
inline void PropertyService::setAdapter( PropertyAdapter* adapter ) { mAdapter = adapter; }

}

#endif
