/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// Qt
#include <qlabel.h>
#include <qlineedit.h>
#include <qcombobox.h>
#include <qtooltip.h>
#include <qlayout.h>
// KDE
#include <kglobal.h>
#include <kdialog.h>
#include <kconfig.h>
#include <klocale.h>
// module
#include "openinwebmapconfig.h"


static const char KAddressbookRCFileName[] = "kaddressbookrc";
static const char GeneralGroupId[] = "General";
static const char HookId[] = "LocationMapURL";
static const char HooksId[] = "LocationMapURLs";


static const char GoogleMapsURL[] =
    "http://maps.google.com/maps?f=q&q=%s, %l, %r, %z, %c";
static const char MaporamaURL[] =
    "http://world.maporama.com/idl/maporama/drawaddress.aspx?MD_scale=0.0002&MD_size=500x380&GC_country=%c&GC_address=%s&GC_zip=%z&GC_state=%r&GC_city=%l";
static const char Map24URL[] =
    "http://link2.map24.com/?lid=9cc343ae&maptype=CGI&lang=%1&street0=%s&zip0=%z&city0=%l&country0=%c";
static const char MapQuestURL[] =
    "http://www.mapquest.com/maps/map.adp?country=%c&address=%s&state=%r&zipcode=%z&city=%l";

OpenInWebMapServiceConfigWidget::OpenInWebMapServiceConfigWidget( QWidget* Parent, const char* Name, const QStringList& Arguments )
 : QWidget( Parent, Name )
{
Q_UNUSED(Arguments)
    KGlobal::locale()->insertCatalogue( "khalkhi_address" );

    QHBoxLayout *TopLayout = new QHBoxLayout( this, 0, KDialog::spacingHint() );

    QLabel *Label = new QLabel( i18n("Open URL:"), this );

    HookEdit = new QComboBox( true, this );
    HookEdit->setSizePolicy( QSizePolicy(QSizePolicy::Preferred,QSizePolicy::Fixed) );
    const QString EditToolTip = i18n( ""
        "You can use the following macros in the url:"
        "<ul>"
            "<li><b>%s</b>: the street</li>"
            "<li><b>%l</b>: the location</li>"
            "<li><b>%r</b>: the region</li>"
            "<li><b>%z</b>: the zip code</li>"
            "<li><b>%c</b>: the country ISO code</li>"
            "<li><b>%1</b>: your language ISO code</li>"
        "</ul>" );
    QToolTip::add( Label, EditToolTip );
    QToolTip::add( HookEdit, EditToolTip );

    TopLayout->addWidget( Label );
    TopLayout->addWidget( HookEdit );

    KConfig Config( KAddressbookRCFileName );
    Config.setGroup( GeneralGroupId );

    // TODO: move this data into configrc, don't include defaults in source file
    // TODO: try to make this use KHotNewStuff
    QStringList DefaultURLTemplates;
    DefaultURLTemplates
        << QString::fromLatin1(GoogleMapsURL)
        << QString::fromLatin1(MaporamaURL)
        << QString::fromLatin1(Map24URL)
        << QString::fromLatin1(MapQuestURL);

    OriginalURLTemplate = Config.readEntry( HookId, DefaultURLTemplates[0] );
    URLsTemplate = Config.readListEntry( HooksId, DefaultURLTemplates );
    HookEdit->insertStringList( URLsTemplate );
    HookEdit->setCurrentText( OriginalURLTemplate );

    connect( HookEdit, SIGNAL(textChanged(const QString &) ), SLOT(updateChanged()) );
}


void OpenInWebMapServiceConfigWidget::save()
{
    KConfig Config( KAddressbookRCFileName );
    Config.setGroup( GeneralGroupId );
    Config.writeEntry( HookId, HookEdit->currentText() );
}

void OpenInWebMapServiceConfigWidget::defaults()
{
    HookEdit->setCurrentText( QString::fromLatin1(GoogleMapsURL) );
    HookEdit->lineEdit()->setCursorPosition( 0 );
}

void OpenInWebMapServiceConfigWidget::updateChanged()
{
    emit changed( HookEdit->currentText() != OriginalURLTemplate );
}

OpenInWebMapServiceConfigWidget::~OpenInWebMapServiceConfigWidget()
{
    KGlobal::locale()->removeCatalogue( "khalkhi_address" );
}

#include "openinwebmapconfig.moc"


// KDE
#include <kgenericfactory.h>

typedef K_TYPELIST_1( OpenInWebMapServiceConfigWidget ) Product;
K_EXPORT_COMPONENT_FACTORY( khalkhipropertyactionserviceconfig_openinwebmap, KGenericFactory<Product>("khalkhipropertyactionserviceconfig_openinwebmap") )
