/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// kde specific
#include <kglobal.h>
#include <klocale.h>
#include <kabc/addressee.h>
#include <kabc/address.h>
#include <kconfig.h>
#include <kapp.h>
// Khalkhi
#include <propertyadapter.h>
// service specific
#include "openinwebmapserviceactionadapter.h"
#include "openinwebmapservice.h"


OpenInWebMapService::OpenInWebMapService( QObject* Parent, const char* Name, const QStringList& Arguments )
: PropertyActionService( Parent, Name, Arguments )
{
    KGlobal::locale()->insertCatalogue( "khalkhi_address" );

    OpenInWebMapService::reloadConfig();
}

void OpenInWebMapService::reloadConfig()
{
    KConfig Config( "kaddressbookrc" );
    Config.setGroup( "General" );

    URLTemplate = Config.readEntry( "LocationMapURL" );
}


ServiceAction OpenInWebMapService::action( const KABC::Addressee &Person, int ItemIndex, int Flags ) const
{
    const KABC::Address A = Person.addresses()[ItemIndex];

    const QString Address = Flags&ReferItem ? A.formattedAddress() : QString::null;

    return new OpenInWebMapServiceActionAdapter( Address );
}

bool OpenInWebMapService::supports( const KABC::Addressee &Person, int ItemIndex ) const
{
    bool IsSupported = false;

    int MaxItemIndex;
    if( ItemIndex == -1 )
    {
        ItemIndex = 0;
        MaxItemIndex = Adapter->numberOfItems( Person );
    }
    else
        MaxItemIndex = ItemIndex+1;

    for( ; ItemIndex<MaxItemIndex; ++ItemIndex )
    {
//        if( !PropertyParser::folderName(Khalkhi,ItemIndex).isEmpty() )
        {
            IsSupported = true;
            break;
        }
    }

    return IsSupported;
}

bool OpenInWebMapService::isAvailable() const
{
    return !URLTemplate.isEmpty();
}

inline static QString createUrl( const QString &URLTemplate, const KABC::Address &Address )
{
  return URLTemplate.arg( KGlobal::locale()->country() ).
                     replace( "%s", Address.street() ).
                     replace( "%r", Address.region() ).
                     replace( "%l", Address.locality() ).
                     replace( "%z", Address.postalCode() ).
                     replace( "%c", Address.countryToISO( Address.country() ) );
}

void OpenInWebMapService::execute( const KABC::Addressee &Person, int ItemIndex )
{
    const KABC::Address Address = Person.addresses()[ItemIndex];

    kapp->invokeBrowser( createUrl(URLTemplate,Address) );
}

OpenInWebMapService::~OpenInWebMapService()
{
    KGlobal::locale()->removeCatalogue( "khalkhi_address" );
}

// kde specific
#include <kgenericfactory.h>

typedef K_TYPELIST_1( OpenInWebMapService ) Product;
K_EXPORT_COMPONENT_FACTORY( khalkhipropertyactionservice_openinwebmap, KGenericFactory<Product>("khalkhipropertyactionservice_openinwebmap") )
