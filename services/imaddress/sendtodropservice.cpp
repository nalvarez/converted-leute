/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// kde specific
#include <kglobal.h>
#include <klocale.h>
#include <kabc/addressee.h>
#include <kimproxy.h>
#include <kapp.h>
#include <kurl.h>
#include <kurldrag.h>
// khalkhi core specific
#include <propertyadapter.h>
// service specific
#include "sendtodropserviceactionadapter.h"
#include "sendtodropservice.h"


SendToDataActionService::SendToDataActionService( QObject* Parent, const char* Name, const QStringList& Arguments )
: PropertyDataActionService( Parent, Name, Arguments )
{
    KGlobal::locale()->insertCatalogue( "khalkhi_imaddress" );

    IMProxy = KIMProxy::instance( kapp->dcopClient() );
    IMProxy->initialize();

    connect( IMProxy, SIGNAL(sigContactPresenceChanged( const QString& )),
             this,    SLOT(  onPresenceChanged( const QString& )) );
    connect( IMProxy, SIGNAL(sigPresenceInfoExpired()),
             this,    SLOT(  onPresenceInfoExpired()) );
}

ServiceAction SendToDataActionService::action( const KABC::Addressee &Person, int /*ItemIndex*/,
                                                        QMimeSource *DataSource, int /*Flags*/ ) const
{
    const QString &UID = Person.uid();

    const SendToDropServiceActionAdapter::KState State =
        !IMProxy->imAppsAvailable() ?    SendToDropServiceActionAdapter::IMNotAvailable :
        !IMProxy->isPresent( UID )  ?    SendToDropServiceActionAdapter::Unregistered :
        !IMProxy->canReceiveFiles(UID) ? SendToDropServiceActionAdapter::CannotSend :
        SendToDropServiceActionAdapter::Registered;

    KURL::List URLs;
    KURLDrag::decode( DataSource, URLs );
    const int URLsSize = URLs.size();

    return new SendToDropServiceActionAdapter( IMProxy, State, UID, URLsSize );
}


void SendToDataActionService::execute( const KABC::Addressee &Person, int /*ItemIndex*/, QMimeSource *Data )
{
    KURL::List  URLs;

    if( IMProxy->canReceiveFiles(Person.uid()) && KURLDrag::decode(Data,URLs) )
    {
        KURL::List::ConstIterator it;
        for( it = URLs.begin(); it != URLs.end(); ++it )
            IMProxy->sendFile( Person.uid(), *it );
    }
}


bool SendToDataActionService::supports( QMimeSource *Data ) const
{
    return KURLDrag::canDecode( Data );
}

bool SendToDataActionService::isAvailableFor( QMimeSource *Data, const KABC::Addressee &Person, int /*ItemIndex*/ ) const
{
    return KURLDrag::canDecode( Data ) && IMProxy->canReceiveFiles( Person.uid() );
}

void SendToDataActionService::registerClient( PropertyDataActionServiceClient* Client, int ItemIndex )
{
    Clients.append( PropertyDataActionServiceClientForItem(Client,ItemIndex) );
}


void SendToDataActionService::unregisterClient( PropertyDataActionServiceClient* Client, int ItemIndex )
{
    Clients.remove( PropertyDataActionServiceClientForItem(Client,ItemIndex) );
}

void SendToDataActionService::onPresenceChanged( const QString &UID )
{
    for( PropertyDataActionServiceClientForItemList::Iterator ClientIt = Clients.begin();
         ClientIt != Clients.end(); ++ClientIt )
    {
        if( (*ClientIt).Client->person().uid() == UID )
            (*ClientIt).Client->onDataActionServiceStateChange( *this, PropertyDataActionServiceClient::Unknown );
    }
}


void SendToDataActionService::onPresenceInfoExpired()
{
    for( PropertyDataActionServiceClientForItemList::Iterator ClientIt = Clients.begin();
         ClientIt != Clients.end(); ++ClientIt )
        (*ClientIt).Client->onDataActionServiceStateChange( *this, PropertyDataActionServiceClient::Unknown );
}

SendToDataActionService::~SendToDataActionService()
{
    KGlobal::locale()->removeCatalogue( "khalkhi_imaddress" );
}

#include "sendtodropservice.moc"


// kde specific
#include <kgenericfactory.h>

typedef K_TYPELIST_1( SendToDataActionService ) Product;
K_EXPORT_COMPONENT_FACTORY( khalkhipropertydataactionservice_sendfilesperimto, KGenericFactory<Product>("khalkhipropertydataactionservice_sendfilesperimto") )
