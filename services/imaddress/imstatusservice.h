/*
    This file is part of the KDE project.
    Copyright (c) 2006-2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


#ifndef IMSTATUSSERVICE_H
#define IMSTATUSSERVICE_H


// khalkhi core
#include <propertystatusservice.h>
#include <propertystatusserviceclientforitemlist.h>

class KIMProxy;

struct IMStatusServiceClientForItem : public PropertyStatusServiceClientForItem
{
    int Status;

    IMStatusServiceClientForItem() {}
    IMStatusServiceClientForItem( PropertyStatusServiceClient* C, int I, int S )
    : PropertyStatusServiceClientForItem( C, I ), Status( S ) {}
    IMStatusServiceClientForItem( PropertyStatusServiceClient* C, int I )
    : PropertyStatusServiceClientForItem( C, I ) {}
    bool operator==( const IMStatusServiceClientForItem& Other ) const
        { return PropertyStatusServiceClientForItem::operator==(Other); }
};

typedef QValueList<IMStatusServiceClientForItem> IMStatusServiceClientForItemList;


class IMStatusService : public PropertyStatusService
{
    Q_OBJECT
public:
    IMStatusService( QObject* Parent, const char* Name, const QStringList& Arguments );
    virtual ~IMStatusService();

public: // PropertyActionService API
    virtual Status status( const KABC::Addressee &Person, int ItemIndex, int Flags ) const;

    virtual void registerClient( PropertyStatusServiceClient* Client, int ItemIndex = -1 );
    virtual void unregisterClient( PropertyStatusServiceClient* Client, int ItemIndex = -1 );

protected slots:
    void onPresenceChanged( const QString & );
    void onPresenceInfoExpired();

protected:
    KIMProxy* IMProxy;

protected:
    IMStatusServiceClientForItemList Clients;
};

#endif
