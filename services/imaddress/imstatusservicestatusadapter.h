/*
    This file is part of the KDE project.
    Copyright (C) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


#ifndef IMSTATUSSERVICESTATUSADAPTER_H
#define IMSTATUSSERVICESTATUSADAPTER_H


// khalkhi core specific
#include <statusadapter.h>

class KIMProxy;

using namespace Khalkhi;


class IMStatusAdapter : public StatusAdapter
{
public:
    enum KState { IMNotAvailable, Unregistered, Registered };
public:
    IMStatusAdapter( KIMProxy* P, KState S, const QString &U );
public:
    QVariant data( int Role ) const;

protected:
    const QString UID;
    const KState State;
    KIMProxy* IMProxy;
};

#endif
