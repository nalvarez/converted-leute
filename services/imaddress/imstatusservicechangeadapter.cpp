/*
    This file is part of the KDE project.
    Copyright (C) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// Qt
#include <qimage.h>
// KDE
#include <klocale.h>
#include <kiconloader.h>
#include <kimproxy.h>
// Khalkhi
#include <roles.h>
// service
#include "imstatusservicechangeadapter.h"


static const char* EventIds[] = {
    "",
    "person_online",
    "person_status_change",
    "person_offline"
};

QVariant IMStatusChangeAdapter::data( int Role ) const
{
    QVariant Result;

    switch( Role )
    {
    case IdRole:
        Result = QString::fromLatin1( EventIds[StateChange] );
        break;
    case DisplayTextRole:
        if( StateChange != Unknown )
            Result = i18n( "Is now %1." ).arg( IMProxy->presenceString(UID) );
        break;
    case DisplayIconRole:
        if( StateChange != Unknown )
            Result = IMProxy->presenceIcon( UID ).convertToImage();
        break;
    case EnabledRole:
        Result = (StateChange != Unknown);
    default:
        ;
    }
    return Result;
}
