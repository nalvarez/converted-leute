/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// kde specific
#include <kglobal.h>
#include <klocale.h>
#include <kabc/addressee.h>
// lib specific
#include "imaddresspropertyitemadapter.h"
#include "imaddresspropertyadapter.h"


IMAddressPropertyAdapter::IMAddressPropertyAdapter( QObject* Parent, const char* Name, const QStringList& Arguments )
: PropertyAdapter( Parent, Name, Arguments )
{
    KGlobal::locale()->insertCatalogue( "khalkhi_imaddress" );
}


int IMAddressPropertyAdapter::numberOfItems( const KABC::Addressee &Person ) const
{
    return Person.custom("KADDRESSBOOK","X-IMAddress").isEmpty() ? 0 : 1;
}


PropertyItem IMAddressPropertyAdapter::propertyItemOf( const KABC::Addressee &Person, int /*ItemIndex*/ ) const
{
  return new IMAddressPropertyItemAdapter( Person.custom("KADDRESSBOOK","X-IMAddress") );
}


IMAddressPropertyAdapter::~IMAddressPropertyAdapter()
{
    KGlobal::locale()->removeCatalogue( "khalkhi_imaddress" );
}

// kde specific
#include <kgenericfactory.h>

typedef K_TYPELIST_1( IMAddressPropertyAdapter ) Product;
K_EXPORT_COMPONENT_FACTORY( khalkhiproperty_imaddress, KGenericFactory<Product>("khalkhiproperty_imaddress") )
