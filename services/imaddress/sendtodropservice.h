/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


#ifndef SENDTODATAACTIONSERVICE_H
#define SENDTODATAACTIONSERVICE_H


// khalkhi core specific
#include <propertydataactionservice.h>
#include <propertydataactionserviceclientforitemlist.h>

class KIMProxy;


class SendToDataActionService : public PropertyDataActionService
{
    Q_OBJECT
public:
    SendToDataActionService( QObject* Parent, const char* Name, const QStringList& Arguments );
    virtual ~SendToDataActionService();

public: // PropertyDataActionService API
    virtual ServiceAction action( const KABC::Addressee &Person, int ItemIndex,
                                                 QMimeSource *Data, int Flags ) const;

    virtual void execute( const KABC::Addressee &Person, int ItemIndex, QMimeSource *Data );

    virtual bool supports( QMimeSource *Data ) const;
    virtual bool isAvailableFor( QMimeSource *Data, const KABC::Addressee &Person, int ItemIndex ) const;

    virtual void registerClient( PropertyDataActionServiceClient* Client, int ItemIndex = -1 );
    virtual void unregisterClient( PropertyDataActionServiceClient* Client, int ItemIndex = -1 );

protected slots:
    void onPresenceChanged( const QString & );
    void onPresenceInfoExpired();

protected:
    KIMProxy* IMProxy;

protected:
    PropertyDataActionServiceClientForItemList Clients;
};

#endif
