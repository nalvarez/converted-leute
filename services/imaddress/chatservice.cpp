/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// kde specific
#include <kglobal.h>
#include <klocale.h>
#include <kabc/addressee.h>
#include <kimproxy.h>
#include <kapp.h>
// khalkhi core specific
#include <propertyadapter.h>
// service specific
#include "chatserviceactionadapter.h"
#include "chatservice.h"


ChatService::ChatService( QObject* Parent, const char* Name, const QStringList& Arguments )
: PropertyActionService( Parent, Name, Arguments )

{
    KGlobal::locale()->insertCatalogue( "khalkhi_imaddress" );

    IMProxy = KIMProxy::instance( kapp->dcopClient() );
    IMProxy->initialize();

    connect( IMProxy, SIGNAL(sigContactPresenceChanged( const QString& )),
             this,    SLOT(  onPresenceChanged( const QString& )) );
    connect( IMProxy, SIGNAL(sigPresenceInfoExpired()),
             this,    SLOT(  onPresenceInfoExpired()) );
}

void ChatService::execute( const KABC::Addressee &Person, int /*ItemIndex*/ )
{
    IMProxy->chatWithContact( Person.uid() );
}

ServiceAction ChatService::action( const KABC::Addressee &Person, int /*ItemIndex*/, int /*Flags*/ ) const
{
    const QString &UID = Person.uid();

    ChatServiceActionAdapter::KState State =
        !IMProxy->imAppsAvailable() ? ChatServiceActionAdapter::IMNotAvailable :
        !IMProxy->isPresent( UID )  ? ChatServiceActionAdapter::Unregistered :
        ChatServiceActionAdapter::Registered;

    return new ChatServiceActionAdapter( IMProxy, State, UID );
}


void ChatService::registerClient( PropertyActionServiceClient* Client, int I )
{
    Clients.append( PropertyActionServiceClientForItem(Client,I) );
}


void ChatService::unregisterClient( PropertyActionServiceClient* Client, int ItemIndex )
{
    Clients.remove( PropertyActionServiceClientForItem(Client,ItemIndex) );
}

void ChatService::onPresenceChanged( const QString &UID )
{
    for( PropertyActionServiceClientForItemList::Iterator ClientIt = Clients.begin();
         ClientIt != Clients.end(); ++ClientIt )
    {
        if( (*ClientIt).Client->person().uid() == UID )
            (*ClientIt).Client->onActionServiceStateChange( *this, PropertyActionServiceClient::Unknown );
    }
}


void ChatService::onPresenceInfoExpired()
{
    for( PropertyActionServiceClientForItemList::Iterator ClientIt = Clients.begin();
         ClientIt != Clients.end(); ++ClientIt )
        (*ClientIt).Client->onActionServiceStateChange( *this, PropertyActionServiceClient::Unknown );
}

ChatService::~ChatService()
{
    KGlobal::locale()->removeCatalogue( "khalkhi_imaddress" );
}

#include "chatservice.moc"



// kde specific
#include <kgenericfactory.h>

typedef K_TYPELIST_1( ChatService ) Product;
K_EXPORT_COMPONENT_FACTORY( khalkhipropertyactionservice_chatwith, KGenericFactory<Product>("khalkhipropertyactionservice_chatwith") )
