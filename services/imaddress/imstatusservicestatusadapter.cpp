/*
    This file is part of the KDE project.
    Copyright (C) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// qt specific
#include <qimage.h>
// kde specific
#include <klocale.h>
#include <kiconloader.h>
#include <kimproxy.h>
// khalkhi core
#include <roles.h>
// lib
#include "imstatusservicestatusadapter.h"


IMStatusAdapter::IMStatusAdapter( KIMProxy* P, KState S, const QString &U )
: UID( U ), State(S), IMProxy( P ) {}

QVariant IMStatusAdapter::data( int Role ) const
{
    QVariant Result;

    switch( Role )
    {
    case DisplayTextRole:
        Result = (State==IMNotAvailable) ? i18n( "Instant messaging not available." ) :
                 (State==Unregistered) ?   i18n( "Not registered to messaging programs." ) :
                 IMProxy->presenceString( UID );
        break;
    case DisplayIconRole:
        if( State == Registered )
          Result = IMProxy->presenceIcon( UID ).convertToImage();
        break;
    default:
        ;
    }
    return Result;
}
