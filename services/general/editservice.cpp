/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// kde specific
#include <kabc/addressee.h>
#include <kapplication.h>
#include <dcopclient.h>
#include <dcopref.h>
// lib specific
#include "editserviceactionadapter.h"
#include "editservice.h"


EditService::EditService( QObject* Parent, const char* Name, const QStringList& Arguments )
: ActionService( Parent, Name, Arguments )
{}


ServiceAction EditService::action( const KABC::Addressee &Person, int Flags ) const
{
    return new EditServiceActionAdapter( Flags&ReferItem ? Person.realName() : QString::null );
}


void EditService::execute( const KABC::Addressee &Person )
{
    if ( kapp->dcopClient()->isApplicationRegistered("kaddressbook") )
        DCOPRef( "kaddressbook", "kaddressbook" ).send( "newInstance()" );
    else
        KApplication::startServiceByDesktopName( "kaddressbook" );

    DCOPRef( "kaddressbook", "KAddressBookIface" ).send( "showContactEditor(QString)", Person.uid() );
}

EditService::~EditService() {}


// kde specific
#include <kgenericfactory.h>

typedef K_TYPELIST_1( EditService ) Product;
K_EXPORT_COMPONENT_FACTORY( khalkhiactionservice_edit, KGenericFactory<Product>("khalkhiactionservice_edit") )
