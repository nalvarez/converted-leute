/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


#ifndef CALLUPSERVICE_H
#define CALLUPSERVICE_H


// khalkhi core
#include <propertyactionservice.h>


class CallUpService : public PropertyActionService
{
public:
    CallUpService( QObject* Parent, const char* Name, const QStringList& Arguments );
    virtual ~CallUpService();

public: // Service API
    virtual void reloadConfig();

public: // PropertyService API
    virtual void execute( const KABC::Addressee &Person, int ItemIndex );
    virtual ServiceAction action( const KABC::Addressee &Person, int ItemIndex, int Flags ) const;

    virtual bool supports( const KABC::Addressee &Person, int ItemIndex ) const;
    virtual bool isAvailable() const;

public:
    QString CommandLine;
};

#endif
