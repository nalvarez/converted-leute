/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// kde specific
#include <klocale.h>
#include <kiconloader.h>
// khalkhi core
#include <roles.h>
// service specific
#include "callupserviceactionadapter.h"


QVariant CallUpServiceActionAdapter::data( int Role ) const
{
    QVariant Result;

    switch( Role )
    {
    case DisplayTextRole:
        Result = Unconfigured ?             i18n( "No phone program configured." ) :
                 Number.number().isNull() ? i18n( "Call up..." ) :
                 i18n( "Do a phone call at number (type)","Call up at \"%1\" (%2)..." )
                     .arg( Number.number(), Number.typeLabel() );
        break;
    case DisplayIconRole:
        Result = SmallIconSet( "kcall" );
        break;
    case EnabledRole:
        Result = !Unconfigured;
        break;
    default:
        ;
    }

    return Result;
}
