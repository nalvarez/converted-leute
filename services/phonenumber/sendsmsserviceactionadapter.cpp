/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// kde specific
#include <kabc/addressee.h>
#include <klocale.h>
#include <kiconloader.h>
// khalkhi core
#include <roles.h>
// service specific
#include "sendsmsserviceactionadapter.h"


QVariant SendSMSServiceActionAdapter::data( int Role ) const
{
    QVariant Result;

    switch( Role )
    {
    case DisplayTextRole:
        Result = Unconfigured ?             i18n( "No SMS program configured." ) :
                 Number.number().isNull() ? i18n( "Send SMS..." ) :
                 i18n( "Send SMS at \"%1\" (%2)..." ).arg( Number.number(), Number.typeLabel() );
        break;
    case DisplayIconRole:
        Result = SmallIconSet( "note" );
        break;
    case EnabledRole:
        Result = !Unconfigured;
        break;
    default:
        ;
    }

    return Result;
}
