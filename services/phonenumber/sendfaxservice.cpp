/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// kde specific
#include <kglobal.h>
#include <klocale.h>
#include <kabc/addressee.h>
#include <kconfig.h>
#include <krun.h>
// Khalkhi
#include <propertyadapter.h>
// service specific
#include "sendfaxserviceactionadapter.h"
#include "sendfaxservice.h"


SendFaxService::SendFaxService( QObject* Parent, const char* Name, const QStringList& Arguments )
: PropertyActionService( Parent, Name, Arguments )
{
    KGlobal::locale()->insertCatalogue( "khalkhi_phonenumber" );

    SendFaxService::reloadConfig();
}

void SendFaxService::reloadConfig()
{
    KConfig Config( "kaddressbookrc" );
    Config.setGroup( "General" );

    CommandLine = Config.readEntry( "FaxHookApplication", "kdeprintfax --phone %N" );
}


ServiceAction SendFaxService::action( const KABC::Addressee &Person, int ItemIndex, int Flags ) const
{
    KABC::PhoneNumber Number;
    if( Flags&ReferItem )
        Number = Person.phoneNumbers()[ItemIndex];

    return new SendFaxServiceActionAdapter( Number, CommandLine.isEmpty() );
}

bool SendFaxService::supports( const KABC::Addressee &Person, int ItemIndex ) const
{
    bool IsSupported = false;

    int MaxItemIndex;
    if( ItemIndex == -1 )
    {
        ItemIndex = 0;
        MaxItemIndex = Adapter->numberOfItems( Person );
    }
    else
        MaxItemIndex = ItemIndex+1;

    for( ; ItemIndex<MaxItemIndex; ++ItemIndex )
    {
        const KABC::PhoneNumber Number = Person.phoneNumbers()[ItemIndex];
        if( Number.type() & KABC::PhoneNumber::Fax )
        {
            IsSupported = true;
            break;
        }
    }

    return IsSupported;
}

bool SendFaxService::isAvailableFor( const KABC::Addressee &Person, int ItemIndex ) const
{
    return supports( Person, ItemIndex ) && isAvailable();
}


bool SendFaxService::isAvailable() const
{
    return !CommandLine.isEmpty();
}

void SendFaxService::execute( const KABC::Addressee &Person, int ItemIndex )
{
    const KABC::PhoneNumber Number = Person.phoneNumbers()[ItemIndex];

    QString Command = CommandLine;
    Command.replace( "%N", Number.number() );
    KRun::runCommand( Command );
}

SendFaxService::~SendFaxService()
{
    KGlobal::locale()->removeCatalogue( "khalkhi_phonenumber" );
}

// kde specific
#include <kgenericfactory.h>

typedef K_TYPELIST_1( SendFaxService ) Product;
K_EXPORT_COMPONENT_FACTORY( khalkhipropertyactionservice_sendfax, KGenericFactory<Product>("khalkhipropertyactionservice_sendfax") )
