/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// Qt
#include <qlabel.h>
#include <qlineedit.h>
#include <qtooltip.h>
#include <qlayout.h>
// KDE
#include <kglobal.h>
#include <kdialog.h>
#include <kconfig.h>
#include <klocale.h>
// module
#include "callupconfig.h"


static const char KAddressbookRCFileName[] = "kaddressbookrc";
static const char GeneralGroupId[] = "General";
static const char HookId[] = "PhoneHookApplication";


CallUpServiceConfigWidget::CallUpServiceConfigWidget( QWidget* Parent, const char* Name, const QStringList& Arguments )
 : QWidget( Parent, Name )
{
Q_UNUSED(Arguments)
    KGlobal::locale()->insertCatalogue( "khalkhi_phonenumber" );

    QHBoxLayout *TopLayout = new QHBoxLayout( this, 0, KDialog::spacingHint() );

    QLabel *Label = new QLabel( i18n("Execute Command:"), this );

    HookEdit = new QLineEdit( this );
    const QString EditToolTip = i18n( ""
        "You can use the following macros in the command line:"
        "<ul>"
            "<li><b>%N</b>: the phone number</li>"
        "</ul>" );
    QToolTip::add( Label, EditToolTip );
    QToolTip::add( HookEdit, EditToolTip );

    TopLayout->addWidget( Label );
    TopLayout->addWidget( HookEdit );

    KConfig Config( KAddressbookRCFileName );
    Config.setGroup( GeneralGroupId );

    OriginalScript = Config.readEntry( HookId );
    HookEdit->setText( OriginalScript );

    connect( HookEdit, SIGNAL(textChanged(const QString &) ), SLOT(updateChanged()) );
}


void CallUpServiceConfigWidget::save()
{
    KConfig Config( KAddressbookRCFileName );
    Config.setGroup( GeneralGroupId );
    Config.writeEntry( HookId, HookEdit->text() );
}

void CallUpServiceConfigWidget::defaults()
{
    HookEdit->setText( QString::null );
}

void CallUpServiceConfigWidget::updateChanged()
{
    emit changed( HookEdit->text() != OriginalScript );
}

CallUpServiceConfigWidget::~CallUpServiceConfigWidget()
{
    KGlobal::locale()->removeCatalogue( "khalkhi_phonenumber" );
}

#include "callupconfig.moc"


// KDE
#include <kgenericfactory.h>

typedef K_TYPELIST_1( CallUpServiceConfigWidget ) Product;
K_EXPORT_COMPONENT_FACTORY( khalkhipropertyactionserviceconfig_makephonecall, KGenericFactory<Product>("khalkhipropertyactionserviceconfig_makephonecall") )
