/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// kde specific
#include <kabc/addressee.h>
#include <klocale.h>
#include <kiconloader.h>
#include <kmimetype.h>
// khalkhi core
#include <roles.h>
// service specific
#include "browseserviceactionadapter.h"


QVariant BrowseServiceActionAdapter::data( int Role ) const
{
    QVariant Result;

    switch( Role )
    {
    case DisplayTextRole:
        Result = Homepage.isEmpty() ?
                 i18n( "Browse to Homepage..." ) :
                 i18n( "Browse to <%1>..." ).arg( Homepage.url() );
        break;
    case DisplayIconRole:
        if( Homepage.isEmpty() )
            Result = SmallIconSet( "www" );
        else
            Result = KMimeType::pixmapForURL( Homepage, 0, KIcon::Small );
        break;
    case EnabledRole:
        Result = true;
        break;
    default:
        ;
    }

    return Result;
}
