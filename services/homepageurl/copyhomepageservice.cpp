/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// qt specific
#include <kglobal.h>
#include <klocale.h>
#include <qapplication.h>
#include <qclipboard.h>
// kde specific
#include <kabc/addressee.h>
// service specific
#include "copyhomepageserviceactionadapter.h"
#include "copyhomepageservice.h"


CopyHomepageService::CopyHomepageService( QObject* Parent, const char* Name, const QStringList& Arguments )
: PropertyActionService( Parent, Name, Arguments )
{
    KGlobal::locale()->insertCatalogue( "khalkhi_homepageurl" );
}


void CopyHomepageService::execute( const KABC::Addressee &Person, int /*ItemIndex*/ )
{
    // Copy text into the clipboard
    QApplication::clipboard()->setText( Person.url().url(), QClipboard::Clipboard );
}

ServiceAction CopyHomepageService::action( const KABC::Addressee &Person, int /*ItemIndex*/, int Flags ) const
{
  return new CopyHomepageServiceActionAdapter( Flags&ReferItem ? Person.url() : KURL() );
}

CopyHomepageService::~CopyHomepageService()
{
    KGlobal::locale()->removeCatalogue( "khalkhi_homepageurl" );
}

// kde specific
#include <kgenericfactory.h>

typedef K_TYPELIST_1( CopyHomepageService ) Product;
K_EXPORT_COMPONENT_FACTORY( khalkhipropertyactionservice_copyhomepageurl, KGenericFactory<Product>("khalkhipropertyactionservice_copyhomepageurl") )
