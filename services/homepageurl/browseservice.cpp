/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// kde specific
#include <kglobal.h>
#include <klocale.h>
#include <kabc/addressee.h>
#include <kurl.h>
#include <kapp.h>
// service specific
#include "browseserviceactionadapter.h"
#include "browseservice.h"



BrowseService::BrowseService( QObject* Parent, const char* Name, const QStringList& Arguments )
: PropertyActionService( Parent, Name, Arguments )
{
    KGlobal::locale()->insertCatalogue( "khalkhi_homepageurl" );
}



ServiceAction BrowseService::action( const KABC::Addressee &Person, int /*ItemIndex*/, int Flags ) const
{
  return new BrowseServiceActionAdapter( Flags&ReferItem ? Person.url() : KURL() );
}

void BrowseService::execute( const KABC::Addressee &Person, int /*ItemIndex*/ )
{
/*    QString URL = Person.url().url();

    if( !URL.startsWith("http://") && !URL.startsWith("https://") )
        URL.prepend( "http://" );
*/
    kapp->invokeBrowser( Person.url().url() );
}

BrowseService::~BrowseService()
{
    KGlobal::locale()->removeCatalogue( "khalkhi_homepageurl" );
}


// kde specific
#include <kgenericfactory.h>

typedef K_TYPELIST_1( BrowseService ) Product;
K_EXPORT_COMPONENT_FACTORY( khalkhipropertyactionservice_openhomepage, KGenericFactory<Product>("khalkhipropertyactionservice_openhomepage") )
