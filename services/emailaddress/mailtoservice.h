/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


#ifndef MAILTOSERVICE_H
#define MAILTOSERVICE_H

// khalkhi core specific
#include <listpropertyactionservice.h>


class MailToService : public ListPropertyActionService
{
public:
    MailToService( QObject* Parent, const char* Name, const QStringList& Arguments );
    virtual ~MailToService();

public: // PropertyActionService API
    virtual void execute( const KABC::Addressee &Person, int ItemIndex );
    virtual void execute( const KABC::AddresseeList& PersonList );
    virtual ServiceAction action( const KABC::Addressee &Person, int ItemIndex, int Flags ) const;
    virtual ServiceAction action( const KABC::AddresseeList &PersonList, int Flags ) const;
};

#endif
