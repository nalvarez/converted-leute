/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


#ifndef UNREADINEMAILFOLDERSERVICE_H
#define UNREADINEMAILFOLDERSERVICE_H


// qt
#include <qvaluevector.h>
// kde specific
#include <dcopobject.h>
// khalkhi core
#include <propertystatusservice.h>
#include <propertystatusserviceclientforitemlist.h>

class QTimer;
using namespace Khalkhi;


struct UnreadInEmailFolderServiceClientForItem : public PropertyStatusServiceClientForItem
{
    QValueVector<int> UnreadEmails;

    UnreadInEmailFolderServiceClientForItem() {}
    UnreadInEmailFolderServiceClientForItem( PropertyStatusServiceClient* C, int I, const QValueVector<int> &U )
    : PropertyStatusServiceClientForItem( C, I ), UnreadEmails( U ) {}
    UnreadInEmailFolderServiceClientForItem( PropertyStatusServiceClient* C, int I )
    : PropertyStatusServiceClientForItem( C, I ) {}
    bool operator==( const UnreadInEmailFolderServiceClientForItem& Other ) const
        { return PropertyStatusServiceClientForItem::operator==(Other); }
};

typedef QValueList<UnreadInEmailFolderServiceClientForItem> UnreadInEmailFolderServiceClientForItemList;

/** With KMail of KDE 3.5.3 we get informed about new incoming messages, but not unread becoming read
 * So we have to pull for those folders which have unread emails.
 */
class UnreadInEmailFolderService : public PropertyStatusService, public DCOPObject
{
    Q_OBJECT
    K_DCOP

public:
    UnreadInEmailFolderService( QObject* Parent, const char* Name, const QStringList& Arguments );
    virtual ~UnreadInEmailFolderService();

public: // PropertyStatusService API
    virtual Status status( const KABC::Addressee &Person, int ItemIndex, int Flags ) const;

    virtual void registerClient( PropertyStatusServiceClient* Client, int ItemIndex = -1 );
    virtual void unregisterClient( PropertyStatusServiceClient* Client, int ItemIndex = -1 );

    virtual bool supports( const KABC::Addressee &Person, int ItemIndex ) const;

k_dcop:
    void onUnreadCountChanged();

protected slots:
    void onUpdateTimer();

protected:
    UnreadInEmailFolderServiceClientForItemList Clients;

    /** true if there are any unread known */
    bool AnyUnread;
    int TimeOfLastMessageCountUpdate;
    QTimer *UpdateTimer;
};

#endif
