/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// qt specific
#include <qdir.h>
#include <qfileinfo.h>
// kde specific
#include <kglobal.h>
#include <klocale.h>
#include <kabc/addresseelist.h>
#include <kapp.h>
#include <kurl.h>
#include <kurldrag.h>
#include <kzip.h>
#include <ktempfile.h>
// service specific
#include "mailtodropserviceactionadapter.h"
#include "mailtolistdropserviceactionadapter.h"
#include "mailtodropservice.h"


MailToDataActionService::MailToDataActionService( QObject* Parent, const char* Name, const QStringList& Arguments )
: ListPropertyDataActionService( Parent, Name, Arguments )
{
    KGlobal::locale()->insertCatalogue( "khalkhi_emailaddress" );
}


ServiceAction MailToDataActionService::action( const KABC::Addressee &Person, int ItemIndex,
                                                        QMimeSource *DataSource, int Flags ) const
{
    KURL::List URLs;
    KURLDrag::decode( DataSource, URLs );
    const int URLsSize = URLs.size();
    const QString Address = Flags&ReferItem ? Person.emails()[ItemIndex] : QString::null;

    return new MailToDataActionServiceActionAdapter( Address, URLsSize );
}

ServiceAction MailToDataActionService::action( const KABC::AddresseeList &PersonList,
                                                        QMimeSource *DataSource, int /*Flags*/ ) const
{
    KURL::List URLs;
    KURLDrag::decode( DataSource, URLs );
    const int URLsSize = URLs.size();

    int NoOfEmails = 0;
    for( KABC::AddresseeList::ConstIterator it = PersonList.begin(); it != PersonList.end(); ++it )
        if( (*it).emails().size() > 0 )
            ++NoOfEmails;

    return new MailToListDataActionServiceActionAdapter( NoOfEmails, PersonList.size(), URLsSize );
}


static void createFileNames( QStringList *URLNames, QString *FileNames, const KURL::List &URLs )
{
    for( KURL::List::ConstIterator it = URLs.begin(); it != URLs.end(); ++it )
    {
        if( !FileNames->isEmpty() )
            *FileNames += ", ";
        // zip directory if local
        if( (*it).isLocalFile() && QFileInfo((*it).path()).isDir() )
        {
            KTempFile ZipFile;
            QString ZipFileName = ZipFile.name();
            ZipFile.unlink();

            QDir().mkdir( ZipFileName, true );
            ZipFileName = ZipFileName + '/' + (*it).fileName() + ".zip";
            KZip Zipper( ZipFileName );
            if( !Zipper.open( IO_WriteOnly ) )
                continue; // TODO: error message
            Zipper.addLocalDirectory( (*it).path(), QString() );
            Zipper.close();
            *FileNames += (*it).fileName() + ".zip";
            URLNames->append( ZipFileName );
        }
        else
        {
            *FileNames += (*it).fileName();
            URLNames->append( (*it).url() );
        }
    }
}

void MailToDataActionService::execute( const KABC::Addressee &Person, int ItemIndex, QMimeSource *DataSource )
{
    KURL::List URLs;

    if( KURLDrag::decode(DataSource,URLs) )
    {
        QStringList URLNames;
        QString FileNames;
        createFileNames( &URLNames, &FileNames, URLs );

        kapp->invokeMailer( Person.fullEmail(Person.emails()[ItemIndex]), QString::null, QString::null,
                            FileNames, QString::null, QString::null, URLNames );
    }
}
void MailToDataActionService::execute( const KABC::AddresseeList& PersonList, QMimeSource *DataSource )
{
    KURL::List URLs;

    if( KURLDrag::decode(DataSource,URLs) )
    {
        QStringList URLNames;
        QString FileNames;
        createFileNames( &URLNames, &FileNames, URLs );

        QString EmailAddresses;
        for( KABC::AddresseeList::ConstIterator it = PersonList.begin(); it != PersonList.end(); ++it )
        {
            const QString PreferredEmail = (*it).preferredEmail();
            if( !PreferredEmail.isNull() )
                EmailAddresses.append( (*it).fullEmail(PreferredEmail)+',' );
        }

        kapp->invokeMailer( EmailAddresses, QString::null, QString::null,
                            FileNames, QString::null, QString::null, URLNames );
    }
}


bool MailToDataActionService::supports( QMimeSource *DataSource ) const
{
    return KURLDrag::canDecode( DataSource );
}

MailToDataActionService::~MailToDataActionService()
{
    KGlobal::locale()->removeCatalogue( "khalkhi_emailaddress" );
}

// kde specific
#include <kgenericfactory.h>

typedef K_TYPELIST_1( MailToDataActionService ) Product;
K_EXPORT_COMPONENT_FACTORY( khalkhipropertydataactionservice_emailfilesto, KGenericFactory<Product>("khalkhipropertydataactionservice_emailfilesto") )
