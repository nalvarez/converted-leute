/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// kde specific
#include <kglobal.h>
#include <klocale.h>
#include <kabc/addresseelist.h>
#include <kapp.h>
#include <kurl.h>
#include <kurldrag.h>
// lib specific
#include "mailurlstodropserviceactionadapter.h"
#include "mailurlstolistdropserviceactionadapter.h"
#include "mailurlstodropservice.h"


MailURLsToDataActionService::MailURLsToDataActionService( QObject* Parent, const char* Name, const QStringList& Arguments )
: ListPropertyDataActionService( Parent, Name, Arguments )
{
    KGlobal::locale()->insertCatalogue( "khalkhi_emailaddress" );
}


ServiceAction MailURLsToDataActionService::action( const KABC::Addressee &Person, int ItemIndex,
                                                        QMimeSource *DataSource, int Flags ) const
{
    KURL::List URLs;
    KURLDrag::decode( DataSource, URLs );
    const int URLsSize = URLs.size();
    const QString Address = Flags&ReferItem ? Person.emails()[ItemIndex] : QString::null;

    return new MailURLsToDataActionServiceActionAdapter( Address, URLsSize );
}
ServiceAction MailURLsToDataActionService::action( const KABC::AddresseeList &PersonList,
                                                        QMimeSource *DataSource, int /*Flags*/ ) const
{
    KURL::List URLs;
    KURLDrag::decode( DataSource, URLs );
    const int URLsSize = URLs.size();

    int NoOfEmails = 0;
    for( KABC::AddresseeList::ConstIterator it = PersonList.begin(); it != PersonList.end(); ++it )
        if( (*it).emails().size() > 0 )
            ++NoOfEmails;

    return new MailURLsToListDataActionServiceActionAdapter( NoOfEmails, PersonList.size(), URLsSize );
}


static void composeEmail( QString *Subject, QString *Body, const KURL::List &URLs )
{
    for( KURL::List::ConstIterator it = URLs.begin(); it != URLs.end(); ++it )
    {
        if( !Subject->isEmpty() )
            *Subject += ", ";
        *Subject += (*it).fileName();
        if( !Body->isEmpty() )
            *Body += '\n';
        *Body += (*it).prettyURL();
    }
}

void MailURLsToDataActionService::execute( const KABC::Addressee &Person, int ItemIndex, QMimeSource *Data )
{
    KURL::List URLs;

    if( KURLDrag::decode(Data,URLs) )
    {
        QString Subject;
        QString Body;
        composeEmail( &Subject, &Body, URLs );
        kapp->invokeMailer( Person.fullEmail(Person.emails()[ItemIndex]), QString::null, QString::null,
                            Subject, Body );
    }
}

void MailURLsToDataActionService::execute( const KABC::AddresseeList& PersonList, QMimeSource *Data )
{
    KURL::List URLs;

    if( KURLDrag::decode(Data,URLs) )
    {
        QString Subject;
        QString Body;
        composeEmail( &Subject, &Body, URLs );

        QString EmailAddresses;
        for( KABC::AddresseeList::ConstIterator it = PersonList.begin(); it != PersonList.end(); ++it )
        {
            const QString PreferredEmail = (*it).preferredEmail();
            if( !PreferredEmail.isNull() )
                EmailAddresses.append( (*it).fullEmail(PreferredEmail)+',' );
        }

        kapp->invokeMailer( EmailAddresses, QString::null, QString::null,
                            Subject, Body );
    }
}


bool MailURLsToDataActionService::supports( QMimeSource *Data ) const
{
    return KURLDrag::canDecode( Data );
}

MailURLsToDataActionService::~MailURLsToDataActionService()
{
    KGlobal::locale()->removeCatalogue( "khalkhi_emailaddress" );
}

// kde specific
#include <kgenericfactory.h>

typedef K_TYPELIST_1( MailURLsToDataActionService ) Product;
K_EXPORT_COMPONENT_FACTORY( khalkhipropertydataactionservice_emailurlsto, KGenericFactory<Product>("khalkhipropertydataactionservice_emailurlsto") )
