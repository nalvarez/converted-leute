/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// kde specific
#include <kglobal.h>
#include <klocale.h>
#include <kabc/addresseelist.h>
#include <kapp.h>
// lib specific
#include "mailtoserviceactionadapter.h"
#include "mailtolistserviceactionadapter.h"
#include "mailtoservice.h"


MailToService::MailToService( QObject* Parent, const char* Name, const QStringList& Arguments )
: ListPropertyActionService( Parent, Name, Arguments )
{
    KGlobal::locale()->insertCatalogue( "khalkhi_emailaddress" );
}



ServiceAction MailToService::action( const KABC::Addressee &Person, int ItemIndex, int Flags ) const
{
    return new MailToServiceActionAdapter( Flags&ReferItem ? Person.emails()[ItemIndex] : QString::null );
}

// TODO: is flags needed?
ServiceAction MailToService::action( const KABC::AddresseeList& PersonList, int /*Flags*/ ) const
{
    int NoOfEmails = 0;
    for( KABC::AddresseeList::ConstIterator it = PersonList.begin(); it != PersonList.end(); ++it )
        if( (*it).emails().size() > 0 )
            ++NoOfEmails;

    return new MailToListServiceActionAdapter( NoOfEmails, PersonList.size() );
}


void MailToService::execute( const KABC::Addressee &Person, int ItemIndex )
{
    kapp->invokeMailer( Person.fullEmail(Person.emails()[ItemIndex]), QString::null );
}

void MailToService::execute( const KABC::AddresseeList& PersonList )
{
    QString EmailAddresses;
    for( KABC::AddresseeList::ConstIterator it = PersonList.begin(); it != PersonList.end(); ++it )
    {
        const QString PreferredEmail = (*it).preferredEmail();
        if( !PreferredEmail.isNull() )
            EmailAddresses.append( (*it).fullEmail(PreferredEmail)+',' );
    }
    kapp->invokeMailer( EmailAddresses, QString::null );
}

MailToService::~MailToService()
{
    KGlobal::locale()->removeCatalogue( "khalkhi_emailaddress" );
}

// kde specific
#include <kgenericfactory.h>

typedef K_TYPELIST_1( MailToService ) Product;
K_EXPORT_COMPONENT_FACTORY( khalkhipropertyactionservice_emailto, KGenericFactory<Product>("khalkhipropertyactionservice_emailto") )
