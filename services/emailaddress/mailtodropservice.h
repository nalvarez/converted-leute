/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


#ifndef MAILTODROPSERVICE_H
#define MAILTODROPSERVICE_H


// khalkhi core specific
#include <listpropertydataactionservice.h>


class MailToDataActionService : public ListPropertyDataActionService
{
public:
    MailToDataActionService( QObject* Parent, const char* Name, const QStringList& Arguments );
    virtual ~MailToDataActionService();

public: // KhalkhiPropertyDataActionService API
    virtual ServiceAction action( const KABC::Addressee &Person, int ItemIndex,
                                                 QMimeSource *DataSource, int Flags ) const;
    virtual ServiceAction action( const KABC::AddresseeList& PersonList,
                                                 QMimeSource *DataSource, int Flags ) const;

    virtual void execute( const KABC::Addressee &Person, int ItemIndex, QMimeSource *DataSource );
    virtual void execute( const KABC::AddresseeList& PersonList, QMimeSource *DataSource );

    virtual bool supports( QMimeSource *DataSource ) const;
};

#endif
