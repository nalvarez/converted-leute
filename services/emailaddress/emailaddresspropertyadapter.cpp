/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// kde specific
#include <kabc/addressee.h>
#include <kabc/addresseelist.h>
// property specific
#include "emailaddresspropertyitemadapter.h"
#include "emailaddresspropertyadapter.h"


EmailAddressPropertyAdapter::EmailAddressPropertyAdapter( QObject* Parent, const char* Name, const QStringList& Arguments )
: PropertyAdapter( Parent, Name, Arguments )
{
}

bool EmailAddressPropertyAdapter::haveProperty( const KABC::AddresseeList& PersonList ) const
{
    bool Result = false;
    for( KABC::AddresseeList::ConstIterator it = PersonList.begin(); it != PersonList.end(); ++it )
        if( (*it).emails().size() > 0 )
        {
            Result = true;
            break;
        }
    return Result;
}

int EmailAddressPropertyAdapter::numberOfItems( const KABC::Addressee &Person ) const
{
    return Person.emails().size();
}


PropertyItem EmailAddressPropertyAdapter::propertyItemOf( const KABC::Addressee &Person, int ItemIndex ) const
{
  return new EmailAddressPropertyItemAdapter( Person.emails()[ItemIndex] );
}


EmailAddressPropertyAdapter::~EmailAddressPropertyAdapter() {}


// kde specific
#include <kgenericfactory.h>

typedef K_TYPELIST_1( EmailAddressPropertyAdapter ) Product;
K_EXPORT_COMPONENT_FACTORY( khalkhiproperty_emailaddress, KGenericFactory<Product>("khalkhiproperty_emailaddress") )
