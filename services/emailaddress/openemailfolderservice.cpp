/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// kde specific
#include <kglobal.h>
#include <klocale.h>
#include <kabc/addressee.h>
#include <kapp.h>
#include <kwin.h>
#include <dcopclient.h>
#include <dcopref.h>
// khalkhi core specific
#include <propertyadapter.h>
// service specific
#include "addresstokmailfoldermapparser.h"
#include "openemailfolderserviceactionadapter.h"
#include "openemailfolderservice.h"


OpenEmailFolderService::OpenEmailFolderService( QObject* Parent, const char* Name, const QStringList& Arguments )
: PropertyActionService( Parent, Name, Arguments )
{
    KGlobal::locale()->insertCatalogue( "khalkhi_emailaddress" );
}


void OpenEmailFolderService::execute( const KABC::Addressee &Person, int ItemIndex )
{
    const QString FolderName = PropertyParser::folderName( Person, ItemIndex );
    if( FolderName.isEmpty() )
        return;

    if ( kapp->dcopClient()->isApplicationRegistered("kmail") )
    {
        int WinId = DCOPRef( "kmail", "kmail-mainwindow#1" ).call( "getWinID" );
        int DesktopId = KWin::windowInfo( WinId, NET::WMDesktop ).desktop();
        KWin::setCurrentDesktop( DesktopId );
        KWin::forceActiveWindow( WinId );
    }
    else
        KApplication::startServiceByDesktopName( "kmail" );

    DCOPRef( "kmail", "KMailIface" ).send( "selectFolder(QString)", FolderName );
}

ServiceAction OpenEmailFolderService::action( const KABC::Addressee &Person, int ItemIndex, int Flags ) const
{
    const QString Address = Flags&ReferItem ? Person.emails()[ItemIndex] : QString::null;
    return new OpenEmailFolderServiceActionAdapter( Address );
}

bool OpenEmailFolderService::supports( const KABC::Addressee &Person, int ItemIndex ) const
{
    bool IsSupported = false;

    int MaxItemIndex;
    if( ItemIndex == -1 )
    {
        ItemIndex = 0;
        MaxItemIndex = Adapter->numberOfItems( Person );
    }
    else
        MaxItemIndex = ItemIndex+1;

    for( ; ItemIndex<MaxItemIndex; ++ItemIndex )
    {
        if( !PropertyParser::folderName(Person,ItemIndex).isEmpty() )
        {
            IsSupported = true;
            break;
        }
    }

    return IsSupported;
}

OpenEmailFolderService::~OpenEmailFolderService()
{
    KGlobal::locale()->removeCatalogue( "khalkhi_emailaddress" );
}

// kde specific
#include <kgenericfactory.h>

typedef K_TYPELIST_1( OpenEmailFolderService ) Product;
K_EXPORT_COMPONENT_FACTORY( khalkhipropertyactionservice_openemailfolder, KGenericFactory<Product>("khalkhipropertyactionservice_openemailfolder") )
