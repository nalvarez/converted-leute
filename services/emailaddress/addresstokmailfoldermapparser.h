/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


#ifndef PROPERTYPARSER_H
#define PROPERTYPARSER_H


// qt specific
#include <qstringlist.h>
// kde specific
#include <kabc/addressee.h>


class PropertyParser
{
public:
    static QString folderName( const KABC::Addressee &Person, int ItemIndex );
};

inline QString PropertyParser::folderName( const KABC::Addressee &Person, int ItemIndex )
{
    QString FolderName;

    const QString EmailAddress = Person.emails()[ItemIndex];

    // get foldername
    const QStringList Folders = QStringList::split( ';', Person.custom("KADDRESSBOOK","kmailfolder") );
    for( unsigned int i = 0; i<Folders.size(); ++i )
    {
        const QString &Entry = Folders[i];
        int SplitPos = Entry.find( ':' );
        if( SplitPos != -1 )
        {
            const QString Folder = Entry.mid( SplitPos+1 );

            // check with email address
            const QString AddressForFolder = Entry.left( SplitPos );
            if( AddressForFolder == EmailAddress )
            {
                FolderName = Folder;
                break;
            }
        }
    }

    return FolderName;
}

#endif
