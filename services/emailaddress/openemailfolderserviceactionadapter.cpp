/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// kde specific
#include <klocale.h>
#include <kiconloader.h>
// khalkhi core specific
#include <roles.h>
// service specific
#include "openemailfolderserviceactionadapter.h"


QVariant OpenEmailFolderServiceActionAdapter::data( int Role ) const
{
    QVariant Result;

    switch( Role )
    {
    case DisplayTextRole:
        Result = Address.isEmpty() ? i18n( "Open E-mail folder..." ) :
                 i18n( "Open E-mail folder for %1..." ).arg( Address );
        break;
    case DisplayIconRole:
        Result = SmallIconSet( "email" );
        break;
    case EnabledRole:
        Result = true;
        break;
    default:
        ;
    }

    return Result;
}
