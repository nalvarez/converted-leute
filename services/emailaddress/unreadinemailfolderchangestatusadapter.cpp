/*
    This file is part of the KDE project.
    Copyright (C) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// KDE
#include <kabc/addressee.h>
#include <klocale.h>
#include <kiconloader.h>
// Khalkhi
#include <roles.h>
// service
#include "unreadinemailfolderchangestatusadapter.h"


QVariant UnreadInEmailFolderChangeStatusAdapter::data( int Role ) const
{
    QVariant Result;

    switch( Role )
    {
    case IdRole:
        Result = QString::fromLatin1( "new_mail_arrived" );
        break;
    case DisplayTextRole:
        Result = Address.isEmpty() ?
                     i18n( "1 new E-mail.", "%n new E-mails.", NumberOfNewUnread ) :
                     i18n( "1 new E-mail from %1.", "%n new E-mails from %1.", NumberOfNewUnread ).arg( Address );
        break;
    case DisplayIconRole:
        Result = KGlobal::iconLoader()->loadIcon("email",KIcon::Small/*,Icon->symbolSize()*/).convertToImage();
        break;
    case EnabledRole:
        Result = true;
    default:
        ;
    }

    return Result;
}
