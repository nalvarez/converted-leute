/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


#ifndef OPENEMAILFOLDERSERVICEACTIONADAPTER_H
#define OPENEMAILFOLDERSERVICEACTIONADAPTER_H


// service specific
#include <serviceactionadapter.h>

using namespace Khalkhi;


class OpenEmailFolderServiceActionAdapter : public ServiceActionAdapter
{
public:
    OpenEmailFolderServiceActionAdapter( const QString &A );
    virtual ~OpenEmailFolderServiceActionAdapter();

public: // ServiceActionAdapter API
    virtual QVariant data( int Role ) const;

protected:
    const QString Address;
};


inline OpenEmailFolderServiceActionAdapter::OpenEmailFolderServiceActionAdapter( const QString &A )
: Address( A ) {}

inline OpenEmailFolderServiceActionAdapter::~OpenEmailFolderServiceActionAdapter() {}

#endif
