/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/



#ifndef EMAILADDRESSPROPERTYADAPTER_H
#define EMAILADDRESSPROPERTYADAPTER_H


// khalkhi core
#include <propertyadapter.h>

using namespace Khalkhi;


class EmailAddressPropertyAdapter : public PropertyAdapter
{
public:
    EmailAddressPropertyAdapter( QObject* Parent, const char* Name, const QStringList& Arguments );
    virtual ~EmailAddressPropertyAdapter();

public: // PropertyAdaptor API
    virtual bool haveProperty( const KABC::AddresseeList &PersonList ) const;
    virtual int numberOfItems( const KABC::Addressee &Person ) const;
    virtual PropertyItem propertyItemOf( const KABC::Addressee &Person, int ItemIndex ) const;
};

#endif
