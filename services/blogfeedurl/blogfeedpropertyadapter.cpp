/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// kde specific
#include <kglobal.h>
#include <klocale.h>
#include <kabc/addressee.h>
// service specific
#include "blogfeedpropertyitemadapter.h"
#include "blogfeedpropertyadapter.h"


BlogFeedPropertyAdapter::BlogFeedPropertyAdapter( QObject* Parent, const char* Name, const QStringList& Arguments )
: PropertyAdapter( Parent, Name, Arguments )
{
    KGlobal::locale()->insertCatalogue( "khalkhi_blogfeedurl" );
}

int BlogFeedPropertyAdapter::numberOfItems( const KABC::Addressee &Person ) const 
{
    return Person.custom( "KADDRESSBOOK", "BlogFeed" ).isNull() ? 0 : 1;
}

PropertyItem BlogFeedPropertyAdapter::propertyItemOf( const KABC::Addressee &Person, int /*ItemIndex*/ ) const
{
  return new BlogFeedPropertyItemAdapter( Person.custom( "KADDRESSBOOK", "BlogFeed" ) );
}

BlogFeedPropertyAdapter::~BlogFeedPropertyAdapter()
{
    KGlobal::locale()->removeCatalogue( "khalkhi_blogfeedurl" );
}


// kde specific
#include <kgenericfactory.h>

typedef K_TYPELIST_1( BlogFeedPropertyAdapter ) Product;
K_EXPORT_COMPONENT_FACTORY( khalkhiproperty_blogfeedurl, KGenericFactory<Product>("khalkhiproperty_blogfeedurl") )
