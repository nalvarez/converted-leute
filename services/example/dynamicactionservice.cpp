/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// qt specific
#include <qtimer.h>
// kde specific
#include <kmessagebox.h>
#include <klocale.h>
// lib specific
#include "dynamicactionserviceactionadapter.h"
#include "dynamicactionservice.h"

static const int MaxIndexFlag = 3;


DYNAMICACTION_Service::DYNAMICACTION_Service( QObject* Parent, const char* Name, const QStringList& Arguments )
: PropertyActionService( Parent, Name, Arguments ),
AvailableIndexFlag( MaxIndexFlag )
{
    // simulate outer events like changes in one of the systems the service uses
    QTimer *Timer = new QTimer( this );
    connect( Timer, SIGNAL(timeout()), SLOT(onEvent()) );
    Timer->start( 1000 );
}

// this method invokes the action on the supported property with the given index
void DYNAMICACTION_Service::execute( const KABC::Addressee &Person, int ItemIndex )
{
    QString Data;
    if( TypeAdapter ) Data = TypeAdapter->property( Person, ItemIndex ).data();

    KMessageBox::information( 0, i18n( "Care now for me: %1" ).arg(Data) );
}

ServiceAction DYNAMICACTION_Service::action( const KABC::Addressee &Person, int ItemIndex, int Flags ) const
{
    QString Data;
    if( TypeAdapter ) Data = TypeAdapter->property( Person, ItemIndex ).data();

    DYNAMICACTION_ServiceActionAdapter::KState State =
        !isAvailableFor(Person,ItemIndex)  ? DYNAMICACTION_ServiceActionAdapter::Unknown :
        DYNAMICACTION_ServiceActionAdapter::Some;

    if( !(Flags & PropertyService::ReferItem) )
        Data = QString::null;

    return new DYNAMICACTION_ServiceActionAdapter( State, Data );
}

bool DYNAMICACTION_Service::isAvailable() const
{
    return ( TypeAdapter != 0 );
}

bool DYNAMICACTION_Service::isAvailableFor( const KABC::Addressee &Person, int ItemIndex ) const
{
    if( !TypeAdapter )
        return false;

    QString Data = TypeAdapter->property( Person, ItemIndex ).data();

    // for the example we take the first char as alphanumeric and test for the eveness of it
    bool Even = Data.isEmpty() || ( Data[0].unicode() % 2 == 0 ) ;

    return  (Even?2:1) & AvailableIndexFlag;
}

bool DYNAMICACTION_Service::supports( const KABC::Addressee &Person, int ItemIndex ) const
{
    bool IsSupported = false;
    if( TypeAdapter )
    {
        int MaxItemIndex;
        if( ItemIndex == -1 )
        {
            ItemIndex = 0;
            MaxItemIndex = Adapter->numberOfItems( Person );
        }
        else
            MaxItemIndex = ItemIndex+1;

        for( ; ItemIndex<MaxItemIndex; ++ItemIndex )
        {
            QString Data = TypeAdapter->property( Person, ItemIndex ).data();
            if( !Data.isEmpty() )
            {
                IsSupported = true;
                break;
            }
        }
    }
    return IsSupported;
}

void DYNAMICACTION_Service::registerClient( PropertyActionServiceClient* Client, int I )
{
    if( supports(Client->person(),I) )
        Clients.append( PropertyActionServiceClientForItem(Client,I) );
}


void DYNAMICACTION_Service::unregisterClient( PropertyActionServiceClient* Client, int ItemIndex )
{
    // try to remove the client even if it's not registered
    Clients.remove( PropertyActionServiceClientForItem(Client,ItemIndex) );
}


void DYNAMICACTION_Service::setAdapter( PropertyAdapter *Adapter )
{
    TypeAdapter = static_cast<TYPE_PropertyAdapter*>( Adapter->qt_cast("TYPE_PropertyAdapter") );

    PropertyActionService::setAdapter( Adapter );
}


void DYNAMICACTION_Service::onEvent()
{
    // note state of system, here made up
    if( AvailableIndexFlag < MaxIndexFlag )
        ++AvailableIndexFlag;
    else
        AvailableIndexFlag = 0;

    // inform affected clients
    if( TypeAdapter )
    for( PropertyActionServiceClientForItemList::Iterator ClientIt = Clients.begin();
         ClientIt != Clients.end(); ++ClientIt )
    {
        int ItemIndex = (*ClientIt).ItemIndex;
        // loop over registered items
        int MaxItemIndex;
        if( ItemIndex == -1 )
        {
            ItemIndex = 0;
            MaxItemIndex = Adapter->numberOfItems( (*ClientIt).Client->person() );
        }
        else
            MaxItemIndex = ItemIndex+1;

        for( ; ItemIndex<MaxItemIndex; ++ItemIndex )
        {
            QString Data = TypeAdapter->property( (*ClientIt).Client->person(), ItemIndex ).data();
            // uneven, or also even affected?
            bool Even = Data.isEmpty() || ( Data[0].unicode() % 2 == 0 ) ;

            if( !Even || (AvailableIndexFlag%2==0) )
                (*ClientIt).Client->onActionServiceStateChange( *this, PropertyActionServiceClient::Unknown, ItemIndex );
        }
    }
}

DYNAMICACTION_Service::~DYNAMICACTION_Service() {}

#include "dynamicactionservice.moc"



// kde specific
#include <kgenericfactory.h>

typedef K_TYPELIST_1( DYNAMICACTION_Service ) Product;
K_EXPORT_COMPONENT_FACTORY( khalkhipropertyactionservice_dynamicaction, KGenericFactory<Product>("khalkhipropertyactionservice_dynamicaction") )
