/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/



#ifndef TYPE_PROPERTYADAPTER_H
#define TYPE_PROPERTYADAPTER_H


// khalkhi core specific
#include <propertyadapter.h>
// property specific
#include "typepropertyadapterif.h"


class TYPE_PropertyAdapter : public PropertyAdapter, public TYPE_PropertyAdapterIf
{
    Q_OBJECT
public:
    // all parameters are required by the plugin loading mechanism,
    // although there is no use for the khalkhi framework for now
    TYPE_PropertyAdapter( QObject* Parent, const char* Name, const QStringList& Arguments );
    virtual ~TYPE_PropertyAdapter();

public: // PropertyAdapter API
    virtual int numberOfItems( const KABC::Addressee &Person ) const;
    virtual PropertyItem propertyItemOf( const KABC::Addressee &Person, int ItemIndex ) const;

public: // TYPE_PropertyAdapterIf API
    virtual PropertType property( const KABC::Addressee &Person, int ItemIndex ) const;
};

#endif
