/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// qt specific
#include <qtimer.h>
// kde specific
#include <kabc/addressee.h>
#include <kmessagebox.h>
#include <klocale.h>
// service specific
#include "dynamicdataactionserviceactionadapter.h"
#include "dynamicdataactionservice.h"

static const int MaxIndexFlag = 3;


DYNAMICDATAACTION_Service::DYNAMICDATAACTION_Service( QObject* Parent, const char* Name, const QStringList& Arguments )
: PropertyDataActionService( Parent, Name, Arguments ),
AvailableIndexFlag( MaxIndexFlag )
{
    QTimer *Timer = new QTimer( this );
    connect( Timer, SIGNAL(timeout()), SLOT(onEvent()) );
    Timer->start( 1000 );
}

ServiceAction DYNAMICDATAACTION_Service::action( const KABC::Addressee &Person, int ItemIndex,
                                                        QMimeSource *Data, int Flags ) const
{
    QString Name;
    if( TypeAdapter ) Name = TypeAdapter->property( Person, ItemIndex ).data();

    DYNAMICDATAACTION_ServiceActionAdapter::KState State =
        !isAvailableFor(Data,Person,ItemIndex)  ? DYNAMICDATAACTION_ServiceActionAdapter::Unknown :
        DYNAMICDATAACTION_ServiceActionAdapter::Some;

    if( !(Flags & PropertyService::ReferItem) )
        Name = QString::null;

    return new DYNAMICDATAACTION_ServiceActionAdapter( State, Name );
}


void DYNAMICDATAACTION_Service::execute( const KABC::Addressee &Person, int ItemIndex, QMimeSource *Data )
{
    QString PropertyData = TypeAdapter->property( Person, ItemIndex ).data();
    KMessageBox::information( 0, i18n("I, %1, got %2 for more consumption.").arg(PropertyData,QString::number( Data->serialNumber() )) );
}


bool DYNAMICDATAACTION_Service::supports( QMimeSource */*Data*/ ) const
{
    // taking all in the example
    return true;
}

bool DYNAMICDATAACTION_Service::isAvailableFor( QMimeSource */*Data*/, const KABC::Addressee &Person, int ItemIndex ) const
{
    // for the example we do not care about the mimesource
    if( !TypeAdapter )
        return false;

    QString Data = TypeAdapter->property( Person, ItemIndex ).data();

    // for the example we take the first char as alphanumeric and test for the eveness of it
    bool Even = Data.isEmpty() || ( Data[0].unicode() % 2 == 0 ) ;

    return  (Even?2:1) & AvailableIndexFlag;
}

void DYNAMICDATAACTION_Service::registerClient( PropertyDataActionServiceClient* Client, int ItemIndex )
{
    if( supports(Client->dataSource()) )
        Clients.append( PropertyDataActionServiceClientForItem(Client,ItemIndex) );
}


void DYNAMICDATAACTION_Service::unregisterClient( PropertyDataActionServiceClient* Client, int ItemIndex )
{
    Clients.remove( PropertyDataActionServiceClientForItem(Client,ItemIndex) );
}

void DYNAMICDATAACTION_Service::setAdapter( PropertyAdapter *Adapter )
{
    TypeAdapter = static_cast<TYPE_PropertyAdapter*>( Adapter->qt_cast("TYPE_PropertyAdapter") );

    PropertyDataActionService::setAdapter( Adapter );
}


void DYNAMICDATAACTION_Service::onEvent()
{
    // note state of system, here made up
    if( AvailableIndexFlag < MaxIndexFlag )
        ++AvailableIndexFlag;
    else
        AvailableIndexFlag = 0;

    // inform affected clients
    if( TypeAdapter )
    for( PropertyDataActionServiceClientForItemList::Iterator ClientIt = Clients.begin();
         ClientIt != Clients.end(); ++ClientIt )
    {
        int ItemIndex = (*ClientIt).ItemIndex;
        // loop over registered items
        int MaxItemIndex;
        if( ItemIndex == -1 )
        {
            ItemIndex = 0;
            MaxItemIndex = Adapter->numberOfItems( (*ClientIt).Client->person() );
        }
        else
            MaxItemIndex = ItemIndex+1;

        for( ; ItemIndex<MaxItemIndex; ++ItemIndex )
        {
            QString Data = TypeAdapter->property( (*ClientIt).Client->person(), ItemIndex ).data();
            // uneven, or also even affected?
            bool Even = Data.isEmpty() || ( Data[0].unicode() % 2 == 0 ) ;

            if( !Even || (AvailableIndexFlag%2==0) )
                (*ClientIt).Client->onDataActionServiceStateChange(
                    *this, PropertyDataActionServiceClient::Unknown, ItemIndex );
        }
    }
}

DYNAMICDATAACTION_Service::~DYNAMICDATAACTION_Service() {}

#include "dynamicdataactionservice.moc"


// kde specific
#include <kgenericfactory.h>

typedef K_TYPELIST_1( DYNAMICDATAACTION_Service ) Product;
K_EXPORT_COMPONENT_FACTORY( khalkhipropertydataactionservice_dynamicaction, KGenericFactory<Product>("khalkhipropertydataactionservice_dynamicaction") )
