/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// kde specific
#include <kabc/addressee.h>
#include <kmessagebox.h>
#include <klocale.h>
// khalkhi core specific
#include <propertyadapter.h>
// service specific
#include "dataactionserviceactionadapter.h"
#include "dataactionservice.h"


DATAACTION_Service::DATAACTION_Service( QObject* Parent, const char* Name, const QStringList& Arguments )
: PropertyDataActionService( Parent, Name, Arguments )
{
}


ServiceAction DATAACTION_Service::action( const KABC::Addressee &Person, int ItemIndex,
                                                        QMimeSource */*Event*/, int Flags ) const
{
    QString PropertyData = TypeAdapter->property( Person, ItemIndex ).data();

    DATAACTION_ServiceActionAdapter::KState State =
        PropertyData.isEmpty()  ? DATAACTION_ServiceActionAdapter::Unknown :
        DATAACTION_ServiceActionAdapter::Some;

    if( !(Flags & PropertyService::ReferItem) )
        PropertyData = QString::null;

    return new DATAACTION_ServiceActionAdapter( State, PropertyData );
}


void DATAACTION_Service::execute( const KABC::Addressee &Person, int ItemIndex, QMimeSource *Data )
{
    QString PropertyData = TypeAdapter->property( Person, ItemIndex ).data();
    KMessageBox::information( 0, i18n("I, %1, got %2 for consumption.").arg(PropertyData,QString::number( Data->serialNumber() )) );
}


bool DATAACTION_Service::supports( QMimeSource */*Data*/ ) const
{
    // taking all in the example
    return true;
}


void DATAACTION_Service::setAdapter( PropertyAdapter *Adapter )
{
    TypeAdapter = static_cast<TYPE_PropertyAdapter*>( Adapter->qt_cast("TYPE_PropertyAdapter") );

    PropertyDataActionService::setAdapter( Adapter );
}


DATAACTION_Service::~DATAACTION_Service() {}


// kde specific
#include <kgenericfactory.h>

typedef K_TYPELIST_1( DATAACTION_Service ) Product;
K_EXPORT_COMPONENT_FACTORY( khalkhipropertydataactionservice_action, KGenericFactory<Product>("khalkhipropertydataactionservice_action") )
