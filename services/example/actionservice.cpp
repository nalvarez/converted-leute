/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// kde specific
#include <kmessagebox.h>
#include <klocale.h>
// khalkhi core specific
#include <propertyadapter.h>
// service specific
#include "actionserviceactionadapter.h"
#include "actionservice.h"


ACTION_Service::ACTION_Service( QObject* Parent, const char* Name, const QStringList& Arguments )
: PropertyActionService( Parent, Name, Arguments ), TypeAdapter( 0 )
{
}

// this method invokes the action on the supported property with the given index
void ACTION_Service::execute( const KABC::Addressee &Person, int ItemIndex )
{
    QString Data = TypeAdapter->property( Person, ItemIndex ).data();
    KMessageBox::information( 0, i18n( "Look at me: %1" ).arg(Data) );
}

ServiceAction ACTION_Service::action( const KABC::Addressee &Person, int ItemIndex, int Flags ) const
{
    QString Data;
    if( TypeAdapter ) Data = TypeAdapter->property( Person, ItemIndex ).data();

    ACTION_ServiceActionAdapter::KState State =
        Data.isEmpty()  ? ACTION_ServiceActionAdapter::Unknown :
        ACTION_ServiceActionAdapter::Some;

    if( !(Flags & PropertyService::ReferItem) )
        Data = QString::null;

    return new ACTION_ServiceActionAdapter( State, Data );
}

bool ACTION_Service::isAvailable() const
{
    return TypeAdapter != 0;
}

void ACTION_Service::setAdapter( PropertyAdapter *Adapter )
{
    TypeAdapter = static_cast<TYPE_PropertyAdapter*>( Adapter->qt_cast("TYPE_PropertyAdapter") );

    PropertyActionService::setAdapter( Adapter );
}


ACTION_Service::~ACTION_Service() {}


// kde specific
#include <kgenericfactory.h>

typedef K_TYPELIST_1( ACTION_Service ) Product;
K_EXPORT_COMPONENT_FACTORY( khalkhipropertyactionservice_action, KGenericFactory<Product>("khalkhipropertyactionservice_action") )
