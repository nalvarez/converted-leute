/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


#ifndef STATUSSERVICE_H
#define STATUSSERVICE_H


// khalkhi core
#include <propertystatusservice.h>
#include <propertystatusserviceclientforitemlist.h>
// type specific
#include "typepropertyadapter.h"


struct STATUS_ServiceClientForItem : public PropertyStatusServiceClientForItem
{
    int Data;

    STATUS_ServiceClientForItem() {}
    STATUS_ServiceClientForItem( PropertyStatusServiceClient* C, int I, int D )
    : PropertyStatusServiceClientForItem( C, I ), Data( D ) {}
    STATUS_ServiceClientForItem( PropertyStatusServiceClient* C, int I )
    : PropertyStatusServiceClientForItem( C, I ) {}
    bool operator==( const STATUS_ServiceClientForItem& Other ) const
        { return PropertyStatusServiceClientForItem::operator==(Other); }
};

typedef QValueList<STATUS_ServiceClientForItem> STATUS_ServiceClientForItemList;


class STATUS_Service : public PropertyStatusService
{
    Q_OBJECT
public:
    STATUS_Service( QObject* Parent, const char* Name, const QStringList& Arguments );
    virtual ~STATUS_Service();

public: // PropertyActionService API
    virtual Status status( const KABC::Addressee &Person, int ItemIndex, int Flags ) const;

    virtual void registerClient( PropertyStatusServiceClient* Client, int ItemIndex = -1 );
    virtual void unregisterClient( PropertyStatusServiceClient* Client, int ItemIndex = -1 );

    virtual bool supports( const KABC::Addressee &Person, int ItemIndex ) const;

    virtual void setAdapter( PropertyAdapter *Adapter );

protected slots:
    void onEvent();

protected:
    STATUS_ServiceClientForItemList Clients;

    TYPE_PropertyAdapter *TypeAdapter;
};

#endif
