/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


#ifndef TYPE_PROPERTYITEMADAPTER_H
#define TYPE_PROPERTYITEMADAPTER_H


// khalkhi core specific
#include <propertyitemadapter.h>


class TYPE_PropertyItemAdapter : public PropertyItemAdapter
{
public:
    TYPE_PropertyItemAdapter( const QString &TypeData );
    virtual ~TYPE_PropertyItemAdapter();

public: // Property API
    virtual QVariant data( int Role ) const;

protected:
    QString TypeData;
};


inline TYPE_PropertyItemAdapter::TYPE_PropertyItemAdapter( const QString &Data )
: TypeData( Data )
{}

inline TYPE_PropertyItemAdapter::~TYPE_PropertyItemAdapter() {}


#endif
