/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// qt specific
#include <qstringlist.h>
// kde specific
#include <kabc/addressee.h>
// lib specific
#include "typepropertyitemadapter.h"
#include "typepropertyadapter.h"


TYPE_PropertyAdapter::TYPE_PropertyAdapter( QObject* Parent, const char* Name, const QStringList& Arguments )
: PropertyAdapter( Parent, Name, Arguments )
{}


int TYPE_PropertyAdapter::numberOfItems( const KABC::Addressee &Person ) const
{
    return QStringList::split( ' ', Person.name() ).size();
}


PropertyItem TYPE_PropertyAdapter::propertyItemOf( const KABC::Addressee &Person, int ItemIndex ) const
{
  return new TYPE_PropertyItemAdapter( TYPE_PropertyAdapter::property(Person,ItemIndex).data() );
}


PropertType TYPE_PropertyAdapter::property( const KABC::Addressee &Person, int ItemIndex ) const
{
    const QStringList Names = QStringList::split( ' ', Person.name() );

    return Names[ItemIndex];
}


TYPE_PropertyAdapter::~TYPE_PropertyAdapter() {}

#include "typepropertyadapter.moc"


// kde specific
#include <kgenericfactory.h>

typedef K_TYPELIST_1( TYPE_PropertyAdapter ) Product;
K_EXPORT_COMPONENT_FACTORY( khalkhiproperty_type, KGenericFactory<Product>("khalkhiproperty_type") )
