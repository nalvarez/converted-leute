/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


#ifndef GETDATEINFOSERVICE_H
#define GETDATEINFOSERVICE_H


// khalkhi core specific
#include <propertyactionservice.h>


// This template is useful for action services which are static and operate on all instances of a type
// like copy to clipboard, browse to url, create new email to address, etc.
// If the service depends on certain attributes, like fax for phone, or on the state of the system,
// take the dynamicactionservice template as example.
class GetDateInfoService : public PropertyActionService
{
public:
    GetDateInfoService( QObject* Parent, const char* Name, const QStringList& Arguments );
    virtual ~GetDateInfoService();

public: // PropertyActionService API
    virtual void execute( const KABC::Addressee &Person, int ItemIndex );
    virtual ServiceAction action( const KABC::Addressee &Person, int ItemIndex, int Flags ) const;
};

#endif
