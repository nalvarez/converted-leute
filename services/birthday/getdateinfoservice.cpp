/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// kde specific
#include <kabc/addressee.h>
#include <kconfig.h>
#include <kapp.h>
#include <kglobal.h>
#include <klocale.h>
// khalkhi core specific
#include <propertyadapter.h>
// service specific
#include "getdateinfoserviceactionadapter.h"
#include "getdateinfoservice.h"


GetDateInfoService::GetDateInfoService( QObject* Parent, const char* Name, const QStringList& Arguments )
: PropertyActionService( Parent, Name, Arguments )
{
    KGlobal::locale()->insertCatalogue( "khalkhi_birthday" );
}

void GetDateInfoService::execute( const KABC::Addressee &Person, int /*ItemIndex*/ )
{
    KConfig Config( "khalkhipropertyactionservice_getdateinforc" );
    Config.setGroup( "General" );

    const QString DateFormat = Config.readEntry( "DateFormat" );
    const QString URLFormat = Config.readEntry( "URLFormat" );

    const QDate Date = Person.birthday().date();

    KLocale *Locale = KGlobal::locale();
    const QString OldDateFormat = Locale->dateFormat();
    Locale->setDateFormat( DateFormat );

    const QString URL = URLFormat.arg( Locale->formatDate(Date) );

    Locale->setDateFormat( OldDateFormat );

    KApplication::startServiceByDesktopName( "konqueror", URL );
}

ServiceAction GetDateInfoService::action( const KABC::Addressee& /*Person*/, int /*ItemIndex*/,
                                                         int /*Flags*/ ) const
{
    return new GetDateInfoServiceActionAdapter();
}


GetDateInfoService::~GetDateInfoService()
{
    KGlobal::locale()->removeCatalogue( "khalkhi_birthday" );
}


// kde specific
#include <kgenericfactory.h>

typedef K_TYPELIST_1( GetDateInfoService ) Product;
K_EXPORT_COMPONENT_FACTORY( khalkhipropertyactionservice_getdateinfo, KGenericFactory<Product>("khalkhipropertyactionservice_getdateinfo") )
