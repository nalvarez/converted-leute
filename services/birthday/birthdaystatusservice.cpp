/*
    This file is part of the KDE project.
    Copyright (c) 2006-2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// qt specific
#include <qpixmap.h>
#include <qdatetime.h>
#include <qtimer.h>
// kde specific
#include <kabc/addressee.h>
#include <kglobal.h>
#include <klocale.h>
#include <kiconloader.h>
#include <kapp.h>
// khalkhi core
#include <propertystatusserviceclient.h>
#include <statuschange.h>
#include <propertyadapter.h>
// service specific
#include "birthdaystatusservice.h"



static int daysToMonthDay( const QDate &AncientDate, int MaxDiff = 10 )
{
    const QDate CurrentDate = QDate::currentDate();

    QDate RecurrenceDate( CurrentDate.year(), AncientDate.month(), AncientDate.day() );
    int Diff = CurrentDate.daysTo( RecurrenceDate );

    return 0 > Diff || Diff > MaxDiff ? -1 : Diff;
}


class BirthdayStatusAdapter : public StatusAdapter
{
public:
    BirthdayStatusAdapter( int Days );
public:
    QVariant data( int Role ) const;

protected:
    int DaysToBirthday;
};

BirthdayStatusAdapter::BirthdayStatusAdapter( int Days ) : DaysToBirthday( Days ) {}

QVariant BirthdayStatusAdapter::data( int Role ) const
{
    QVariant Result;

    switch( Role )
    {
    case DisplayTextRole:
        Result = ( DaysToBirthday == 0 ) ? i18n( "Has birthday today." ) :
                 i18n( "Has birthday in one day.", "Has birthday in %n days.", DaysToBirthday );
        break;
    case DisplayIconRole:  // TODO: how control imagesize?
        if( DaysToBirthday == 0 )
            Result = KGlobal::iconLoader()->loadIcon("cookie",KIcon::Small/*,Icon->symbolSize()*/).convertToImage();
        break;
    default:
        ;
    }
    return Result;
}



BirthdayStatusService::BirthdayStatusService( QObject* Parent, const char* Name, const QStringList& Arguments )
: PropertyStatusService( Parent, Name, Arguments )
{
    KGlobal::locale()->insertCatalogue( "khalkhi_birthday" );
    startTimer();
}


Status BirthdayStatusService::status( const KABC::Addressee &Person, int /*ItemIndex*/,
                                                     int /*Flags*/ ) const
{
    StatusAdapter *Result;
    const QDate &Birthdate = Person.birthday().date();
    const int DaysToBirthday = daysToMonthDay( Birthdate );

    Result = ( Birthdate.isValid() && DaysToBirthday >= 0 ) ? new BirthdayStatusAdapter( DaysToBirthday ) :
             new StatusAdapter();

    return Result;
}


bool BirthdayStatusService::supports( const KABC::Addressee &Person, int /*ItemIndex*/ ) const
{
    return Person.birthday().date().isValid();
}


void BirthdayStatusService::registerClient( PropertyStatusServiceClient* Client, int ItemIndex )
{
    const QDate &Birthdate= Client->person().birthday().date();

    // register only valid birthdates
    if( Birthdate.isValid() )
    {
        int State = daysToMonthDay(Birthdate) >= 0 ? BirthdayActive : BirthdayPassive;

        Clients.append( BirthdayStatusServiceClientForItem(Client,ItemIndex,State) );
    }
}


void BirthdayStatusService::unregisterClient( PropertyStatusServiceClient* Client, int ItemIndex )
{
    Clients.remove( BirthdayStatusServiceClientForItem(Client,ItemIndex) );
}

void BirthdayStatusService::startTimer()
{
    // msecsTo only works between 0 and 24 hours, so add some more to get to next day
    int MilliSecondsToAlmostMidnight = QTime::currentTime().msecsTo( QTime(23,59,59,999) );
    const int SomeMilliSecondsIntoNextDay = 1000;
    QTimer::singleShot( MilliSecondsToAlmostMidnight + SomeMilliSecondsIntoNextDay, this, SLOT(onNewDay()) );
}

void BirthdayStatusService::onNewDay()
{
    BirthdayStatusServiceClientForItemList::Iterator ClientIt = Clients.begin();
    while( ClientIt != Clients.end() )
    {
        const QDate &Birthdate= (*ClientIt).Client->person().birthday().date();
        const int DaysToBirthday = daysToMonthDay( Birthdate );

        bool GetsDateSignal = DaysToBirthday >= 0;
        if( GetsDateSignal || (*ClientIt).NotificationState == BirthdayActive )
        {
            const bool BirthdayNotPassed = ( DaysToBirthday >= 0 );
            StatusAdapter *StateAdapter =  BirthdayNotPassed ?
                new BirthdayStatusAdapter( DaysToBirthday ) :
                new StatusAdapter();
            StatusAdapter *ChangeAdapter = BirthdayNotPassed ?
                new BirthdayStatusAdapter( DaysToBirthday ) :
                new StatusAdapter();

            (*ClientIt).Client->onStateChange( *this, StateAdapter, ChangeAdapter, 0 );
        }
        (*ClientIt).NotificationState = GetsDateSignal ? BirthdayActive : BirthdayPassive;

        ++ClientIt;
    }

    startTimer();
}


BirthdayStatusService::~BirthdayStatusService()
{
    KGlobal::locale()->removeCatalogue( "khalkhi_birthday" );
}

#include "birthdaystatusservice.moc"


// kde specific
#include <kgenericfactory.h>

typedef K_TYPELIST_1( BirthdayStatusService ) Product;
K_EXPORT_COMPONENT_FACTORY( khalkhipropertystatusservice_hasbirthday, KGenericFactory<Product>("khalkhipropertystatusservice_hasbirthday") )
