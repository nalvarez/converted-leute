/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// qt specific
#include <qapplication.h>
#include <qclipboard.h>
// kde specific
#include <kabc/addressee.h>
#include <klocale.h>
#include <kglobal.h>
// service specific
#include "copybirthdayserviceactionadapter.h"
#include "copybirthdayservice.h"


CopyBirthdayService::CopyBirthdayService( QObject* Parent, const char* Name, const QStringList& Arguments )
: PropertyActionService( Parent, Name, Arguments )
{
    KGlobal::locale()->insertCatalogue( "khalkhi_birthday" );
}

void CopyBirthdayService::execute( const KABC::Addressee &Person, int /*ItemIndex*/ )
{
    // Copy text into the clipboard
    const QString DateString = KGlobal::locale()->formatDate( Person.birthday().date(), true );
    QApplication::clipboard()->setText( DateString, QClipboard::Clipboard );
}

ServiceAction CopyBirthdayService::action( const KABC::Addressee& /*Person*/, int /*ItemIndex*/, int /*Flags*/ ) const
{
    return new CopyBirthdayServiceActionAdapter( /*Flags&ReferItem ? Person.note() : */QDate() );
}

CopyBirthdayService::~CopyBirthdayService()
{
    KGlobal::locale()->removeCatalogue( "khalkhi_birthday" );
}

// kde specific
#include <kgenericfactory.h>

typedef K_TYPELIST_1( CopyBirthdayService ) Product;
K_EXPORT_COMPONENT_FACTORY( khalkhipropertyactionservice_copybirthday, KGenericFactory<Product>("khalkhipropertyactionservice_copybirthday") )
