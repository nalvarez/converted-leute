/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


#ifndef BIRTHDAYSTATUSSERVICE_H
#define BIRTHDAYSTATUSSERVICE_H


// khalkhi core specific
#include <propertystatusservice.h>
#include <propertystatusserviceclientforitemlist.h>

using namespace Khalkhi;


struct BirthdayStatusServiceClientForItem : public PropertyStatusServiceClientForItem
{
    int NotificationState;

    BirthdayStatusServiceClientForItem() : NotificationState( -1 ) {}
    BirthdayStatusServiceClientForItem( PropertyStatusServiceClient* C, int I, int S )
    : PropertyStatusServiceClientForItem( C, I ), NotificationState( S ) {}
    BirthdayStatusServiceClientForItem( PropertyStatusServiceClient* C, int I )
    : PropertyStatusServiceClientForItem( C, I ), NotificationState( -1 ) {}
    bool operator==( const BirthdayStatusServiceClientForItem& Other ) const
        { return PropertyStatusServiceClientForItem::operator==(Other); }
};

typedef QValueList<BirthdayStatusServiceClientForItem> BirthdayStatusServiceClientForItemList;


class BirthdayStatusService : public PropertyStatusService
{
    Q_OBJECT
public:
    enum NotificationState { BirthdayPassive, BirthdayActive };

public:
    BirthdayStatusService( QObject* Parent, const char* Name, const QStringList& Arguments );
    virtual ~BirthdayStatusService();

public: // PropertyActionService API
    virtual Status status( const KABC::Addressee &Person, int ItemIndex, int Flags ) const;

    virtual bool supports( const KABC::Addressee &Person, int ItemIndex ) const;

    virtual void registerClient( PropertyStatusServiceClient* Client, int ItemIndex = -1 );
    virtual void unregisterClient( PropertyStatusServiceClient* Client, int ItemIndex = -1 );

protected:
    void startTimer();

protected slots:
    void onNewDay();

protected:
    BirthdayStatusServiceClientForItemList Clients;
};

#endif
