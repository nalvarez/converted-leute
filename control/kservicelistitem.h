/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


#ifndef KSERVICELISTITEML_H
#define KSERVICELISTITEML_H


// Qt
#include <qlistbox.h>
// KDE
#include <kservice.h>

enum { ActionServiceId = 0, DataActionServiceId = 1, StatusServiceId = 2 };

class KServiceListItem : public QListBoxText
{
public:
    explicit KServiceListItem( const KService::Ptr &Service, bool IsMain = false );
    virtual ~KServiceListItem();

public: // QListBoxItem API
    virtual void paint( QPainter *Painter );
    virtual int width( const QListBox* ListBox ) const;

public:
    void setMain( bool IsMain );
    bool configure( QWidget *Parent );

public:
    KService::Ptr service() const;
    bool isMain() const;
    bool isConfigurable() const;

private:
    KService::Ptr Service;
    bool IsConfigurable : 1;
    bool IsMain : 1;
};

inline KService::Ptr KServiceListItem::service() const { return Service; }
inline bool KServiceListItem::isConfigurable() const { return IsConfigurable; }

inline bool KServiceListItem::isMain() const { return IsMain; }
inline void KServiceListItem::setMain( bool IM ) { IsMain = IM; }

#endif
