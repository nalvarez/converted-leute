/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// qt
#include <qlayout.h>
#include <qlistbox.h>
#include <qpushbutton.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
// kde
#include <kdialog.h>
#include <knotifyclient.h>
#include <klocale.h>
// module
#include "kservicelistitem.h"
#include "kserviceselectdialog.h"
#include "kpropertystatuslistcontrol.h"


static inline QString serviceId( const KService *Service )
{
    return Service->property( QString::fromLatin1("X-KDE-StatusService") ).toString();
}

KPropertyStatusListControl::KPropertyStatusListControl( QWidget *Parent, const char *Name )
 : KSortedListBox( Parent, Name, Add|UpDown|Action1|Remove )
{
    setButtonAction1( i18n("Configure...") );

    connect( ListBox, SIGNAL(highlighted(int)), SLOT(onItemHighlighted(int)) );

    const QString Description = i18n(
        " This is a list of status services associated with the selected property." );

    QWhatsThis::add( this, Description );
    QWhatsThis::add( ListBox, Description );

    QToolTip::add( NewButton,    i18n( "Adds a new status.") );
    QToolTip::add( UpButton,     i18n( "Moves the current status up in the list.") );
    QToolTip::add( DownButton,   i18n( "Moves the current status down in the list.") );
    QToolTip::add( RemoveButton, i18n( "Removes the current status from the list.") );
}

// TODO: to be optimized
void KPropertyStatusListControl::setStatusServices( const KService::List &StatusServices,
                                            const QStringList &SortedStatusIds, const QStringList &HiddenStatusIds )
{
    HiddenStatusServices = StatusServices;

    QPtrList<QListBoxItem> ItemList;

    // add sorted items
    for( QStringList::ConstIterator sit = SortedStatusIds.begin(); sit != SortedStatusIds.end(); ++sit )
    {
        KService::List::Iterator endIt = HiddenStatusServices.end();
        for( KService::List::Iterator it = HiddenStatusServices.begin(); it != endIt; ++it )
        {
            const QString StatusId = serviceId( *it );
            if( StatusId == *sit )
            {
                ItemList.append( new KServiceListItem(*it) );
                HiddenStatusServices.erase( it );
                break;
            }
        }
    }
    // add unsorted items
    for( KService::List::Iterator it = HiddenStatusServices.begin(); it != HiddenStatusServices.end(); )
    {
        const QString StatusId = serviceId( *it );
        QStringList::ConstIterator sit = HiddenStatusIds.find( StatusId );
        // not hidden?
        if( sit == HiddenStatusIds.end() )
        {
            ItemList.append( new KServiceListItem(*it) );
            it = HiddenStatusServices.erase( it );
        }
        else
            ++it;
    }

    setItemList( ItemList );

    NewButton->setEnabled( HiddenStatusServices.size() > 0 );
}

QStringList KPropertyStatusListControl::sortedStatusIds() const
{
    QStringList Result;
    const unsigned int Count = count();

    for( unsigned int i = 0; i<Count; ++i )
    {
        KServiceListItem *Item = static_cast<KServiceListItem *>( ListBox->item(i) );
        Result.append( serviceId(Item->service()) );
    }

    return Result;
}

QStringList KPropertyStatusListControl::hiddenStatusIds() const
{
    QStringList Result;

    for( KService::List::ConstIterator it = HiddenStatusServices.begin(); it != HiddenStatusServices.end(); ++it )
        Result.append( serviceId(*it) );

    return Result;
}

void KPropertyStatusListControl::onItemHighlighted( int Index )
{
    KServiceListItem *Item = static_cast<KServiceListItem *>( ListBox->item(Index) );

    Action1Button->setEnabled( Item->isConfigurable() );

    emit selected( serviceId(Item->service()) );
}

QListBoxItem *KPropertyStatusListControl::createItem()
{

    KService::Ptr Service = 0;
    KServiceSelectDialog Dialog( i18n("Status Selection"), i18n("Select an status:"), this );
    Dialog.setServices( HiddenStatusServices );
    if( Dialog.exec() != QDialog::Accepted )
        return 0;
    Service = Dialog.service();

    if( !Service )
        return 0;

    HiddenStatusServices.remove( Service );

    return new KServiceListItem( Service );
}

bool KPropertyStatusListControl::action1Item( QListBoxItem *I )
{
    bool Result = false;

    KServiceListItem *Item = static_cast<KServiceListItem *>( I );
    if( Item->configure(this) )
        emit configChanged( StatusServiceId, serviceId(Item->service()) );

    return Result;
}

bool KPropertyStatusListControl::deleteItem( QListBoxItem *I )
{
    bool Result = true;

    KServiceListItem *Item = static_cast<KServiceListItem *>( I );
    HiddenStatusServices.append( Item->service() );
    NewButton->setEnabled( true );

    return Result;
}

KPropertyStatusListControl::~KPropertyStatusListControl() {}

#include "kpropertystatuslistcontrol.moc"
