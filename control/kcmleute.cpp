/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

// qt
#include <qtimer.h>
#include <qlayout.h>
#include <qwidgetstack.h>
#include <qlabel.h>
// kde
#include <kdialog.h>
#include <kcursor.h>
#include <klocale.h>
#include <kglobal.h>
#include <kconfig.h>
#include <ktrader.h>
#include <kparts/componentfactory.h>
#include <ksycoca.h>
#include <dcopclient.h>
#include <kapp.h>
// module
#include "aboutdata.h"
#include "kactionlistcontrol.h"
#include "kdataactionlistcontrol.h"
#include "kstatuslistcontrol.h"
#include "kservicescontrol.h"
#include "kpropertyactionlistcontrol.h"
#include "kpropertydataactionlistcontrol.h"
#include "kpropertystatuslistcontrol.h"
#include "kpropertylistcontrol.h"
#include "kpropertyservicescontrol.h"
#include "kcmkhalkhi.h"

static const char DCOPConfigObjectName[] = "KhalkhiConfig";
static const char DCOPChangeSignalName[] = "changed()";
static const char DCOPServiceConfigChangeSignalName[] = "changed(int,QString,QString)";

static const int PropertyStartLabelId = 1;
static const int PropertyServicesControlId = 2;

static const char ConfigFileName[] = "khalkhirc";

static const char GeneralGroupId[] = "General";

static const char PropertiesGroupId[] =   "Properties";
static const char PropertiesSortingId[] = "Sorting";
static const char PropertiesHiddenId[] =  "Hidden";

static const char PropertyGroupIdTemplate[] = "Property:%1";

static const char ActionsSortingId[] =        "ActionsSorting";
static const char ActionsHiddenId[] =         "HiddenActions";
static const char ActionsMainId[] =           "MainActions";
static const char DataActionsSortingId[] =    "DataActionsSorting";
static const char DataActionsHiddenId[] =     "HiddenDataActions";
static const char DataActionsMainId[] =       "MainDataActions";
static const char StatesSortingId[] =         "StatesSorting";
static const char StatesHiddenId[] =          "HiddenStates";

static const char ABIVersionId[] =            "X-KDE-ABI-Version";
static const char ServiceABIVersion[] = "1";


static void emitDCOPChangeSignal()
{
    kapp->dcopClient()->emitDCOPSignal( DCOPConfigObjectName, DCOPChangeSignalName, QByteArray() );
}

static void emitDCOPServiceConfigChangeSignal( int ServiceType, const QString &PropertyId, const QString &ServiceId )
{
    QByteArray SignalData;
    QDataStream SignalDataStream( SignalData, IO_WriteOnly );
    SignalDataStream << ServiceType << PropertyId << ServiceId;
    kapp->dcopClient()->emitDCOPSignal( DCOPConfigObjectName, DCOPServiceConfigChangeSignalName, SignalData );
}

KhalkhiControlModule::KhalkhiControlModule( QWidget *Parent, const char *Name, const QStringList& )
 : KCModule( Parent, Name ), AboutData( new KhalkhiControlModuleAboutData ),
   ActionServicesDirty( false ), DataActionServicesDirty( false ), StatusServicesDirty( false ),
   PropertiesDirty( false ),
   PropertyActionServicesDirty( false ), PropertyDataActionServicesDirty( false ), PropertyStatusServicesDirty( false )
{
    QHBoxLayout *TopLayout = new QHBoxLayout( this, 0, KDialog::marginHint() );

    TabWidget = new QTabWidget( this );
    TabWidget->setMargin( KDialog::marginHint() );

    // General
    ServicesControl = new KServicesControl( TabWidget );
    connect( ServicesControl, SIGNAL(changed(bool)), SLOT(setServicesDirty(bool)) );
    connect( ServicesControl, SIGNAL(configChanged( int, const QString & )),
             SLOT(signalServiceConfigChange( int, const QString & )) );
    TabWidget->addTab( ServicesControl, i18n("&General") );

    // Properties
    QWidget *PropertiesPage = new QWidget( TabWidget );
    TabWidget->addTab( PropertiesPage, i18n("&Properties") );

        QHBoxLayout *PropertiesLayout = new QHBoxLayout( PropertiesPage, 0, KDialog::marginHint() );

        // left: list of properties
        PropertyListControl = new KPropertyListControl( PropertiesPage );
        connect( PropertyListControl, SIGNAL(selected( const QString& )), SLOT(onPropertySelect( const QString& )) );
        connect( PropertyListControl, SIGNAL(changed(bool)), SLOT(setPropertiesDirty(bool)) );

        // right: certain things
        WidgetStack = new QWidgetStack( PropertiesPage );

        // Property Services
        PropertyServicesControl = new KPropertyServicesControl( WidgetStack );
        connect( PropertyServicesControl, SIGNAL(changed(bool)), SLOT(setPropertyServicesDirty(bool)) );
        connect( PropertyServicesControl, SIGNAL(configChanged( int, const QString & )),
                 SLOT(signalPropertyServiceConfigChange( int, const QString & )) );

        // Widget shown on startup
        QLabel *StartLabel = new QLabel( i18n("Select a property"), WidgetStack );
        StartLabel->setAlignment( AlignCenter );

        WidgetStack->addWidget( PropertyServicesControl, PropertyServicesControlId);
        WidgetStack->addWidget( StartLabel, PropertyStartLabelId );
        WidgetStack->raiseWidget( PropertyStartLabelId );

    connect( KSycoca::self(), SIGNAL(databaseChanged()), SLOT(onDatabaseChange()) );

    PropertiesLayout->addWidget( PropertyListControl );
    PropertiesLayout->addWidget( WidgetStack );
    PropertiesLayout->setStretchFactor( WidgetStack, 2 );

    TopLayout->addWidget( TabWidget );

    load();
}

void KhalkhiControlModule::load()
{
    const QString PropertyConstraint = QString::fromLatin1( "[X-KDE-ABI-Version] == '%1'" )
        .arg( QString::fromLatin1(ServiceABIVersion) );

    KConfig Config( ConfigFileName );

    Config.setGroup( GeneralGroupId );
    const QStringList SortedActionIds = Config.readListEntry( ActionsSortingId );
    const QStringList HiddenActionIds = Config.readListEntry( ActionsHiddenId );

    const KTrader::OfferList ActionOffers = KTrader::self()->query( "khalkhi/actionservice", PropertyConstraint );

    ServicesControl->actionControl()->setServices( ActionOffers, SortedActionIds, HiddenActionIds );

    const QStringList SortedDataActionIds = Config.readListEntry( DataActionsSortingId );
    const QStringList HiddenDataActionIds = Config.readListEntry( DataActionsHiddenId );

    const KTrader::OfferList DataActionOffers = KTrader::self()->query( "khalkhi/dataactionservice", PropertyConstraint );

    ServicesControl->dataActionControl()->setServices( DataActionOffers, SortedDataActionIds, HiddenDataActionIds );

    const QStringList SortedStatusIds = Config.readListEntry( StatesSortingId );
    const QStringList HiddenStatusIds = Config.readListEntry( StatesHiddenId );

    const KTrader::OfferList StatusOffers = KTrader::self()->query( "khalkhi/statusservice", PropertyConstraint );

    ServicesControl->dataActionControl()->setServices( StatusOffers, SortedStatusIds, HiddenStatusIds );


    Config.setGroup( PropertiesGroupId );
    const QStringList SortedPropertyIds = Config.readListEntry( PropertiesSortingId );
    const QStringList HiddenPropertyIds = Config.readListEntry( PropertiesHiddenId );

    KTrader::OfferList PropertyOffers = KTrader::self()->query( "khalkhi/property", PropertyConstraint );

    PropertyListControl->setProperties( PropertyOffers, SortedPropertyIds, HiddenPropertyIds );
}


void KhalkhiControlModule::defaults()
{
    // insert your default settings code here...
    emit changed( true );
}


void KhalkhiControlModule::save()
{
    KConfig Config( ConfigFileName );

    if( ActionServicesDirty )
    {
        Config.setGroup( GeneralGroupId );
        Config.writeEntry( ActionsSortingId, ServicesControl->actionControl()->sortedIds() );
        Config.writeEntry( ActionsHiddenId, ServicesControl->actionControl()->hiddenIds() );

        ActionServicesDirty = false;
    }

    if( DataActionServicesDirty )
    {
        Config.setGroup( GeneralGroupId );
        Config.writeEntry( DataActionsSortingId, ServicesControl->dataActionControl()->sortedIds() );
        Config.writeEntry( DataActionsHiddenId, ServicesControl->dataActionControl()->hiddenIds() );

        DataActionServicesDirty = false;
    }

    if( StatusServicesDirty )
    {
        Config.setGroup( GeneralGroupId );
        Config.writeEntry( StatesSortingId, ServicesControl->statusControl()->sortedIds() );
        Config.writeEntry( StatesHiddenId, ServicesControl->statusControl()->hiddenIds() );

        StatusServicesDirty = false;
    }

    if( PropertiesDirty )
    {
        Config.setGroup( PropertiesGroupId );
        Config.writeEntry( PropertiesSortingId, PropertyListControl->sortedPropertyIds() );
        Config.writeEntry( PropertiesHiddenId, PropertyListControl->hiddenPropertyIds() );

        PropertiesDirty = false;
    }

    storeDirtyServices();
    for( QMap<QString,KMainedIds>::ConstIterator it = DirtyPropertyActionServiceIds.begin();
         it != DirtyPropertyActionServiceIds.end(); ++it )
    {
        const QString PropertyId = it.key();
        const KMainedIds &Ids = it.data();

        Config.setGroup( QString::fromLatin1(PropertyGroupIdTemplate).arg(PropertyId) );

        Config.writeEntry( ActionsSortingId, Ids.SortedIds);
        Config.writeEntry( ActionsHiddenId, Ids.HiddenIds );
        Config.writeEntry( ActionsMainId, Ids.MainIds );
    }
    DirtyPropertyActionServiceIds.clear();
    for( QMap<QString,KMainedIds>::ConstIterator it = DirtyPropertyDataActionServiceIds.begin();
         it != DirtyPropertyDataActionServiceIds.end(); ++it )
    {
        const QString PropertyId = it.key();
        const KMainedIds &Ids = it.data();

        Config.setGroup( QString::fromLatin1(PropertyGroupIdTemplate).arg(PropertyId) );

        Config.writeEntry( DataActionsSortingId, Ids.SortedIds);
        Config.writeEntry( DataActionsHiddenId, Ids.HiddenIds );
        Config.writeEntry( DataActionsMainId, Ids.MainIds );
    }
    DirtyPropertyDataActionServiceIds.clear();
    for( QMap<QString,KIds>::ConstIterator it = DirtyPropertyStatusServiceIds.begin();
         it != DirtyPropertyStatusServiceIds.end(); ++it )
    {
        const QString PropertyId = it.key();
        const KIds &Ids = it.data();

        Config.setGroup( QString::fromLatin1(PropertyGroupIdTemplate).arg(PropertyId) );

        Config.writeEntry( StatesSortingId, Ids.SortedIds);
        Config.writeEntry( StatesHiddenId, Ids.HiddenIds );
    }
    DirtyPropertyStatusServiceIds.clear();

    Config.sync();

    emitDCOPChangeSignal();
    emit changed( true );
}


int KhalkhiControlModule::buttons()
{
    return /*Default|*/Apply/*|Help*/;
}


void KhalkhiControlModule::configChanged()
{
    // insert your saving code here...
    emitDCOPChangeSignal();
    emit changed( true );
}


QString KhalkhiControlModule::quickHelp() const
{
    return i18n( "<h1>Person Properties and Services</h1>"
    " This module allows you to choose which of the properties of the"
    " persons in you addressbook are used and which services are offered."
    " <p>There are three kind of service types:"
    " <ul><li>Actions, e.g. starting a chat</li>"
    " <li>Actions on Data, e.g. sending a file via the chat system</li>"
    " <li>States, e.g. showing the presence in the chat system</li></ul>"
    " Services are available for a property of persons"
    " (e.g. for the addresses in a chat system)"
    " or for an entire person. To have an action service for a property also shown"
    " in menus on the entire person, set it to \"Global\", otherwise \"Local\"."
    " <p>Currently not all programs support these settings yet." );
}


const KAboutData *KhalkhiControlModule::aboutData() const
{
    return AboutData;
}


void KhalkhiControlModule::setServicesDirty( bool D )
{
    const QWidget *CurrentControl = ServicesControl->currentPage();
    if( CurrentControl == ServicesControl->actionControl() )
        ActionServicesDirty = D;
    else if( CurrentControl == ServicesControl->dataActionControl() )
        DataActionServicesDirty = D;
    else if( CurrentControl == ServicesControl->statusControl() )
        StatusServicesDirty = D;

    emit changed( D );
}

void KhalkhiControlModule::setPropertiesDirty( bool D )
{
    PropertiesDirty = D;
    emit changed( D );
}

void KhalkhiControlModule::setPropertyServicesDirty( bool D )
{
    const QWidget *CurrentControl = PropertyServicesControl->currentPage();
    if( CurrentControl == PropertyServicesControl->actionControl() )
        PropertyActionServicesDirty = D;
    else if( CurrentControl == PropertyServicesControl->dataActionControl() )
        PropertyDataActionServicesDirty = D;
    else if( CurrentControl == PropertyServicesControl->statusControl() )
        PropertyStatusServicesDirty = D;

    emit changed( D );
}

void KhalkhiControlModule::storeDirtyServices()
{
    if( PropertyActionServicesDirty )
    {
        KMainedIds &Ids = DirtyPropertyActionServiceIds[PropertyServicesControl->propertyId()];

        Ids.SortedIds = PropertyServicesControl->actionControl()->sortedActionIds();
        Ids.HiddenIds = PropertyServicesControl->actionControl()->hiddenActionIds();
        Ids.MainIds = PropertyServicesControl->actionControl()->mainActionIds();

        PropertyActionServicesDirty = false;
    }
    if( PropertyDataActionServicesDirty )
    {
        KMainedIds &Ids = DirtyPropertyDataActionServiceIds[PropertyServicesControl->propertyId()];

        Ids.SortedIds = PropertyServicesControl->dataActionControl()->sortedDataActionIds();
        Ids.HiddenIds = PropertyServicesControl->dataActionControl()->hiddenDataActionIds();
        Ids.MainIds = PropertyServicesControl->dataActionControl()->mainDataActionIds();

        PropertyDataActionServicesDirty = false;
    }
    if( PropertyStatusServicesDirty )
    {
        KIds &Ids = DirtyPropertyStatusServiceIds[PropertyServicesControl->propertyId()];

        Ids.SortedIds = PropertyServicesControl->statusControl()->sortedStatusIds();
        Ids.HiddenIds = PropertyServicesControl->statusControl()->hiddenStatusIds();

        PropertyStatusServicesDirty = false;
    }
}

void KhalkhiControlModule::signalServiceConfigChange( int ServiceTypeId, const QString &ServiceId )
{
    emitDCOPServiceConfigChangeSignal( ServiceTypeId, QString::null, ServiceId );
}

void KhalkhiControlModule::signalPropertyServiceConfigChange( int ServiceTypeId, const QString &ServiceId )
{
    emitDCOPServiceConfigChangeSignal( ServiceTypeId, PropertyServicesControl->propertyId(), ServiceId );
}

void KhalkhiControlModule::onPropertySelect( const QString& PropertyId )
{
    storeDirtyServices();
    WidgetStack->raiseWidget( PropertyServicesControlId );

    const QString PropertyConstraint =
        QString::fromLatin1( "([X-KDE-KhalkhiProperty] == '%1') and ([X-KDE-ABI-Version] == '%2')" )
            .arg( PropertyId, QString::fromLatin1(ServiceABIVersion) );

    KConfig Config( ConfigFileName );
    Config.setGroup( QString::fromLatin1(PropertyGroupIdTemplate).arg(PropertyId) );

    // action services
    QMap<QString,KMainedIds>::ConstIterator ait = DirtyPropertyActionServiceIds.find( PropertyId );
    const KMainedIds *AIds = ( ait != DirtyPropertyActionServiceIds.end() ) ? &ait.data() : 0;

    const QStringList SortedActionIds = AIds ? AIds->SortedIds : Config.readListEntry( ActionsSortingId );
    const QStringList HiddenActionIds = AIds ? AIds->HiddenIds : Config.readListEntry( ActionsHiddenId );
    const QStringList MainActionIds =   AIds ? AIds->MainIds : Config.readListEntry( ActionsMainId );

    const KTrader::OfferList ActionOffers =
        KTrader::self()->query( "khalkhi/propertyactionservice", PropertyConstraint );

    PropertyServicesControl->actionControl()->setActionServices( ActionOffers, SortedActionIds, HiddenActionIds,
                                                                 MainActionIds );

    // data action services
    QMap<QString,KMainedIds>::ConstIterator dait = DirtyPropertyDataActionServiceIds.find( PropertyId );
    const KMainedIds *DAIds = ( dait != DirtyPropertyDataActionServiceIds.end() ) ? &dait.data() : 0;

    const QStringList SortedDataActionIds = DAIds ? DAIds->SortedIds : Config.readListEntry( DataActionsSortingId );
    const QStringList HiddenDataActionIds = DAIds ? DAIds->HiddenIds : Config.readListEntry( DataActionsHiddenId );
    const QStringList MainDataActionIds =   DAIds ? DAIds->MainIds : Config.readListEntry( DataActionsMainId );

    const KTrader::OfferList DataActionOffers =
        KTrader::self()->query( "khalkhi/propertydataactionservice", PropertyConstraint );

    PropertyServicesControl->dataActionControl()->setDataActionServices( DataActionOffers,
                                                                         SortedDataActionIds, HiddenDataActionIds,
                                                                         MainDataActionIds );

    // status services
    QMap<QString,KIds>::ConstIterator sit = DirtyPropertyStatusServiceIds.find( PropertyId );
    const KIds *SIds = ( sit != DirtyPropertyStatusServiceIds.end() ) ? &sit.data() : 0;

    const QStringList SortedStatusIds = SIds ? SIds->SortedIds : Config.readListEntry( StatesSortingId );
    const QStringList HiddenStatusIds = SIds ? SIds->HiddenIds : Config.readListEntry( StatesHiddenId );

    const KTrader::OfferList StatusOffers =
        KTrader::self()->query( "khalkhi/propertystatusservice", PropertyConstraint );

    PropertyServicesControl->statusControl()->setStatusServices( StatusOffers, SortedStatusIds, HiddenStatusIds );

    PropertyServicesControl->setProperty( PropertyId );
}

void KhalkhiControlModule::onDatabaseChange()
{
#if 0
    if( KSycoca::self()->isChanged( "services" ) )
    {
    // ksycoca has new KService objects for us, make sure to update
    // our 'copies' to be in sync with it. Not important for OK, but
    // important for Apply (how to differentiate those 2?).
    // See BR 35071.
    QValueList<TypesListItem *>::Iterator it = m_itemsModified.begin();
    for( ; it != m_itemsModified.end(); ++it ) {
        QString name = (*it)->name();
        if ( removedList.find( name ) == removedList.end() ) // if not deleted meanwhile
            (*it)->refresh();
    }
    m_itemsModified.clear();
    }
#endif
}

KhalkhiControlModule::~KhalkhiControlModule()
{
    delete AboutData;
}


#include "kcmkhalkhi.moc"
