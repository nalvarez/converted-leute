/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// module specific
#include "aboutdata.h"


// CModule
static const char CModuleId[] =          "kcmkhalkhi";
static const char CModuleName[] =        I18N_NOOP("Person Properties and Services Configuration");
static const char CModuleDescription[] = I18N_NOOP("Person properties and services");
static const char CModuleVersion[] =     "0.1.0";
static const char CModuleCopyright[] =   "(C) 2007 Friedrich W. H. Kossebau";
static const char CModuleComment[] =     I18N_NOOP("Allows to configure the persons");
// Author
static const char FWHKName[] =         "Friedrich W. H. Kossebau";
static const char FWHKTask[] =         I18N_NOOP("Author");
static const char FWHKEmailAddress[] = "kossebau@kde.org";


KhalkhiControlModuleAboutData::KhalkhiControlModuleAboutData()
: KAboutData( CModuleId, CModuleName, CModuleVersion, CModuleDescription,
              KAboutData::License_GPL_V2, CModuleCopyright, CModuleComment, 0, FWHKEmailAddress )
{
    addAuthor( FWHKName, FWHKTask, FWHKEmailAddress );
}
