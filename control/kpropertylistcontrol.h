/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


#ifndef KPROPERTYLISTCONTROL_H
#define KPROPERTYLISTCONTROL_H


// kde
#include <kservice.h>
// lib
#include <ksortedlistbox.h>

class QStringList;


class KPropertyListControl : public KSortedListBox
{
    Q_OBJECT

public:
    explicit KPropertyListControl( QWidget *Parent = 0, const char *Name = 0 );
    virtual ~KPropertyListControl();

public:
    void setProperties( const KService::List &PropertyServices,
                        const QStringList &SortedPropertyIds, const QStringList &HiddenPropertyIds );
public:
    QStringList sortedPropertyIds() const;
    QStringList hiddenPropertyIds() const;

signals:
    void selected( const QString &PropertyId );

protected: // KSortedListBox API
    virtual QListBoxItem *createItem();
    virtual bool action1Item( QListBoxItem *Item );
    virtual bool deleteItem( QListBoxItem *Item );

protected slots:
    void onItemHighlighted( int Index );

private:
    KService::List HiddenPropertyServices;
};

#endif
