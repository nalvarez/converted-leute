/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// KDE
#include <klocale.h>
// class
#include "serviceconfigdialog.h"


ServiceConfigDialog::ServiceConfigDialog( QWidget *Parent, const QString &ServiceName )
 : KDialogBase( Parent, 0, true, /*modal*/
                i18n( "Configure %1" ).arg( ServiceName ),
                Default|Ok|Cancel, Cancel ),
   Dirty( false )
{
    enableButtonSeparator( true );
}

void ServiceConfigDialog::setConfigWidget( QWidget *ConfigWidget )
{
    setMainWidget( ConfigWidget );

    connect( ConfigWidget, SIGNAL(changed(bool)), SLOT(setDirty(bool)) );
    connect( this, SIGNAL(defaultClicked()), ConfigWidget, SLOT(defaults()));
    connect( this, SIGNAL(okClicked()),      ConfigWidget, SLOT(save()));
}


bool ServiceConfigDialog::isDirty() const
{
    return Dirty;
}

void ServiceConfigDialog::setDirty( bool D )
{
    Dirty = D;
}

#include "serviceconfigdialog.moc"
