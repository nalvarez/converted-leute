/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


#ifndef KCMKHALKHI_H
#define KCMKHALKHI_H


// kde
#include <kcmodule.h>

class QWidgetStack;
class QTabWidget;
class KServicesControl;
class KPropertyListControl;
class KPropertyServicesControl;


class KMainedIds
{
public:
    QStringList SortedIds;
    QStringList HiddenIds;
    QStringList MainIds;
};
class KIds
{
public:
    QStringList SortedIds;
    QStringList HiddenIds;
};


class KhalkhiControlModule: public KCModule
{
    Q_OBJECT

public:
    explicit KhalkhiControlModule( QWidget *Parent=0, const char *Name=0, const QStringList& = QStringList() );
    virtual ~KhalkhiControlModule();

public: // KCModule API
    virtual void load();
    virtual void save();
    virtual void defaults();
    virtual int buttons();
    virtual QString quickHelp() const;
    virtual const KAboutData *aboutData() const;

protected:
    void storeDirtyServices();

protected slots:
    void setServicesDirty( bool D );
    void setPropertiesDirty( bool D );
    void setPropertyServicesDirty( bool D );
    void signalServiceConfigChange( int ServiceTypeId, const QString &ServiceId );
    void signalPropertyServiceConfigChange( int ServiceTypeId, const QString &ServiceId );
    void onPropertySelect( const QString& PropertyId );
    void configChanged();
    void onDatabaseChange();

private:
    QTabWidget *TabWidget;
    KServicesControl *ServicesControl;
    KPropertyListControl *PropertyListControl;
    QWidgetStack *WidgetStack;
    KPropertyServicesControl *PropertyServicesControl;

    KAboutData *AboutData;

    QMap<QString,KMainedIds> DirtyPropertyActionServiceIds;
    QMap<QString,KMainedIds> DirtyPropertyDataActionServiceIds;
    QMap<QString,KIds> DirtyPropertyStatusServiceIds;
    bool ActionServicesDirty;
    bool DataActionServicesDirty;
    bool StatusServicesDirty;
    bool PropertiesDirty;
    bool PropertyActionServicesDirty;
    bool PropertyDataActionServicesDirty;
    bool PropertyStatusServicesDirty;
};

#endif
