/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// Qt
#include <qapplication.h>
#include <qpainter.h>
// KDE
#include <kparts/componentfactory.h>
// module
#include "serviceconfigdialog.h"
#include "kservicelistitem.h"


static inline QString configLibrary( const KService *Service )
{
    return Service->property( QString::fromLatin1("X-KDE-ConfigLibrary") ).toString();
}

KServiceListItem::KServiceListItem( const KService::Ptr &S, bool IM )
 : QListBoxText( S->name() ), Service( S ), IsMain( IM )
{
    IsConfigurable = !configLibrary( Service ).isNull();
}

bool KServiceListItem::configure( QWidget *Parent )
{
    bool Result = false;

    ServiceConfigDialog Dialog( Parent, Service->name() );

    QWidget *ConfigWidget =
        KParts::ComponentFactory::createInstanceFromLibrary<QWidget>( configLibrary(Service).local8Bit().data(), &Dialog );

    if( ConfigWidget )
    {
        Dialog.setConfigWidget( ConfigWidget );

        if( Dialog.exec() == QDialog::Accepted )
            Result = Dialog.isDirty();
    }

    return Result;
}


void KServiceListItem::paint( QPainter *Painter )
{
    if( IsMain )
    {
        QFont Font = Painter->font();
        Font.setBold( true );
        Painter->setFont( Font );
    }
    const int itemHeight = height( listBox() );
    const QFontMetrics FontMetrics = Painter->fontMetrics();
    const int yPos = ( ( itemHeight - FontMetrics.height() ) / 2 ) + FontMetrics.ascent();
    Painter->drawText( 3, yPos, text() );
}

int KServiceListItem::width( const QListBox* ListBox ) const
{
    const int Width = ListBox ? ListBox->fontMetrics().width( text() ) + 6 : 0;
    return QMAX( Width, QApplication::globalStrut().width() );
}

KServiceListItem::~KServiceListItem() {}
