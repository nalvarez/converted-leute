/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


#ifndef KSORTEDLISTBOX_H
#define KSORTEDLISTBOX_H


// Qt
#include <qptrlist.h>
#include <qwidget.h>

class KListBox;
class QListBoxItem;
class QPushButton;


class KSortedListBox : public QWidget
{
    Q_OBJECT

public:
    enum Button { Add=1, UpDown=2, Action1=4, Action2=8, Action3=16, Remove=32 };

public:
    explicit KSortedListBox( QWidget *Parent = 0, const char *Name = 0, int Buttons = Add|UpDown|Remove );
    virtual ~KSortedListBox();

public:

signals:
    void changed( bool );

protected:
    virtual QListBoxItem *createItem();
    virtual bool action1Item( QListBoxItem *Item );
    virtual bool action2Item( QListBoxItem *Item );
    virtual bool action3Item( QListBoxItem *Item );
    virtual bool deleteItem( QListBoxItem *Item );

protected:
    void setItemList( const QPtrList<QListBoxItem> &List );
    int count() const;
    void setButtonAction1( const QString &Titel );
    void setButtonAction2( const QString &Titel );
    void setButtonAction3( const QString &Titel );

protected slots:
    void addItem();
    void action1Item();
    void action2Item();
    void action3Item();
    void removeItem();
    void moveUp();
    void moveDown();
private slots:
    void onItemHighlighted( int Index );

protected:
    KListBox *ListBox;
    QPushButton *NewButton;
    QPushButton *UpButton;
    QPushButton *DownButton;
    QPushButton *Action1Button;
    QPushButton *Action2Button;
    QPushButton *Action3Button;
    QPushButton *RemoveButton;
};

#endif
