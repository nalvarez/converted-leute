/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// qt
#include <qlayout.h>
#include <qpushbutton.h>
#include <qwhatsthis.h>
// kde
#include <klistbox.h>
#include <kdialog.h>
#include <knotifyclient.h>
#include <klocale.h>
// module
#include "ksortedlistbox.h"


KSortedListBox::KSortedListBox( QWidget *Parent, const char *Name, int Buttons )
 : QWidget( Parent, Name )
{
    QHBoxLayout *TopLayout = new QHBoxLayout( this, 0, KDialog::spacingHint() );
    QVBoxLayout *ButtonsLayout = new QVBoxLayout( KDialog::spacingHint() );

    ListBox = new KListBox( this );
    ListBox->insertItem( i18n("None") );
    ListBox->setEnabled( false );
    connect( ListBox, SIGNAL(highlighted(int)), SLOT(onItemHighlighted(int)) );
//     connect( ListBox, SIGNAL(executed( QListBoxItem * )), SLOT(action1Item()) );

    if( Buttons & Add )
    {
        NewButton = new QPushButton( i18n("Add..."), this );
        NewButton->setEnabled( false );
        connect( NewButton, SIGNAL(clicked()), SLOT(addItem()) );
        ButtonsLayout->addWidget( NewButton );
    }
    else
        NewButton = 0;

    if( Buttons & UpDown )
    {
        UpButton = new QPushButton( i18n("Move &Up"), this );
        UpButton->setEnabled( false );
        connect( UpButton, SIGNAL(clicked()), SLOT(moveUp()) );
        ButtonsLayout->addWidget( UpButton );
        DownButton = new QPushButton( i18n("Move &Down"), this );
        DownButton->setEnabled( false );
        connect( DownButton, SIGNAL(clicked()), SLOT(moveDown()) );
        ButtonsLayout->addWidget( DownButton );
    }
    else
    {
        UpButton = 0;
        DownButton = 0;
    }

    if( Buttons & Action1 )
    {
        Action1Button = new QPushButton( this );
        Action1Button->setEnabled( false );
        connect( Action1Button, SIGNAL(clicked()), SLOT(action1Item()) );
        ButtonsLayout->addWidget( Action1Button );
    }
    else
        Action1Button = 0;

    if( Buttons & Action2 )
    {
        Action2Button = new QPushButton( this );
        Action2Button->setEnabled( false );
        connect( Action2Button, SIGNAL(clicked()), SLOT(action2Item()) );
        ButtonsLayout->addWidget( Action2Button );
    }
    else
        Action2Button = 0;

    if( Buttons & Action3 )
    {
        Action3Button = new QPushButton( this );
        Action3Button->setEnabled( false );
        connect( Action3Button, SIGNAL(clicked()), SLOT(action3Item()) );
        ButtonsLayout->addWidget( Action3Button );
    }
    else
        Action3Button = 0;

    if( Buttons & Remove )
    {
        RemoveButton = new QPushButton( i18n("Remove"), this );
        RemoveButton->setEnabled( false );
        connect( RemoveButton, SIGNAL(clicked()), SLOT(removeItem()) );
        ButtonsLayout->addWidget( RemoveButton );
    }
    else
        RemoveButton = 0;
    ButtonsLayout->addStretch();

    TopLayout->addWidget( ListBox );
    TopLayout->addLayout( ButtonsLayout );
}

QListBoxItem *KSortedListBox::createItem() { return 0; }
bool KSortedListBox::action1Item( QListBoxItem */*Item*/ ) { return false; }
bool KSortedListBox::action2Item( QListBoxItem */*Item*/ ) { return false; }
bool KSortedListBox::action3Item( QListBoxItem */*Item*/ ) { return false; }
bool KSortedListBox::deleteItem( QListBoxItem */*Item*/ ) { return false; }

int KSortedListBox::count() const
{
    return ( ListBox->text(0) == i18n("None") ) ? 0 : ListBox->count();
}

void KSortedListBox::setItemList( const QPtrList<QListBoxItem> &List )
{
    ListBox->clear();

    const bool HasItems = ( List.count() > 0 );
    if( HasItems )
        for( QPtrList<QListBoxItem>::ConstIterator it = List.begin(); it != List.end(); ++it )
            ListBox->insertItem( *it );
    else
        ListBox->insertItem( i18n("None") );

    ListBox->setEnabled( HasItems );
}

void KSortedListBox::setButtonAction1( const QString &Titel )
{
    if( Action1Button )
        Action1Button->setText( Titel );
}

void KSortedListBox::setButtonAction2( const QString &Titel )
{
    if( Action2Button )
        Action2Button->setText( Titel );
}

void KSortedListBox::setButtonAction3( const QString &Titel )
{
    if( Action3Button )
        Action3Button->setText( Titel );
}


void KSortedListBox::addItem()
{
    QListBoxItem* Item = createItem();
    if( !Item )
        return;

    // if None is the only item, then there currently is no default
    if( ListBox->text(0) == i18n("None") )
    {
        ListBox->removeItem( 0 );
        ListBox->setEnabled( true );
    }

    ListBox->insertItem( Item, 0 );
    ListBox->setCurrentItem( 0 );

    emit changed( true );
}

void KSortedListBox::removeItem()
{
    int CurrentIndex = ListBox->currentItem();
    QListBoxItem *CurrentItem = ListBox->item(CurrentIndex);

    if( CurrentItem && deleteItem(CurrentItem) )
    {
        ListBox->removeItem( CurrentIndex );

        if( ListBox->currentItem() == -1 )
        {
            ListBox->insertItem( i18n("None") );
            ListBox->setEnabled( false );

            RemoveButton->setEnabled( false );
            if( Action1Button ) Action1Button->setEnabled( false );
        }
        emit changed(true);
    }
}

void KSortedListBox::action1Item()
{
    QListBoxItem *CurrentItem = ListBox->selectedItem();

    if( CurrentItem && action1Item(CurrentItem) )
    {
//         updateItem( CurrentItem );
        ListBox->triggerUpdate( true );
        emit changed( true );
    }
}

void KSortedListBox::action2Item()
{
    QListBoxItem *CurrentItem = ListBox->selectedItem();

    if( CurrentItem && action2Item(CurrentItem) )
    {
//         updateItem( CurrentItem );
        ListBox->triggerUpdate( true );
        emit changed( true );
    }
}

void KSortedListBox::action3Item()
{
    QListBoxItem *CurrentItem = ListBox->selectedItem();

    if( CurrentItem && action3Item(CurrentItem) )
    {
//         updateItem( CurrentItem );
        ListBox->triggerUpdate( true );
        emit changed( true );
    }
}

void KSortedListBox::moveUp()
{
    if( !ListBox->isEnabled() )
    {
        KNotifyClient::beep();
        return;
    }

    unsigned int CurrentIndex = ListBox->currentItem();
    // already first? TODO: up button should not be available
    if( CurrentIndex == 0 )
    {
        KNotifyClient::beep();
        return;
    }

    QListBoxItem *CurrentItem = ListBox->item( CurrentIndex );
    ListBox->takeItem( CurrentItem );
    ListBox->insertItem( CurrentItem, CurrentIndex-1 );
    ListBox->setCurrentItem( CurrentIndex-1 );

    emit changed( true );
}

void KSortedListBox::moveDown()
{
    if( !ListBox->isEnabled() )
    {
        KNotifyClient::beep();
        return;
    }

    const unsigned int CurrentIndex = ListBox->currentItem();
    // already last? TODO: down button should not be available
    if( CurrentIndex == ListBox->count()-1 )
    {
        KNotifyClient::beep();
        return;
    }

    QListBoxItem *CurrentItem = ListBox->item( CurrentIndex );
    ListBox->takeItem( CurrentItem );
    ListBox->insertItem( CurrentItem, CurrentIndex+1 );
    ListBox->setCurrentItem( CurrentIndex+1 );

    emit changed( true );
}

void KSortedListBox::onItemHighlighted( int Index )
{
    if( ListBox->count() <= 1 )
    {
        UpButton->setEnabled( false );
        DownButton->setEnabled( false );
    }
    else if( (uint)Index == (ListBox->count()-1))
    {
        UpButton->setEnabled( true );
        DownButton->setEnabled( false );
    }
    else if( Index == 0 )
    {
        UpButton->setEnabled( false );
        DownButton->setEnabled( true );
    }
    else
    {
        UpButton->setEnabled( true );
        DownButton->setEnabled( true );
    }

    RemoveButton->setEnabled( true );
    if( Action1Button ) Action1Button->setEnabled( true );
}

KSortedListBox::~KSortedListBox() {}

#include "ksortedlistbox.moc"
