/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


#ifndef KPROPERTYSERVICESCONTROL_H
#define KPROPERTYSERVICESCONTROL_H


// qt
#include <qtabwidget.h>
// kde
#include <kservice.h>

class QStringList;
class KPropertyActionListControl;
class KPropertyDataActionListControl;
class KPropertyStatusListControl;


class KPropertyServicesControl : public QTabWidget
{
    Q_OBJECT

public:
    explicit KPropertyServicesControl( QWidget *Parent = 0, const char *Name = 0 );
    virtual ~KPropertyServicesControl();

public:
    void setProperty( const QString &PropertyId );

public:
    QString propertyId() const;
    KPropertyActionListControl *actionControl() const;
    KPropertyDataActionListControl *dataActionControl() const;
    KPropertyStatusListControl *statusControl() const;

signals:
    void changed( bool );
    void configChanged( int ServiceTypeId, const QString &ServiceId );

private:
    KPropertyActionListControl *ActionControl;
    KPropertyDataActionListControl *DataActionControl;
    KPropertyStatusListControl *StatusControl;

    QString PropertyId;
};

inline QString KPropertyServicesControl::propertyId() const { return PropertyId; }
inline KPropertyActionListControl *KPropertyServicesControl::actionControl() const { return ActionControl; }
inline KPropertyDataActionListControl *KPropertyServicesControl::dataActionControl() const { return DataActionControl; }
inline KPropertyStatusListControl *KPropertyServicesControl::statusControl() const { return StatusControl; }

#endif
