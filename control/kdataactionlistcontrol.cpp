/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// qt
#include <qlayout.h>
#include <qlistbox.h>
#include <qpushbutton.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
// kde
#include <kdialog.h>
#include <knotifyclient.h>
#include <klocale.h>
// module
#include "kservicelistitem.h"
#include "kserviceselectdialog.h"
#include "kdataactionlistcontrol.h"


static inline QString serviceId( const KService *Service )
{
    return Service->property( QString::fromLatin1("X-KDE-DataActionService") ).toString();
}

KDataActionListControl::KDataActionListControl( QWidget *Parent, const char *Name )
 : KSortedListBox( Parent, Name, Add|UpDown|Action1|Remove )
{
    setButtonAction1( i18n("Configure...") );

    connect( ListBox, SIGNAL(highlighted(int)), SLOT(onItemHighlighted(int)) );

    const QString Description = i18n(
        " This is a list of action services on data associated with entire persons."
        " This list is e.g. shown in a person's menu for some dropped data." );

    QWhatsThis::add( this, Description );
    QWhatsThis::add( ListBox, Description );

    QToolTip::add( NewButton,    i18n( "Adds a new action.") );
    QToolTip::add( UpButton,     i18n( "Moves the current action up in the list.") );
    QToolTip::add( DownButton,   i18n( "Moves the current action down in the list.") );
    QToolTip::add( RemoveButton, i18n( "Removes the current action from the list.") );
}

// TODO: to be optimized
void KDataActionListControl::setServices( const KService::List &Services,
                                                 const QStringList &SortedIds, const QStringList &HiddenIds )
{
    HiddenServices = Services;

    QPtrList<QListBoxItem> ItemList;

    // add sorted items
    for( QStringList::ConstIterator sit = SortedIds.begin(); sit != SortedIds.end(); ++sit )
    {
        KService::List::Iterator endIt = HiddenServices.end();
        for( KService::List::Iterator it = HiddenServices.begin(); it != endIt; ++it )
        {
            const QString ActionId = serviceId( *it );
            if( ActionId == *sit )
            {
                ItemList.append( new KServiceListItem(*it) );
                HiddenServices.erase( it );
                break;
            }
        }
    }
    // add unsorted items
    for( KService::List::Iterator it = HiddenServices.begin(); it != HiddenServices.end(); )
    {
        const QString ActionId = serviceId( *it );
        QStringList::ConstIterator sit = HiddenIds.find( ActionId );
        // not hidden?
        if( sit == HiddenIds.end() )
        {
            ItemList.append( new KServiceListItem(*it) );
            it = HiddenServices.erase( it );
        }
        else
            ++it;
    }

    setItemList( ItemList );

    NewButton->setEnabled( HiddenServices.size() > 0 );
}

QStringList KDataActionListControl::sortedIds() const
{
    QStringList Result;
    unsigned int Count = count();

    for( unsigned int i = 0; i<Count; ++i )
    {
        KServiceListItem *Item = static_cast<KServiceListItem *>( ListBox->item(i) );
        Result.append( serviceId(Item->service()) );
    }

    return Result;
}

QStringList KDataActionListControl::hiddenIds() const
{
    QStringList Result;

    for( KService::List::ConstIterator it = HiddenServices.begin(); it != HiddenServices.end(); ++it )
        Result.append( serviceId(*it) );

    return Result;
}

void KDataActionListControl::onItemHighlighted( int Index )
{
    KServiceListItem *Item = static_cast<KServiceListItem *>( ListBox->item(Index) );

    Action1Button->setEnabled( Item->isConfigurable() );

    emit selected( serviceId(Item->service()) );
}

QListBoxItem *KDataActionListControl::createItem()
{

    KService::Ptr Service = 0;
    KServiceSelectDialog Dialog( i18n("Action on Data Selection"), i18n("Select an action:"), this );
    Dialog.setServices( HiddenServices );
    if( Dialog.exec() != QDialog::Accepted )
        return 0;
    Service = Dialog.service();

    if( !Service )
        return 0;

    HiddenServices.remove( Service );

    return new KServiceListItem( Service );
}

bool KDataActionListControl::action1Item( QListBoxItem *I )
{
    bool Result = false;

    KServiceListItem *Item = static_cast<KServiceListItem *>( I );
    if( Item->configure(this) )
        emit configChanged( DataActionServiceId, serviceId(Item->service()) );

    return Result;
}

bool KDataActionListControl::deleteItem( QListBoxItem *I )
{
    bool Result = true;

    KServiceListItem *Item = static_cast<KServiceListItem *>( I );
    HiddenServices.append( Item->service() );
    NewButton->setEnabled( true );

    return Result;
}


KDataActionListControl::~KDataActionListControl() {}

#include "kdataactionlistcontrol.moc"
