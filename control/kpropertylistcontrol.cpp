/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// qt
#include <qlayout.h>
#include <qlistbox.h>
#include <qpushbutton.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
// kde
#include <kdialog.h>
#include <knotifyclient.h>
#include <klocale.h>
// module
#include "kservicelistitem.h"
#include "kserviceselectdialog.h"
#include "kpropertylistcontrol.h"


KPropertyListControl::KPropertyListControl( QWidget *Parent, const char *Name )
 : KSortedListBox( Parent, Name )
{
    connect( ListBox, SIGNAL(highlighted(int)), SLOT(onItemHighlighted(int)) );

    const QString Description = i18n(
        " This is a list of person properties."
        " This list is used for limiting and sorting when displaying"
        " a person's properties, also offering the associated actions in the menus." );

    QWhatsThis::add( this, Description );
    QWhatsThis::add( ListBox, Description );

    QToolTip::add( NewButton,     i18n( "Adds a new property.") );
    QToolTip::add( UpButton,      i18n( "Moves the current property up in the list.") );
    QToolTip::add( DownButton,    i18n( "Moves the current property down in the list.") );
    QToolTip::add( RemoveButton,  i18n( "Removes the current property from the list.") );
}

// TODO: to be optimized
void KPropertyListControl::setProperties( const KService::List &PropertyServices,
                                          const QStringList &SortedPropertyIds, const QStringList &HiddenPropertyIds )
{
    HiddenPropertyServices = PropertyServices;

    QPtrList<QListBoxItem> ItemList;

    // add sorted items
    for( QStringList::ConstIterator sit = SortedPropertyIds.begin(); sit != SortedPropertyIds.end(); ++sit )
    {
        KService::List::Iterator endIt = HiddenPropertyServices.end();
        for( KService::List::Iterator it = HiddenPropertyServices.begin(); it != endIt; ++it )
        {
            const QString PropertyId = (**it).property( QString::fromLatin1("X-KDE-KhalkhiProperty") ).toString();
            if( PropertyId == *sit )
            {
                ItemList.append( new KServiceListItem(*it) );
                HiddenPropertyServices.erase( it );
                break;
            }
        }
    }
    // add unsorted items
    for( KService::List::Iterator it = HiddenPropertyServices.begin(); it != HiddenPropertyServices.end(); )
    {
        const QString PropertyId = (**it).property( QString::fromLatin1("X-KDE-KhalkhiProperty") ).toString();
        QStringList::ConstIterator sit = HiddenPropertyIds.find( PropertyId );
        // not hidden?
        if( sit == HiddenPropertyIds.end() )
        {
            ItemList.append( new KServiceListItem(*it) );
            it = HiddenPropertyServices.erase( it );
        }
        else
            ++it;
    }

    setItemList( ItemList );

    NewButton->setEnabled( HiddenPropertyServices.size() > 0 );
}

QStringList KPropertyListControl::sortedPropertyIds() const
{
    QStringList Result;
    unsigned int count = ListBox->count();

    for( unsigned int i = 0; i<count; ++i )
    {
        KServiceListItem *Item = static_cast<KServiceListItem *>( ListBox->item(i) );
        Result.append(  Item->service()->property( QString::fromLatin1("X-KDE-KhalkhiProperty") ).toString() );
    }

    return Result;
}

QStringList KPropertyListControl::hiddenPropertyIds() const
{
    QStringList Result;

    for( KService::List::ConstIterator it = HiddenPropertyServices.begin(); it != HiddenPropertyServices.end(); ++it )
        Result.append( (**it).property( QString::fromLatin1("X-KDE-KhalkhiProperty") ).toString() );

    return Result;
}

void KPropertyListControl::onItemHighlighted( int Index )
{
    KServiceListItem *Item = static_cast<KServiceListItem *>( ListBox->item(Index) );

    emit selected( Item->service()->property( QString::fromLatin1("X-KDE-KhalkhiProperty") ).toString() );
}

QListBoxItem *KPropertyListControl::createItem()
{

    KService::Ptr Service = 0;
    KServiceSelectDialog Dialog( i18n("Property Selection"), i18n("Select a property:"), this );
    Dialog.setServices( HiddenPropertyServices );
    if( Dialog.exec() != QDialog::Accepted )
        return 0;
    Service = Dialog.service();

    if( !Service )
        return 0;

    HiddenPropertyServices.remove( Service );

    return new KServiceListItem( Service );
}


bool KPropertyListControl::deleteItem( QListBoxItem *I )
{
    bool Result = true;

    KServiceListItem *Item = static_cast<KServiceListItem *>( I );
    HiddenPropertyServices.append( Item->service() );
    NewButton->setEnabled( true );

    return Result;
}


bool KPropertyListControl::action1Item( QListBoxItem */*Item*/ )
{
    return true;
}

KPropertyListControl::~KPropertyListControl() {}

#include "kpropertylistcontrol.moc"
