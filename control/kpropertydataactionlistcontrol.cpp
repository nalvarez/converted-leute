/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// qt
#include <qlayout.h>
#include <qlistbox.h>
#include <qpushbutton.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
// kde
#include <kdialog.h>
#include <knotifyclient.h>
#include <klocale.h>
// module
#include "kservicelistitem.h"
#include "kserviceselectdialog.h"
#include "kpropertydataactionlistcontrol.h"


static inline QString serviceId( const KService *Service )
{
    return Service->property( QString::fromLatin1("X-KDE-DataActionService") ).toString();
}
static inline bool hasCategories2( const KService *Service )
{
    return !Service->property( QString::fromLatin1("X-KDE-Categories") ).toStringList().isEmpty();
}


KPropertyDataActionListControl::KPropertyDataActionListControl( QWidget *Parent, const char *Name )
 : KSortedListBox( Parent, Name, Add|UpDown|Action1|Action2|Action3|Remove )
{
    setButtonAction1( i18n("Global") );
    setButtonAction2( i18n("Local") );
    setButtonAction3( i18n("Configure...") );

    connect( ListBox, SIGNAL(highlighted(int)), SLOT(onItemHighlighted(int)) );

    const QString Description = i18n(
        " This is a list of action services on data associated with the selected property."
        " This list is e.g. shown in a property's menu for some dropped data."
        " Set an action to \"Global\" if it should also be included"
        " in the entire person's menu for dropped data." );

    QWhatsThis::add( this, Description );
    QWhatsThis::add( ListBox, Description );

    QToolTip::add( NewButton,     i18n( "Adds a new action.") );
    QToolTip::add( UpButton,      i18n( "Moves the current action up in the list.") );
    QToolTip::add( DownButton,    i18n( "Moves the current action down in the list.") );
    QToolTip::add( Action1Button, i18n( "Marks the current action to appear also in menus on the entire person.") );
    QToolTip::add( Action2Button, i18n( "Marks the current action to appear only in menus on the property.") );
    QToolTip::add( RemoveButton,  i18n( "Removes the current action from the list.") );
}

// TODO: to be optimized
void KPropertyDataActionListControl::setDataActionServices( const KService::List &Services,
                                                    const QStringList &SortedIds, const QStringList &HiddenIds,
                                                    const QStringList &MainIds )
{
    HiddenDataActionServices = Services;

    QPtrList<QListBoxItem> ItemList;

    // add sorted items
    for( QStringList::ConstIterator sit = SortedIds.begin(); sit != SortedIds.end(); ++sit )
    {
        KService::List::Iterator endIt = HiddenDataActionServices.end();
        for( KService::List::Iterator it = HiddenDataActionServices.begin(); it != endIt; ++it )
        {
            const QString ActionId = serviceId( *it );
            if( ActionId == *sit )
            {
                ItemList.append( new KServiceListItem(*it,MainIds.contains( ActionId )) );
                HiddenDataActionServices.erase( it );
                break;
            }
        }
    }
    // add unsorted items
    for( KService::List::Iterator it = HiddenDataActionServices.begin(); it != HiddenDataActionServices.end(); )
    {
        const QString ActionId = serviceId( *it );
        QStringList::ConstIterator sit = HiddenIds.find( ActionId );
        // not hidden?
        if( sit == HiddenIds.end() )
        {
            ItemList.append( new KServiceListItem(*it,hasCategories2(*it)) );
            it = HiddenDataActionServices.erase( it );
        }
        else
            ++it;
    }

    setItemList( ItemList );

    NewButton->setEnabled( HiddenDataActionServices.size() > 0 );
}

QStringList KPropertyDataActionListControl::sortedDataActionIds() const
{
    QStringList Result;
    const unsigned int Count = count();

    for( unsigned int i = 0; i<Count; ++i )
    {
        KServiceListItem *Item = static_cast<KServiceListItem *>( ListBox->item(i) );
        Result.append( serviceId(Item->service()) );
    }

    return Result;
}

QStringList KPropertyDataActionListControl::hiddenDataActionIds() const
{
    QStringList Result;

    for( KService::List::ConstIterator it = HiddenDataActionServices.begin(); it != HiddenDataActionServices.end(); ++it )
        Result.append( serviceId(*it) );

    return Result;
}

QStringList KPropertyDataActionListControl::mainDataActionIds() const
{
    QStringList Result;
    unsigned int Count = count();

    for( unsigned int i = 0; i<Count; ++i )
    {
        KServiceListItem *Item = static_cast<KServiceListItem *>( ListBox->item(i) );
        if( Item->isMain() )
            Result.append( serviceId(Item->service()) );
    }

    return Result;
}

void KPropertyDataActionListControl::onItemHighlighted( int Index )
{
    KServiceListItem *Item = static_cast<KServiceListItem *>( ListBox->item(Index) );

    const bool IsMain = Item->isMain();
    Action1Button->setEnabled( !IsMain );
    Action2Button->setEnabled( IsMain );

    emit selected( serviceId(Item->service()) );
}

QListBoxItem *KPropertyDataActionListControl::createItem()
{

    KService::Ptr Service = 0;
    KServiceSelectDialog Dialog( i18n("Action on Data Selection"), i18n("Select an action:"), this );
    Dialog.setServices( HiddenDataActionServices );
    if( Dialog.exec() != QDialog::Accepted )
        return 0;
    Service = Dialog.service();

    if( !Service )
        return 0;

    HiddenDataActionServices.remove( Service );

    return new KServiceListItem( Service );
}


bool KPropertyDataActionListControl::deleteItem( QListBoxItem *I )
{
    bool Result = true;

    KServiceListItem *Item = static_cast<KServiceListItem *>( I );
    HiddenDataActionServices.append( Item->service() );
    NewButton->setEnabled( true );

    return Result;
}


bool KPropertyDataActionListControl::action1Item( QListBoxItem *I )
{
    bool Result = true;

    KServiceListItem *Item = static_cast<KServiceListItem *>( I );
    Item->setMain( true );
    Action1Button->setEnabled( false );
    Action2Button->setEnabled( true );
    Action3Button->setEnabled( Item->isConfigurable() );

    return Result;
}

bool KPropertyDataActionListControl::action2Item( QListBoxItem *I )
{
    bool Result = true;

    KServiceListItem *Item = static_cast<KServiceListItem *>( I );
    Item->setMain( false );
    Action1Button->setEnabled( true );
    Action2Button->setEnabled( false );

    return Result;
}

bool KPropertyDataActionListControl::action3Item( QListBoxItem *I )
{
    bool Result = false;

    KServiceListItem *Item = static_cast<KServiceListItem *>( I );
    if( Item->configure(this) )
        emit configChanged( DataActionServiceId, serviceId(Item->service()) );

    return Result;
}

KPropertyDataActionListControl::~KPropertyDataActionListControl() {}

#include "kpropertydataactionlistcontrol.moc"
