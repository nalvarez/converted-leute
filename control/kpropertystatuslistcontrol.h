/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


#ifndef KPROPERTYSTATUSLISTCONTROL_H
#define KPROPERTYSTATUSLISTCONTROL_H


// kde
#include <kservice.h>
// lib
#include <ksortedlistbox.h>

class QStringList;


class KPropertyStatusListControl : public KSortedListBox
{
    Q_OBJECT

public:
    explicit KPropertyStatusListControl( QWidget *Parent = 0, const char *Name = 0 );
    virtual ~KPropertyStatusListControl();

public:
    void setStatusServices( const KService::List &StatusServices,
                            const QStringList &SortedStatusIds, const QStringList &HiddenStatusIds );

public:
    QStringList sortedStatusIds() const;
    QStringList hiddenStatusIds() const;

signals:
    void selected( const QString &PropertyId );
    void configChanged( int ServiceTypeId, const QString &ServiceId );

protected: // KSortedListBox API
    virtual QListBoxItem *createItem();
    virtual bool action1Item( QListBoxItem *I );
    virtual bool deleteItem( QListBoxItem *Item );

protected slots:
    void onItemHighlighted( int Index );

private:
    KService::List HiddenStatusServices;
};

#endif
