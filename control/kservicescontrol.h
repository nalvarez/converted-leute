/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


#ifndef KHALKHI_SERVICESCONTROL_H
#define KHALKHI_SERVICESCONTROL_H


// qt
#include <qtabwidget.h>
// kde
#include <kservice.h>

class KActionListControl;
class KDataActionListControl;
class KStatusListControl;


class KServicesControl : public QTabWidget
{
    Q_OBJECT

public:
    explicit KServicesControl( QWidget *Parent = 0, const char *Name = 0 );
    virtual ~KServicesControl();

public:

public:
    KActionListControl *actionControl() const;
    KDataActionListControl *dataActionControl() const;
    KStatusListControl *statusControl() const;

signals:
    void changed( bool );
    void configChanged( int ServiceTypeId, const QString &ServiceId );

private:
    KActionListControl *ActionControl;
    KDataActionListControl *DataActionControl;
    KStatusListControl *StatusControl;
};

inline KActionListControl *KServicesControl::actionControl() const { return ActionControl; }
inline KDataActionListControl *KServicesControl::dataActionControl() const { return DataActionControl; }
inline KStatusListControl *KServicesControl::statusControl() const { return StatusControl; }

#endif
