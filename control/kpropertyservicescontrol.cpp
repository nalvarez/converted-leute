/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


// kde
#include <kdialog.h>
#include <klocale.h>
// module
#include "kpropertyactionlistcontrol.h"
#include "kpropertydataactionlistcontrol.h"
#include "kpropertystatuslistcontrol.h"
#include "kpropertyservicescontrol.h"


KPropertyServicesControl::KPropertyServicesControl( QWidget *Parent, const char *Name )
 : QTabWidget( Parent, Name )
{
    setMargin( KDialog::marginHint() );

    ActionControl = new KPropertyActionListControl( this );
    addTab( ActionControl, i18n("&Actions") );
    connect( ActionControl, SIGNAL(changed(bool)),  SIGNAL(changed(bool)) );
    connect( ActionControl, SIGNAL(configChanged( int, const QString & )),
             SIGNAL(configChanged( int, const QString & )) );

    DataActionControl = new KPropertyDataActionListControl( this );
    addTab( DataActionControl, i18n("Actions on &Data") );
    connect( DataActionControl, SIGNAL(changed(bool)),  SIGNAL(changed(bool)) );
    connect( DataActionControl, SIGNAL(configChanged( int, const QString & )),
             SIGNAL(configChanged( int, const QString & )) );

    StatusControl = new KPropertyStatusListControl( this );
    addTab( StatusControl, i18n("&States") );
    connect( StatusControl, SIGNAL(changed(bool)),  SIGNAL(changed(bool)) );
    connect( StatusControl, SIGNAL(configChanged( int, const QString & )),
             SIGNAL(configChanged( int, const QString & )) );
}

void KPropertyServicesControl::setProperty( const QString &PId )
{
    PropertyId = PId;
}

KPropertyServicesControl::~KPropertyServicesControl()
{}

#include "kpropertyservicescontrol.moc"
