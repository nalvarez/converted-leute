/*
    This file is part of the Leute Gui library, part of the KDE project.

    Copyright 2006,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#ifndef LISTALLPROPERTIESDEFAULTACTIONSERVICEMENUFILLER_H
#define LISTALLPROPERTIESDEFAULTACTIONSERVICEMENUFILLER_H

// lib specific
#include "leutegui_export.h"
#include "idserviceactionmap.h"
// leute core
#include "listallpropertiesglobalactionserviceclient.h"
// KDE
#include <kabc/addresseelist.h>
// Qt
#include <QtCore/QObject>

class QMenu;

namespace Leute {

class LEUTEGUI_EXPORT ListAllPropertiesGlobalActionServiceMenuFiller : public QObject, public ListAllPropertiesGlobalActionServiceClient
{
    Q_OBJECT
public:
    ListAllPropertiesGlobalActionServiceMenuFiller();
    virtual ~ListAllPropertiesGlobalActionServiceMenuFiller();

public:
    void setPersonList( const KABC::AddresseeList& List );
    void fillMenu( QMenu* menu );

public: // GlobalServiceClient interface
    virtual void onGlobalActionServiceSwitch( const QString &PropertyId );
    virtual void onPropertyManagerChange();

public: // ServiceClient interface
    virtual const KABC::AddresseeList &personList() const;
    virtual void onActionServiceStateChange( const PropertyActionService &Service, int Change,
                                             int ItemIndex );

protected Q_SLOTS:
    void onActionSelection( QAction* action );
    void onMenuDestruction();

protected:
    KABC::AddresseeList mPersonList;

    QMenu* mMenu;
    IdServiceActionMap mIdActionMap;
};


inline ListAllPropertiesGlobalActionServiceMenuFiller::ListAllPropertiesGlobalActionServiceMenuFiller() : mMenu( 0 ) {}

inline void ListAllPropertiesGlobalActionServiceMenuFiller::setPersonList( const KABC::AddresseeList &L )
{ mPersonList = L; }

}

#endif
