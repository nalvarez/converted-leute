/*
    This file is part of the Leute Gui library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#include "listallpropertiesglobalactionservicexmlguifiller.h"

// leute core
#include <services.h>
#include <propertyadapter.h>
#include <listpropertyactionservice.h>
// kde
#include <KXMLGUIClient>
// Qt
#include <QtCore/QSignalMapper>


namespace Leute
{

ListAllPropertiesGlobalActionServiceXMLGUIFiller::ListAllPropertiesGlobalActionServiceXMLGUIFiller()
 : Client( 0 ), SignalMapper( new QSignalMapper(this) )
{
    connect( SignalMapper, SIGNAL(mapped( int )), SLOT(onActionActivation( int )) );
}

const KABC::AddresseeList &ListAllPropertiesGlobalActionServiceXMLGUIFiller::personList() const { return PersonList; }

void ListAllPropertiesGlobalActionServiceXMLGUIFiller::onGlobalActionServiceSwitch( const QString &/*PropertyId*/ ) { /*TODO: what to do?*/}
void ListAllPropertiesGlobalActionServiceXMLGUIFiller::onPropertyManagerChange() { /*TODO: what to do?*/}
void ListAllPropertiesGlobalActionServiceXMLGUIFiller::onActionServiceStateChange( const PropertyActionService &/*Service*/,
                                                                              int /*Change*/, int /*ItemIndex*/ )
{
#if 0
    const QString &ServiceId = Service.id();

    int MaxItemIndex;
    if( ItemIndex == -1 )
    {
        ItemIndex = 0;
        MaxItemIndex = Service.adapter()->numberOfItems( Person );
    }
    else
        MaxItemIndex = ItemIndex+1;

    for( ; ItemIndex<MaxItemIndex; ++ItemIndex )
    {
        // find menuid of action
        int Flags;
        int ActionId = IdActionMap.menuId( Service.adapter()->id(), ItemIndex, ServiceId, &Flags );

        if( ActionId != -1 )
        {
            // TODO: store (refer item) flag in IdActionMap
            ServiceAction Action = Service.action( Person, ItemIndex, Flags );

            QString Entry = Action.data( DisplayTextRole ).asString();
            QIconSet IconSet = Action.data( DisplayIconRole ).asIconSet();
            bool Enabled = Action.data( EnabledRole ).asBool();

            Menu->changeItem( ActionId, IconSet, Entry );
            Menu->setItemEnabled( ActionId, Enabled );
        }
    }
#endif
}

void ListAllPropertiesGlobalActionServiceXMLGUIFiller::setGUIClient( KXMLGUIClient *C, const QString &LI )
{
    Client = C;
    ListId = LI;

    if( Client == 0 )
        Services::self()->unregisterClient( this );
}


void ListAllPropertiesGlobalActionServiceXMLGUIFiller::setPersonList( const KABC::AddresseeList &L )
{
    Services::self()->unregisterClient( this );

    PersonList = L;

    Services::self()->registerClient( this );
    update();
}

void ListAllPropertiesGlobalActionServiceXMLGUIFiller::update()
{
    const PropertyManagerList &Managers = Services::self()->propertyManagers();

    mActions.clear();
    int ActionId = 0;
    IdActionMap.prepare( Managers.size() );
    PropertyManagerList::ConstIterator ManagerIt = Managers.begin();
    for( ; ManagerIt != Managers.end(); ++ManagerIt )
    {
        const PropertyManager &Manager = **ManagerIt;

        const PropertyActionServiceList &Services = Manager.mainActionServices();
        if( Services.size() == 0 )
            continue;

        const PropertyAdapter *Adapter = Manager.propertyAdapter();
        if( !Adapter->haveProperty(PersonList) )
            continue;

        IdActionMap.addProperty( Adapter->id(), Services.size() );

        for( PropertyActionServiceList::ConstIterator ServiceIt = Services.begin(); ServiceIt != Services.end(); ++ServiceIt )
        {
            const ListPropertyActionService *ListService =
                ::qt_cast<ListPropertyActionService*>( *ServiceIt );
            if( !ListService )
                continue;

            if( !ListService->isAvailable() )
                ;// TODO: 
            else
            {
                if( !ListService->supports(PersonList) )
                    continue;

                const int Flags = PropertyService::Always;

                IdActionMap.addService( ListService->id(), 1 );

                // insert service action entries
                IdActionMap.addItem( 0, ActionId, Flags );

                ServiceAction Action = ListService->action( PersonList, Flags );

                QString Entry = Action.data( DisplayTextRole ).asString();
                QIconSet IconSet = Action.data( DisplayIconRole ).asIconSet();
                bool Enabled = Action.data( EnabledRole ).asBool();

                KAction *GUIAction =
                    new KAction( Entry, IconSet, KShortcut(), SignalMapper, SLOT(map()),
                                 (KActionCollection*)0, 0 );
                GUIAction->setEnabled( Enabled );
                SignalMapper->setMapping( GUIAction, ActionId );
                ++ActionId;
                mActions.append( GUIAction );
            }
        }
    }

    Client->unplugActionList( ListId );
    Client->plugActionList( ListId, mActions );
}


void ListAllPropertiesGlobalActionServiceXMLGUIFiller::onActionActivation( int ActionId )
{
    QString PropertyId;
    int ItemIndex;
    QString ServiceId;

    if( IdActionMap.remap(&PropertyId,&ItemIndex,&ServiceId, ActionId) )
        Services::self()->execute( PersonList, PropertyId, ServiceId );
}

ListAllPropertiesGlobalActionServiceXMLGUIFiller::~ListAllPropertiesGlobalActionServiceXMLGUIFiller()
{
    qDeleteAll( mActions );
}

}
