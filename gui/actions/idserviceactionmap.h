/*
    This file is part of the Leute Gui library, part of the KDE project.

    Copyright 2006,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#ifndef IDSERVICEACTIONMAP_H
#define IDSERVICEACTIONMAP_H

// Qt
#include <QtCore/QVector>
#include <QtCore/QString>

class QAction;


struct ItemMenuMap
{
    ItemMenuMap() : MenuAction( 0 ) {}
    ItemMenuMap( int I, QAction* menuAction, int F ) : Index( I ), MenuAction( menuAction ), Flag( F ) {}
    int Index;
    QAction* MenuAction;
    int Flag;
};
struct ServiceItemMap
{
    ServiceItemMap() {}
    ServiceItemMap( const QString I ) : Id( I ) {}
    QString Id;
    QVector<ItemMenuMap> ItemMap;
};
struct PropertyServiceItemMap
{
    PropertyServiceItemMap() {}
    PropertyServiceItemMap( const QString I ) : Id( I ) {}
    QString Id;
    QVector<ServiceItemMap> ServiceMap;
};

class IdServiceActionMap
{
public:
    IdServiceActionMap();
    ~IdServiceActionMap();

public:
    void prepare( int Size );
    void addProperty( const QString& propertyId, int size );
    void addService( const QString& serviceId, int size );
    void addItem( int itemIndex, QAction* menuAction, int flag );

    bool remap( QString *PropertyId, int *ItemIndex, QString *ServiceId, QAction* menuAction ) const;
    QAction* menuAction( const QString& propertyId, int itemIndex, const QString& serviceId, int* flag = 0 ) const;

protected:
    QVector<PropertyServiceItemMap> PropertyMap;
};


inline IdServiceActionMap::IdServiceActionMap() {}
inline IdServiceActionMap::~IdServiceActionMap() {}

inline void IdServiceActionMap::prepare( int Size )
{ PropertyMap.clear(); PropertyMap.reserve( Size ); }

#endif
