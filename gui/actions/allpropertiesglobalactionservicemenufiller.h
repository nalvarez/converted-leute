/*
    This file is part of the Leute Gui library, part of the KDE project.

    Copyright 2006,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#ifndef ALLPROPERTIESDEFAULTACTIONSERVICEMENUFILLER_H
#define ALLPROPERTIESDEFAULTACTIONSERVICEMENUFILLER_H

// lib
#include "leutegui_export.h"
#include "idserviceactionmap.h"
// Leute core
#include "allpropertiesglobalactionserviceclient.h"
// KDE
#include <kabc/addressee.h>
// Qt
#include <QtCore/QObject>

class QMenu;

namespace Leute
{

class LEUTEGUI_EXPORT AllPropertiesGlobalActionServiceMenuFiller : public QObject, public AllPropertiesGlobalActionServiceClient
{
    Q_OBJECT
public:
    AllPropertiesGlobalActionServiceMenuFiller();
    virtual ~AllPropertiesGlobalActionServiceMenuFiller();

public:
    void set( const KABC::Addressee& person );
    void fillMenu( QMenu* menu );

public: // GlobalServiceClient interface
    virtual void onGlobalActionServiceSwitch( const QString& propertyId );
    virtual void onPropertyManagerChange();

public: // ServiceClient interface
    virtual const KABC::Addressee& person() const;
    virtual void onActionServiceStateChange( const PropertyActionService& service, int change,
                                             int itemIndex );

protected Q_SLOTS:
    void onActionSelection( QAction* action );
    void onMenuDestruction();

protected:
    KABC::Addressee mPerson;

    QMenu* mMenu;
    IdServiceActionMap mIdActionMap;
};


inline AllPropertiesGlobalActionServiceMenuFiller::AllPropertiesGlobalActionServiceMenuFiller()
  : mMenu( 0 )
{}

inline void AllPropertiesGlobalActionServiceMenuFiller::set( const KABC::Addressee& person )
{
    mPerson = person;
}

}

#endif
