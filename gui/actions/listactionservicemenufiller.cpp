/*
    This file is part of the Leute Gui library, part of the KDE project.

    Copyright 2006,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#include "listactionservicemenufiller.h"

// Leute core
#include <services.h>
#include <listactionservice.h>
// Qt
#include <QtGui/QMenu>


namespace Leute
{

ListActionServiceMenuFiller::ListActionServiceMenuFiller()
  : mMenu( 0 )
{}

void ListActionServiceMenuFiller::setContext( const char *C )
{
    mContext = QString::fromLatin1( C );
}

void ListActionServiceMenuFiller::setPersonList( const KABC::AddresseeList& L )
{
    mPersonList = L;
}

const KABC::AddresseeList& ListActionServiceMenuFiller::personList() const { return mPersonList; }


void ListActionServiceMenuFiller::onActionServiceStateChange( const ListActionService& service, int /*Change*/ )
{
    const QString& serviceId = service.id();

    // find menuid of action
    QAction* menuAction = mActionServiceIdMap.key( serviceId, 0 );

    if( menuAction != 0 )
    {
        ServiceAction action = service.action( mPersonList, 0 );

        const QString text = action.data( DisplayTextRole ).toString();
        const QIcon icon = action.data( DisplayIconRole ).value<QIcon>();
        const bool enabled = action.data( EnabledRole ).toBool();

        menuAction->setText( text );
        menuAction->setIcon( icon );
        menuAction->setEnabled( enabled );
    }
}

void ListActionServiceMenuFiller::fillMenu( QMenu* menu )
{
    mMenu = menu;
    mActionServiceIdMap.clear();

    connect( mMenu, SIGNAL(destroyed()), SLOT(onMenuDestruction()) );
    connect( mMenu, SIGNAL(triggered(QAction*)), SLOT(onActionSelection(QAction*)) );
    Services::self()->registerClient( this );

    const ServiceManager *Manager = &Services::self()->serviceManager();

    const ActionServiceList &Services = Manager->actionServices();

    for( ActionServiceList::ConstIterator ServiceIt = Services.begin(); ServiceIt != Services.end(); ++ServiceIt )
    {
        const ListActionService* listService = qobject_cast<ListActionService*>( *ServiceIt );
        if( !listService )
            continue;

        if( !listService->isAvailable() )
            ;// TODO: 
        else if( listService->supports(mPersonList) && listService->fitsIn(mContext) )
        {
            const ServiceAction action = listService->action( mPersonList, 0 );

            const QString text = action.data( DisplayTextRole ).toString();
            const QIcon icon = action.data( DisplayIconRole ).value<QIcon>();
            const bool enabled = action.data( EnabledRole ).toBool();

            QAction* menuAction = menu->addAction( icon, text );
            if( ! enabled )
                menuAction->setEnabled( false );
            mActionServiceIdMap[menuAction] = listService->id();
        }
    }
}

void ListActionServiceMenuFiller::onActionSelection( QAction* action )
{
   Services::self()->execute( mPersonList, mActionServiceIdMap[action] );
}

void ListActionServiceMenuFiller::onMenuDestruction()
{
    mMenu = 0;
    Services::self()->unregisterClient( this );
}

ListActionServiceMenuFiller::~ListActionServiceMenuFiller() {}

}
