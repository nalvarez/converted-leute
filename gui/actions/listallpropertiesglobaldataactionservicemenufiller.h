/*
    This file is part of the Leute Gui library, part of the KDE project.

    Copyright 2006,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/


#ifndef LISTALLPROPERTIESGLOBALDATAACTIONSERVICEMENUFILLER_H
#define LISTALLPROPERTIESGLOBALDATAACTIONSERVICEMENUFILLER_H


// lib
#include "leutegui_export.h"
#include "idserviceactionmap.h"
// leute core
#include "listallpropertiesglobaldataactionserviceclient.h"
// KDE
#include <kabc/addresseelist.h>
// Qt
#include <QtCore/QObject>
#include <QtCore/QStringList>

class QMenu;

namespace Leute
{

class LEUTEGUI_EXPORT ListAllPropertiesGlobalDataActionServiceMenuFiller
 : public QObject, public ListAllPropertiesGlobalDataActionServiceClient
{
    Q_OBJECT
public:
    ListAllPropertiesGlobalDataActionServiceMenuFiller();
    virtual ~ListAllPropertiesGlobalDataActionServiceMenuFiller();

public:
    void set( const KABC::AddresseeList& personList, const QMimeData* data );
    void fillMenu( QMenu* menu );

public: // GlobalDropServiceClient API
    virtual void onGlobalDataActionServiceSwitch( const QString& propertyId );
    virtual void onPropertyManagerChange();

public: // DropServiceClient API
    virtual const KABC::AddresseeList& personList() const;
    virtual const QMimeData* data() const;
//     virtual void onDataActionServiceStateChange( const PropertyDataActionService &Service, int Change,
//                                                  int ItemIndex );

protected Q_SLOTS:
    void onActionSelection( QAction* action );
    void onMenuDestruction();

protected:
    KABC::AddresseeList mPersonList;
    const QMimeData* mData;

    QMenu* mMenu;
    IdServiceActionMap mIdActionMap;
};


inline ListAllPropertiesGlobalDataActionServiceMenuFiller::ListAllPropertiesGlobalDataActionServiceMenuFiller()
: mMenu( 0 ) {}

inline void ListAllPropertiesGlobalDataActionServiceMenuFiller::set( const KABC::AddresseeList& personList,
                                                                     const QMimeData* data )
{
    mPersonList = personList;
    mData = data;
}

}

#endif
