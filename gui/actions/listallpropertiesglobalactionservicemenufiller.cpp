/*
    This file is part of the Leute Gui library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#include "listallpropertiesglobalactionservicemenufiller.h"

// Leute core
#include <services.h>
#include <propertyadapter.h>
#include <listpropertyactionservice.h>
// Qt
#include <QtGui/QMenu>


namespace Leute
{

const KABC::AddresseeList &ListAllPropertiesGlobalActionServiceMenuFiller::personList() const { return mPersonList; }

void ListAllPropertiesGlobalActionServiceMenuFiller::onGlobalActionServiceSwitch( const QString &/*PropertyId*/ ) { /*TODO: what to do?*/}
void ListAllPropertiesGlobalActionServiceMenuFiller::onPropertyManagerChange() { /*TODO: what to do?*/}
void ListAllPropertiesGlobalActionServiceMenuFiller::onActionServiceStateChange( const PropertyActionService &/*Service*/,
                                                                              int /*Change*/, int /*ItemIndex*/ )
{
#if 0
    const QString &ServiceId = Service.id();

    int MaxItemIndex;
    if( ItemIndex == -1 )
    {
        ItemIndex = 0;
        MaxItemIndex = Service.adapter()->numberOfItems( Person );
    }
    else
        MaxItemIndex = ItemIndex+1;

    for( ; ItemIndex<MaxItemIndex; ++ItemIndex )
    {
        // find menuid of action
        int Flags;
        int MenuId = IdActionMap.menuId( Service.adapter()->id(), ItemIndex, ServiceId, &Flags );

        if( MenuId != -1 )
        {
            // TODO: store (refer item) flag in IdActionMap
            ServiceAction Action = Service.action( Person, ItemIndex, Flags );

            QString Entry = Action.data( DisplayTextRole ).asString();
            QIconSet IconSet = Action.data( DisplayIconRole ).asIconSet();
            bool Enabled = Action.data( EnabledRole ).asBool();

            Menu->changeItem( MenuId, IconSet, Entry );
            Menu->setItemEnabled( MenuId, Enabled );
        }
    }
#endif
}

void ListAllPropertiesGlobalActionServiceMenuFiller::fillMenu( QMenu* menu )
{
    mMenu = menu;

    connect( mMenu, SIGNAL(aboutToHide()), SLOT(onMenuDestruction()) );
    connect( mMenu, SIGNAL(triggered(QAction*)), SLOT(onActionSelection(QAction*)) );
    Services::self()->registerClient( this );

    const PropertyManagerList &Managers = Services::self()->propertyManagers();
    mIdActionMap.prepare( Managers.size() );
    PropertyManagerList::ConstIterator ManagerIt = Managers.begin();
    for( ; ManagerIt != Managers.end(); ++ManagerIt )
    {
        const PropertyManager &Manager = **ManagerIt;

        const PropertyActionServiceList &Services = Manager.mainActionServices();
        if( Services.size() == 0 )
            continue;

        const PropertyAdapter *Adapter = Manager.propertyAdapter();
        if( !Adapter->haveProperty(mPersonList) )
            continue;

        mIdActionMap.addProperty( Adapter->id(), Services.size() );

        for( PropertyActionServiceList::ConstIterator ServiceIt = Services.begin(); ServiceIt != Services.end(); ++ServiceIt )
        {
            const ListPropertyActionService *listService =
                qobject_cast<ListPropertyActionService*>( *ServiceIt );
            if( !listService )
                continue;

            if( !listService->isAvailable() )
                ;// TODO: 
            else
            {
                if( !listService->supports(mPersonList) )
                    continue;

                const int flags = PropertyService::Always;

                mIdActionMap.addService( listService->id(), 1 );

                // insert service action entries
                const ServiceAction action = listService->action( mPersonList, flags );

                const QString text = action.data( DisplayTextRole ).toString();
                const QIcon icon = action.data( DisplayIconRole ).value<QIcon>();
                const bool enabled = action.data( EnabledRole ).toBool();

                QAction* menuAction = menu->addAction( icon, text );
                if( ! enabled )
                    menuAction->setEnabled( false );

                mIdActionMap.addItem( 0, menuAction, flags );
            }
        }
    }
}


void ListAllPropertiesGlobalActionServiceMenuFiller::onActionSelection( QAction* menuAction )
{
    QString propertyId;
    int itemIndex;
    QString serviceId;

    if( mIdActionMap.remap(&propertyId,&itemIndex,&serviceId, menuAction) )
        Services::self()->execute( mPersonList, propertyId, serviceId );
}

void ListAllPropertiesGlobalActionServiceMenuFiller::onMenuDestruction()
{
    mMenu = 0;
    Services::self()->unregisterClient( this );
}

ListAllPropertiesGlobalActionServiceMenuFiller::~ListAllPropertiesGlobalActionServiceMenuFiller() {}

}
