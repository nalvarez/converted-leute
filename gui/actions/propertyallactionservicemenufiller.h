/*
    This file is part of the Leute Gui library, part of the KDE project.

    Copyright 2006,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#ifndef PROPERTYALLACTIONSERVICEMENUFILLER_H
#define PROPERTYALLACTIONSERVICEMENUFILLER_H

// lib
#include "leutegui_export.h"
// Leute core
#include "propertyallactionserviceclient.h"
// KDE
#include <kabc/addressee.h>
// Qt
#include <QtCore/QObject>
#include <QtCore/QHash>
#include <QtCore/QStringList>

class QMenu;
class QAction;

namespace Leute
{

class LEUTEGUI_EXPORT PropertyAllActionServiceMenuFiller : public QObject, public PropertyAllActionServiceClient
{
    Q_OBJECT
public:
    PropertyAllActionServiceMenuFiller();
    virtual ~PropertyAllActionServiceMenuFiller();

public:
    void set( const KABC::Addressee& Person, const QString &PropertyId, int ItemIndex );

public:
    void fillMenu( QMenu* menu );

public: // AllServiceClient API
    virtual int itemIndex() const;
    virtual QString propertyId() const;
    virtual void onAllActionServiceChange();

public: // ServiceClient API
    virtual const KABC::Addressee &person() const;
    virtual void onActionServiceStateChange( const PropertyActionService &Service, int Change,
                                             int ItemIndex );

protected Q_SLOTS:
    void onActionSelection( QAction* action );
    void onMenuDestruction();

protected:
    KABC::Addressee mPerson;
    QString PropertyId;
    int mItemIndex;

    QMenu* mMenu;
    QHash<QAction*,QString> mActionServiceIdMap;
};

}

#endif
