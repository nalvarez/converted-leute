/*
    This file is part of the Leute Gui library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#include "propertyalldataactionservicemenufiller.h"

// Leute core
#include <services.h>
#include <propertyadapter.h>
// Qt
#include <QtGui/QMenu>


namespace Leute
{

PropertyAllDataActionServiceMenuFiller::PropertyAllDataActionServiceMenuFiller()
  :mMenu( 0 )
{}


void PropertyAllDataActionServiceMenuFiller::set( const KABC::Addressee& C, const QString &P, int I,
                                                  const QMimeData* data )
{
    mPerson = C; mPropertyId = P; mItemIndex = I; mData = data;
}

int PropertyAllDataActionServiceMenuFiller::itemIndex()     const              { return mItemIndex; }
QString PropertyAllDataActionServiceMenuFiller::propertyId() const             { return mPropertyId; }
const KABC::Addressee& PropertyAllDataActionServiceMenuFiller::person() const { return mPerson; }
const QMimeData* PropertyAllDataActionServiceMenuFiller::data() const        { return mData; }


void PropertyAllDataActionServiceMenuFiller::onAllDataActionServiceChange()
{ /*TODO: what to do? update menu*/}

void PropertyAllDataActionServiceMenuFiller::onDataActionServiceStateChange(
    const PropertyDataActionService &service, int /*Change*/, int /*ItemIndex*/ )
{
    const QString &serviceId = service.id();

    // find menuid of action
    QAction* menuAction = mActionServiceIdMap.key( serviceId, 0 );

    if( menuAction != 0 )
    {
        const ServiceAction action = service.action( mPerson, mItemIndex, mData, 0 );

        const QString text = action.data( DisplayTextRole ).toString();
        const QIcon icon = action.data( DisplayIconRole ).value<QIcon>();
        const bool enabled = action.data( EnabledRole ).toBool();

        menuAction->setText( text );
        menuAction->setIcon( icon );
        menuAction->setEnabled( enabled );
    }
}


void PropertyAllDataActionServiceMenuFiller::fillMenu( QMenu* menu )
{
    mMenu = menu;
    mActionServiceIdMap.clear();

    connect( mMenu, SIGNAL(aboutToHide()), SLOT(onMenuDestruction()) );
    connect( mMenu, SIGNAL(triggered(QAction*)), SLOT(onActionSelection(QAction*)) );
    Services::self()->registerClient( this );

    PropertyManager *Manager = Services::self()->propertyManagers()[mPropertyId];
    if( !Manager )
        return;

    const PropertyDataActionServiceList &Services = Manager->dataActionServices();

    for( PropertyDataActionServiceList::ConstIterator ServiceIt = Services.begin(); ServiceIt != Services.end(); ++ServiceIt )
    {
        const PropertyDataActionService* service = *ServiceIt;

        if( ! service->isAvailableFor(mData) )
            ;// TODO: 
        else if( service->supports(mData,mPerson,mItemIndex) )
        {
            const ServiceAction action = service->action( mPerson, mItemIndex, mData, 0 );

            const QString text = action.data( DisplayTextRole ).toString();
            const QIcon icon = action.data( DisplayIconRole ).value<QIcon>();
            const bool enabled = action.data( EnabledRole ).toBool();

            QAction* menuAction = mMenu->addAction( icon, text );
            if( ! enabled )
                menuAction->setEnabled( false );

            mActionServiceIdMap[menuAction] = service->id();
        }
    }
}

void PropertyAllDataActionServiceMenuFiller::onActionSelection( QAction* action )
{
    Services::self()->execute( mPerson, mPropertyId, mItemIndex, mActionServiceIdMap[action], mData );
}

void PropertyAllDataActionServiceMenuFiller::onMenuDestruction()
{
    Services::self()->unregisterClient( this );
}

PropertyAllDataActionServiceMenuFiller::~PropertyAllDataActionServiceMenuFiller() {}

}
