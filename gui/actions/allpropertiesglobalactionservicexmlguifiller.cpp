/*
    This file is part of the Leute Gui library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#include "allpropertiesglobalactionservicexmlguifiller.h"

// Leute core
#include <services.h>
#include <propertyadapter.h>
// KDE
#include <kxmlguiclient.h>
// Qt
#include <QtCore/QSignalMapper>


namespace Leute
{

AllPropertiesGlobalActionServiceXMLGUIFiller::AllPropertiesGlobalActionServiceXMLGUIFiller()
 : mClient( 0 ), mSignalMapper( new QSignalMapper(this) )
{
    connect( mSignalMapper, SIGNAL(mapped( int )), SLOT(onActionActivation( int )) );
}

const KABC::Addressee &AllPropertiesGlobalActionServiceXMLGUIFiller::person() const { return mPerson; }

void AllPropertiesGlobalActionServiceXMLGUIFiller::onGlobalActionServiceSwitch( const QString &/*PropertyId*/ ) { /*TODO: what to do?*/}
void AllPropertiesGlobalActionServiceXMLGUIFiller::onPropertyManagerChange() { /*TODO: what to do?*/}

void AllPropertiesGlobalActionServiceXMLGUIFiller::onActionServiceStateChange( const PropertyActionService& service,
                                                                              int /*Change*/, int itemIndex )
{
    const QString& serviceId = service.id();

    int lastItemIndex;
    if( itemIndex == -1 )
    {
        itemIndex = 0;
        lastItemIndex = service.adapter()->numberOfItems( mPerson );
    }
    else
        lastItemIndex = itemIndex+1;

    for( ; itemIndex<lastItemIndex; ++itemIndex )
    {
        // find menuid of action
        int flags;
        QAction* menuAction = IdActionMap.menuAction( service.adapter()->id(), itemIndex, serviceId, &flags );

        if( menuAction != 0 )
        {
            // TODO: store (refer item) flag in IdActionMap
            const ServiceAction action = service.action( mPerson, itemIndex, flags );

            const QString text = action.data( DisplayTextRole ).toString();
            const QIcon icon = action.data( DisplayIconRole ).value<QIcon>();
            const bool enabled = action.data( EnabledRole ).toBool();

            menuAction->setText( text );
            menuAction->setIcon( icon );
            menuAction->setEnabled( enabled );
            ServiceAction Action = service.action( mPerson, itemIndex, flags );

            QString Entry = Action.data( DisplayTextRole ).asString();
            QIconSet IconSet = Action.data( DisplayIconRole ).asIconSet();
            bool Enabled = Action.data( EnabledRole ).asBool();

            KAction *GUIAction = mActions.at( ActionId );

            GUIAction->setIconSet( IconSet );
            GUIAction->setText( Entry );
            GUIAction->setEnabled( Enabled );
        }
    }
}

void AllPropertiesGlobalActionServiceXMLGUIFiller::setGUIClient( KXMLGUIClient *C, const QString &LI )
{
    mClient = C;
    ListId = LI;

    if( mClient == 0 )
        Services::self()->unregisterClient( this );
}

void AllPropertiesGlobalActionServiceXMLGUIFiller::setPerson( const KABC::Addressee& C )
{
    Services::self()->unregisterClient( this );

    mPerson = C;

    Services::self()->registerClient( this );

    update();
}

void AllPropertiesGlobalActionServiceXMLGUIFiller::update()
{
    const PropertyManagerList &Managers = Services::self()->propertyManagers();

    mActions.clear();
    int ActionId = 0;
    IdActionMap.prepare( Managers.size() );
    PropertyManagerList::ConstIterator ManagerIt = Managers.begin();
    for( ; ManagerIt != Managers.end(); ++ManagerIt )
    {
        const PropertyManager &Manager = **ManagerIt;

        const PropertyActionServiceList &Services = Manager.mainActionServices();
        if( Services.size() == 0 )
            continue;

        const PropertyAdapter *Adapter = Manager.propertyAdapter();
        const int ItemsSize = Adapter->numberOfItems( mPerson );
        if( ItemsSize == 0 )
            continue;

        IdActionMap.addProperty( Adapter->id(), Services.size() );

        for( PropertyActionServiceList::ConstIterator ServiceIt = Services.begin(); ServiceIt != Services.end(); ++ServiceIt )
        {
            const PropertyActionService &Service = **ServiceIt;

            if( !Service.isAvailable() )
                ;// TODO: 
            else
            {
                // find if properties have to be referenced
                int SupportedItems = 0;
                for( int ItemIndex = 0; ItemIndex<ItemsSize; ++ItemIndex )
                    if( Service.supports(mPerson,ItemIndex) )
                        SupportedItems++;
                if( SupportedItems == 0 )
                    continue;

                int Flags = PropertyService::Always;
                if( SupportedItems > 1 )
                    Flags |= PropertyService::ReferItem;

                IdActionMap.addService( Service.id(), ItemsSize );

                // insert service action entries
                for( int ItemIndex = 0; ItemIndex<ItemsSize; ++ItemIndex )
                {
                    if( Service.supports(mPerson,ItemIndex) )
                    {
                        IdActionMap.addItem( ItemIndex, ActionId, Flags );

                        ServiceAction Action = Service.action( mPerson, ItemIndex, Flags );

                        QString Entry = Action.data( DisplayTextRole ).asString();
                        QIconSet IconSet = Action.data( DisplayIconRole ).asIconSet();
                        bool Enabled = Action.data( EnabledRole ).asBool();

                        KAction *GUIAction =
                            new KAction( Entry, IconSet, KShortcut(), mSignalMapper, SLOT(map()),
                                         (KActionCollection*)0, 0 );
                        GUIAction->setEnabled( Enabled );
                        mSignalMapper->setMapping( GUIAction, ActionId );
                        ++ActionId;
                        mActions.append( GUIAction );
                    }
                }
            }
        }
    }

    mClient->unplugActionList( ListId );
    mClient->plugActionList( ListId, mActions );
}

void AllPropertiesGlobalActionServiceXMLGUIFiller::onActionActivation( int ActionId )
{
    QString PropertyId;
    int ItemIndex;
    QString ServiceId;

    if( IdActionMap.remap(&PropertyId,&ItemIndex,&ServiceId, ActionId) )
        Services::self()->execute( mPerson, PropertyId, ItemIndex, ServiceId );
}

AllPropertiesGlobalActionServiceXMLGUIFiller::~AllPropertiesGlobalActionServiceXMLGUIFiller()
{
    qDeleteAll( mActions );
}

}
