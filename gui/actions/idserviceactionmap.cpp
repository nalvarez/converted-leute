/*
    This file is part of the Leute Gui library, part of the KDE project.

    Copyright 2006,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#include "idserviceactionmap.h"

// Qt
#include <QtCore/QString>


void IdServiceActionMap::addProperty( const QString &PropertyId, int Size )
{
    // previous entry used?
    if( PropertyMap.empty() || PropertyMap.last().ServiceMap.size() > 0 )
        // new entry
        PropertyMap.append( PropertyServiceItemMap(PropertyId) );
    else
        // reuse previous entry
        PropertyMap.last() = PropertyServiceItemMap( PropertyId );
    PropertyMap.last().ServiceMap.reserve( Size );
}

void IdServiceActionMap::addService( const QString& serviceId, int Size )
{
    // previous entry used?
    if( PropertyMap.last().ServiceMap.empty() || PropertyMap.last().ServiceMap.last().ItemMap.size() > 0 )
        // new entry
        PropertyMap.last().ServiceMap.append( ServiceItemMap(serviceId) );
    else
        // reuse previous entry
        PropertyMap.last().ServiceMap.last() = ServiceItemMap( serviceId );
    PropertyMap.last().ServiceMap.last().ItemMap.reserve( Size );
}

void IdServiceActionMap::addItem( int itemIndex, QAction* menuAction, int flag )
{
  PropertyMap.last().ServiceMap.last().ItemMap.append( ItemMenuMap(itemIndex,menuAction,flag) );
}

bool IdServiceActionMap::remap( QString *PropertyId, int *ItemIndex, QString *ServiceId, QAction* menuAction ) const
{
    bool Remapped = false;
    int p = PropertyMap.size()-1;
    // last not filled?
    if( PropertyMap[p].ServiceMap.size() == 0 )
        --p;
    for( ; p>=0 ; --p )
    {
        if( PropertyMap[p].ServiceMap.first().ItemMap.size() == 0 )
            continue;
        if( menuAction >= PropertyMap[p].ServiceMap.first().ItemMap.first().MenuAction )
        {
            int s = PropertyMap[p].ServiceMap.size()-1;
            // last not filled?
            if( PropertyMap[p].ServiceMap[s].ItemMap.size() == 0 )
                --s;
            for( ; s>=0 ; --s )
            {
                if( menuAction >= PropertyMap[p].ServiceMap[s].ItemMap.first().MenuAction )
                {
                    for( int i = PropertyMap[p].ServiceMap[s].ItemMap.size()-1; i>=0 ; --i )
                    {
                        if( menuAction == PropertyMap[p].ServiceMap[s].ItemMap[i].MenuAction )
                        {
                            *PropertyId = PropertyMap[p].Id;
                            *ItemIndex =  PropertyMap[p].ServiceMap[s].ItemMap[i].Index;
                            *ServiceId =  PropertyMap[p].ServiceMap[s].Id;
                            Remapped = true;
                            break;
                        }
                    }
                    break;
                }
            }
            break;
        }
    }
    return Remapped;
}


QAction* IdServiceActionMap::menuAction( const QString& propertyId, int itemIndex, const QString& serviceId,
                                         int* flag ) const
{
    QAction* result = 0;
    for( uint p=0; p<PropertyMap.size(); ++p )
        if( propertyId == PropertyMap[p].Id )
        {
            for( uint s=0; s<PropertyMap[p].ServiceMap.size(); ++s )
                if( serviceId == PropertyMap[p].ServiceMap[s].Id )
                {
                    for( uint i=0; i<PropertyMap[p].ServiceMap[s].ItemMap.size(); ++i )
                        if( itemIndex == PropertyMap[p].ServiceMap[s].ItemMap[i].Index )
                        {
                            result = PropertyMap[p].ServiceMap[s].ItemMap[i].MenuAction;
                            if( flag )
                                *flag = PropertyMap[p].ServiceMap[s].ItemMap[i].Flag;
                            break;
                        }
                }
        }

    return result;
}
