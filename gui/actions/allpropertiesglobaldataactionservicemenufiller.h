/*
    This file is part of the Leute Gui library, part of the KDE project.

    Copyright 2006,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#ifndef ALLPROPERTIESDEFAULTDATAACTIONSERVICEMENUFILLER_H
#define ALLPROPERTIESDEFAULTDATAACTIONSERVICEMENUFILLER_H

// lib
#include "leutegui_export.h"
#include "idserviceactionmap.h"
// Leute core
#include "allpropertiesglobaldataactionserviceclient.h"
// KDE
#include <kabc/addressee.h>
// Qt
#include <QtCore/QObject>

class QMenu;


namespace Leute
{

class LEUTEGUI_EXPORT AllPropertiesGlobalDataActionServiceMenuFiller : public QObject, public AllPropertiesGlobalDataActionServiceClient
{
    Q_OBJECT
public:
    AllPropertiesGlobalDataActionServiceMenuFiller();
    virtual ~AllPropertiesGlobalDataActionServiceMenuFiller();

public:
    void set( const KABC::Addressee& P, const QMimeData* data );
    void fillMenu( QMenu* menu );

public: // GlobalDropServiceClient API
    virtual void onGlobalDataActionServiceSwitch( const QString &PropertyId );
    virtual void onPropertyManagerChange();

public: // DropServiceClient API
    virtual const KABC::Addressee& person() const;
    virtual const QMimeData* data() const;
    virtual void onDataActionServiceStateChange( const PropertyDataActionService &Service, int Change,
                                                 int ItemIndex );

protected Q_SLOTS:
    void onActionSelection( QAction* action );
    void onMenuDestruction();

protected:
    KABC::Addressee mPerson;
    const QMimeData* mData;

    QMenu* mMenu;
    IdServiceActionMap mIdActionMap;
};


inline AllPropertiesGlobalDataActionServiceMenuFiller::AllPropertiesGlobalDataActionServiceMenuFiller()
: mMenu( 0 ) {}

inline void AllPropertiesGlobalDataActionServiceMenuFiller::set( const KABC::Addressee& person,
                                                                 const QMimeData* data )
{
    mPerson = person;
    mData = data;
}

}

#endif
