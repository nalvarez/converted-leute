/*
    This file is part of the Leute Gui library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#include "propertyallactionservicemenufiller.h"

// Leute core
#include <services.h>
#include <propertyadapter.h>
// Qt
#include <QtGui/QMenu>


namespace Leute
{

PropertyAllActionServiceMenuFiller::PropertyAllActionServiceMenuFiller()
: mMenu( 0 ) {}


void PropertyAllActionServiceMenuFiller::set( const KABC::Addressee& C, const QString &P, int I )
{
    mPerson = C; PropertyId = P; mItemIndex = I;
}

int PropertyAllActionServiceMenuFiller::itemIndex()     const              { return mItemIndex; }
QString PropertyAllActionServiceMenuFiller::propertyId() const             { return PropertyId; }
const KABC::Addressee& PropertyAllActionServiceMenuFiller::person() const { return mPerson; }


void PropertyAllActionServiceMenuFiller::onAllActionServiceChange()
{ /*TODO: what to do? update menu*/}

void PropertyAllActionServiceMenuFiller::onActionServiceStateChange( const PropertyActionService &service,
                                                                     int /*Change*/, int /*ItemIndex*/ )
{
    const QString& serviceId = service.id();

    // find menuid of action
    QAction* menuAction = mActionServiceIdMap.key( serviceId, 0 );

    if( menuAction != 0 )
    {
        const ServiceAction action = service.action( mPerson, mItemIndex, 0 );

        const QString text = action.data( DisplayTextRole ).toString();
        const QIcon icon = action.data( DisplayIconRole ).value<QIcon>();
        const bool enabled = action.data( EnabledRole ).toBool();

        menuAction->setText( text );
        menuAction->setIcon( icon );
        menuAction->setEnabled( enabled );
    }
}


void PropertyAllActionServiceMenuFiller::fillMenu( QMenu* menu )
{
    mMenu = menu;
    mActionServiceIdMap.clear();

    connect( mMenu, SIGNAL(destroyed()), SLOT(onMenuDestruction()) );
    connect( mMenu, SIGNAL(triggered(QAction*)), SLOT(onActionSelection(QAction*)) );
    Services::self()->registerClient( this );

    PropertyManager *Manager = Services::self()->propertyManagers()[PropertyId];
    if( !Manager )
        return;

    const PropertyActionServiceList &Services = Manager->actionServices();

    for( PropertyActionServiceList::ConstIterator ServiceIt = Services.begin(); ServiceIt != Services.end(); ++ServiceIt )
    {
        const PropertyActionService *service = *ServiceIt;

        if( !service->isAvailable() )
            ;// TODO: 
        else if( service->supports(mPerson,mItemIndex) )
        {
            ServiceAction action = service->action( mPerson, mItemIndex, 0 );

            const QString text = action.data( DisplayTextRole ).toString();
            const QIcon icon = action.data( DisplayIconRole ).value<QIcon>();
            const bool enabled = action.data( EnabledRole ).toBool();

            QAction* menuAction = menu->addAction( icon, text );
            if( ! enabled )
                menuAction->setEnabled( false );
            mActionServiceIdMap[menuAction] = service->id();
        }
    }
}

void PropertyAllActionServiceMenuFiller::onActionSelection( QAction* action )
{
    Services::self()->execute( mPerson, PropertyId, mItemIndex, mActionServiceIdMap[action] );
}

void PropertyAllActionServiceMenuFiller::onMenuDestruction()
{
    mMenu = 0;
    Services::self()->unregisterClient( this );
}

PropertyAllActionServiceMenuFiller::~PropertyAllActionServiceMenuFiller() {}

}
