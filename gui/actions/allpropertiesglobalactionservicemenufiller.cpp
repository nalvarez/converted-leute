/*
    This file is part of the Leute Gui library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#include "allpropertiesglobalactionservicemenufiller.h"

// leute
#include <services.h>
#include <propertyadapter.h>
// Qt
#include <QtGui/QMenu>


namespace Leute
{

const KABC::Addressee &AllPropertiesGlobalActionServiceMenuFiller::person() const { return mPerson; }

void AllPropertiesGlobalActionServiceMenuFiller::onGlobalActionServiceSwitch( const QString &/*PropertyId*/ ) { /*TODO: what to do?*/}
void AllPropertiesGlobalActionServiceMenuFiller::onPropertyManagerChange() { /*TODO: what to do?*/}

void AllPropertiesGlobalActionServiceMenuFiller::onActionServiceStateChange( const PropertyActionService &service,
                                                                              int /*Change*/, int itemIndex )
{
    const QString& serviceId = service.id();

    int lastItemIndex;
    if( itemIndex == -1 )
    {
        itemIndex = 0;
        lastItemIndex = service.adapter()->numberOfItems( mPerson );
    }
    else
        lastItemIndex = itemIndex+1;

    for( ; itemIndex<lastItemIndex; ++itemIndex )
    {
        // find menuid of action
        int flags;
        QAction* menuAction = mIdActionMap.menuAction( service.adapter()->id(), itemIndex, serviceId, &flags );

        if( menuAction != 0 )
        {
            // TODO: store (refer item) flag in IdActionMap
            const ServiceAction action = service.action( mPerson, itemIndex, flags );

            const QString text = action.data( DisplayTextRole ).toString();
            const QIcon icon = action.data( DisplayIconRole ).value<QIcon>();
            const bool enabled = action.data( EnabledRole ).toBool();

            menuAction->setText( text );
            menuAction->setIcon( icon );
            menuAction->setEnabled( enabled );
        }
    }
}


void AllPropertiesGlobalActionServiceMenuFiller::fillMenu( QMenu* menu )
{
    const PropertyManagerList &Managers = Services::self()->propertyManagers();

    mMenu = menu;
    connect( mMenu, SIGNAL(aboutToHide()), SLOT(onMenuDestruction()) );
    connect( mMenu, SIGNAL(triggered(QAction*)), SLOT(onActionSelection(QAction*)) );
    Services::self()->registerClient( this );

    mIdActionMap.prepare( Managers.size() );
    PropertyManagerList::ConstIterator ManagerIt = Managers.begin();
    for( ; ManagerIt != Managers.end(); ++ManagerIt )
    {
        const PropertyManager &Manager = **ManagerIt;

        const PropertyActionServiceList &Services = Manager.mainActionServices();
        if( Services.size() == 0 )
            continue;

        const PropertyAdapter *Adapter = Manager.propertyAdapter();
        const int ItemsSize = Adapter->numberOfItems( mPerson );
        if( ItemsSize == 0 )
            continue;

        mIdActionMap.addProperty( Adapter->id(), Services.size() );

        for( PropertyActionServiceList::ConstIterator ServiceIt = Services.begin(); ServiceIt != Services.end(); ++ServiceIt )
        {
            const PropertyActionService &Service = **ServiceIt;

            if( !Service.isAvailable() )
                ;// TODO: 
            else
            {
                // find if properties have to be referenced
                int SupportedItems = 0;
                for( int ItemIndex = 0; ItemIndex<ItemsSize; ++ItemIndex )
                    if( Service.supports(mPerson,ItemIndex) )
                        SupportedItems++;
                if( SupportedItems == 0 )
                    continue;

                int flags = PropertyService::Always;
                if( SupportedItems > 1 )
                    flags |= PropertyService::ReferItem;

                mIdActionMap.addService( Service.id(), ItemsSize );

                // insert service action entries
                for( int itemIndex = 0; itemIndex<ItemsSize; ++itemIndex )
                {
                    if( Service.supports(mPerson,itemIndex) )
                    {
                        const ServiceAction action = Service.action( mPerson, itemIndex, flags );

                        const QString text = action.data( DisplayTextRole ).toString();
                        const QIcon icon = action.data( DisplayIconRole ).value<QIcon>();
                        const bool enabled = action.data( EnabledRole ).toBool();

                        QAction* menuAction = menu->addAction( icon, text );
                        if( ! enabled )
                            menuAction->setEnabled( false );

                        mIdActionMap.addItem( itemIndex, menuAction, flags );
                    }
                }
            }
        }
    }
}


void AllPropertiesGlobalActionServiceMenuFiller::onActionSelection( QAction* action )
{
    QString propertyId;
    int itemIndex;
    QString serviceId;

    if( mIdActionMap.remap(&propertyId,&itemIndex,&serviceId, action) )
        Services::self()->execute( mPerson, propertyId, itemIndex, serviceId );
}

void AllPropertiesGlobalActionServiceMenuFiller::onMenuDestruction()
{
    mMenu = 0;
    Services::self()->unregisterClient( this );
}

AllPropertiesGlobalActionServiceMenuFiller::~AllPropertiesGlobalActionServiceMenuFiller() {}

}
