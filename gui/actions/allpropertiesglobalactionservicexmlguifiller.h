/*
    This file is part of the Leute Gui library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#ifndef ALLPROPERTIESDEFAULTACTIONSERVICEXMLGUIFILLER_H
#define ALLPROPERTIESDEFAULTACTIONSERVICEXMLGUIFILLER_H

// lib
#include "leutegui_export.h"
#include "idserviceactionmap.h"
// Leute core
#include "allpropertiesglobalactionserviceclient.h"
// KDE
#include <kabc/addressee.h>
#include <KAction>
// Qt
#include <QtCore/QObject>
#include <QtCore/QList>

class QSignalMapper;
class KXMLGUIClient;

namespace Leute
{

class LEUTEGUI_EXPORT AllPropertiesGlobalActionServiceXMLGUIFiller : public QObject, public AllPropertiesGlobalActionServiceClient
{
    Q_OBJECT
public:
    AllPropertiesGlobalActionServiceXMLGUIFiller();
    virtual ~AllPropertiesGlobalActionServiceXMLGUIFiller();

public:
    void setPerson( const KABC::Addressee& C );
    void setGUIClient( KXMLGUIClient *C, const QString &LI );
    void update();

public: // GlobalServiceClient interface
    virtual void onGlobalActionServiceSwitch( const QString &PropertyId );
    virtual void onPropertyManagerChange();

public: // ServiceClient interface
    virtual const KABC::Addressee& person() const;
    virtual void onActionServiceStateChange( const PropertyActionService &Service, int Change,
                                             int ItemIndex );

protected Q_SLOTS:
    void onActionActivation( int Id );

protected:
    KABC::Addressee mPerson;

    KXMLGUIClient* mClient;
    QString ListId;

    QList<KAction*> mActions;
    QSignalMapper* mSignalMapper;
    IdServiceActionMap IdActionMap;
};

}

#endif
