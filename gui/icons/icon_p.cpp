/*
    This file is part of the Leute Gui library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#include "icon_p.h"

// Leute core
#include <services.h>
#include <propertyadapter.h>
// Qt
#include <QtGui/QPainter>


namespace Leute
{

void IconPrivate::appendIcon( const QImage& _icon )
{
    QImage icon = _icon;

    if( icon.size() != mData.size() )
        icon = icon.scaled( mData.size(), Qt::KeepAspectRatio );

    const int DX = (mData.width()-icon.width()) / 2;
    const int DY = (mData.height()-icon.height()) / 2;

    QPainter painter( &mData );
    painter.drawImage( DX, DY, icon );
}

void IconPrivate::appendSymbol( const QImage& _symbol )
{
    ++mSymbolCount;

    QImage symbol = _symbol;
    if( symbol.size() != QSize(mSymbolSize,mSymbolSize) )
        symbol = symbol.scaled( mSymbolSize, mSymbolSize, Qt::KeepAspectRatio );

    QPainter painter( &mData );
    painter.drawImage( mData.width()-mSymbolSize, mData.height()-mSymbolCount*mSymbolSize, symbol );
}

void IconPrivate::fill( const KABC::Addressee& person )
{
    const PropertyManagerList& Managers = Services::self()->propertyManagers();

    PropertyManagerList::ConstIterator ManagerIt = Managers.constBegin();
    for( ; ManagerIt != Managers.constEnd(); ++ManagerIt )
    {
        const PropertyAdapter *Adapter = (*ManagerIt)->propertyAdapter();
        const PropertyStatusServiceList &StatusServices = (*ManagerIt)->statusServices();

        // skip entries without status
        if( StatusServices.size() == 0 )
            continue;

        const int itemSize = Adapter->numberOfItems( person );
        for( int itemIndex = 0; itemIndex<itemSize; ++itemIndex )
        {
            // get status
            QString Status;
            PropertyStatusServiceList::ConstIterator StatusServicesIt = StatusServices.begin();
            for( ; StatusServicesIt != StatusServices.end(); ++StatusServicesIt )
            {
                if( (*StatusServicesIt)->supports(person,itemIndex) )
                {
                    QImage symbol =
                        (*StatusServicesIt)->status( person, itemIndex, 0 ).data(DisplayIconRole).value<QImage>();
                    if( ! symbol.isNull() )
                        appendSymbol( symbol );
                }
            }
        }
    }
}

}
