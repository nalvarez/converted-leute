/*
    This file is part of the Leute Gui library, part of the KDE project.

    Copyright 2006,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#ifndef LEUTE_ICON_P_H
#define LEUTE_ICON_P_H

// lib
#include "icon.h"
// Qt
#include <QtGui/QPixmap>
#include <QtGui/QImage>


namespace Leute
{
static const int maxSymbolCount = 3;
static const int maxSymbolSize = 32;

class IconPrivate
{
public:
    explicit IconPrivate( int size );

public:
    QPixmap data() const;
    int symbolSize() const;

public:
    void appendIcon( const QImage& icon );
    void appendSymbol( const QImage& symbol );
    void fill( const KABC::Addressee& person );

protected:
    QImage mData;
    /** contains the number of added symbols */
    int mSymbolCount;
    int mSymbolSize;
};


inline IconPrivate::IconPrivate( int size )
  : mData( size, size, QImage::Format_ARGB32 ),
    mSymbolCount( 0 )
{
    mData.fill( 0 );

    mSymbolSize = size / maxSymbolCount;
    if( mSymbolSize > maxSymbolSize )
        mSymbolSize = maxSymbolSize;
}


inline QPixmap IconPrivate::data() const { return QPixmap::fromImage(mData); }
inline int IconPrivate::symbolSize() const { return mSymbolSize; }

}

#endif
