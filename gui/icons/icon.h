/*
    This file is part of the Leute Gui library, part of the KDE project.

    Copyright 2006,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#ifndef LEUTE_ICON_H
#define LEUTE_ICON_H

// lib
#include "leutegui_export.h"

namespace KABC { class Addressee; }
class QPixmap;
class QImage;


namespace Leute
{
class IconPrivate;

class LEUTEGUI_EXPORT Icon
{
public:
    explicit Icon( int size );
    ~Icon();

public:
    QPixmap data() const;
    int symbolSize() const;

public:
    void appendIcon( const QImage& icon );
    void appendSymbol( const QImage& symbol );
    void fill( const KABC::Addressee& person );

protected:
    IconPrivate* const d;
};



}

#endif
