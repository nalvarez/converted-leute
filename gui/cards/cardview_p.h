/*
    This file is part of the Leute Gui library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#ifndef LEUTE_CARDVIEW_P_H
#define LEUTE_CARDVIEW_P_H

// lib
#include "cardview.h"
// KDE
#include <kabc/addressee.h>


namespace Leute
{

class GlobalServiceClientCardProxy;
class PropertyAllActionServiceMenuFiller;
class PropertyAllDataActionServiceMenuFiller;
class AllPropertiesGlobalActionServiceMenuFiller;
class AllPropertiesGlobalDataActionServiceMenuFiller;
class StatusClientCardProxy;


class CardViewPrivate
{
public:
    explicit CardViewPrivate( CardView* parent );
    ~CardViewPrivate();

public:
    const KABC::Addressee& person() const;

public: // QTextBrowser API
    void setSource( const QUrl& source );

public:
    void setPerson( const KABC::Addressee& person );

    void updateView();

public: // QTextEdit API
    void contextMenuEvent( QContextMenuEvent* event );

public: // QAbstractScrollArea API
    void dragEnterEvent( QDragEnterEvent* event );
    void dragLeaveEvent( QDragLeaveEvent* event );
    void dragMoveEvent( QDragMoveEvent* event );
    void dropEvent( QDropEvent* event );

protected:
    CardView* q;

    KABC::Addressee mPerson;

    GlobalServiceClientCardProxy* mGlobalServicesProxy;

    PropertyAllActionServiceMenuFiller* mPropertyServices;
    AllPropertiesGlobalActionServiceMenuFiller* mServices;

    PropertyAllDataActionServiceMenuFiller* mPropertyDropServices;
    AllPropertiesGlobalDataActionServiceMenuFiller* mDropServices;

    StatusClientCardProxy* mStatusProxy;
};


inline const KABC::Addressee& CardViewPrivate::person() const { return mPerson; }

}

#endif
