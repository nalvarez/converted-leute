/*
    This file is part of the Leute Gui library, part of the KDE project.

    Copyright 2006,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#include "card.h"
#include "card_p.h"


namespace Leute
{

Card::Card()
  : d( new CardPrivate() )
{
}

QString Card::data() const { return d->data(); }

void Card::initiate()
{
    d->initiate();
}

void Card::finalize()
{
    d->finalize();
}

void Card::appendHeader( const QString& imageUrl, const QString& realName,
                         const QString& role, const QString& organizationName )
{
    d->appendHeader( imageUrl, realName, role, organizationName );
}


void Card::appendItem( const QString& name, const QString& entry, const QString& status )
{
    d->appendItem( name, entry, status );
}

Card::~Card() { delete d; }

}
