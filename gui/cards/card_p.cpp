/*
    This file is part of the Leute Gui library, part of the KDE project.

    Copyright 2006,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#include "card.h"
#include "card_p.h"


namespace Leute
{

void CardPrivate::initiate()
{
    mData = QString::fromLatin1(
      "<html><body>"
      "<div align=\"center\">"
        "<table cellpadding=\"1\" cellspacing=\"0\">" );
}


void CardPrivate::finalize()
{
    mData += QString::fromLatin1(
        "</table>"
      "</div>"
      "</body></html>" );
}


void CardPrivate::appendHeader( const QString& imageUrl, const QString& realName,
                                const QString& role, const QString& organizationName )
{
    const QString tableHeadTemplate = QString::fromLatin1(
        "<tr>"
          "<td align=\"right\" valign=\"top\">"
            "<img src=\"%1\" width=\"50\">"
          "</td>"
          "<td align=\"left\" valign=\"top\">"
            "<font size=\"+1\"><b>%2</b></font>%3"
          "</td>"
        "</tr>"
        "<tr><td>&nbsp;</td><td>&nbsp;</td></tr>" );
    const QString lineBreak = QString::fromLatin1( "<br>" );
    QString RO;
    if( ! role.isEmpty() )
        RO += lineBreak + role;
    if( ! organizationName.isEmpty() )
        RO += lineBreak + organizationName;

    mData.append( tableHeadTemplate.arg(imageUrl,realName,RO) );
}


void CardPrivate::appendItem( const QString& name, const QString& entry, const QString& status )
{
    const QString tableRowTemplate = QString::fromLatin1(
      "<tr>"
        "<td align=\"right\" valign=\"top\">"
          "<b>%1</b>"
        "</td>"
        "<td align=\"left\" valign=\"top\">"
          "%2"
        "</td>"
      "</tr>" );

    const QString statusEntry = status.isEmpty() ? entry :
         QString::fromLatin1( "%1<br><font size=\"-1\">%2</font>" ).arg( entry, status );

    mData.append( tableRowTemplate.arg(name,statusEntry) );
}

}
