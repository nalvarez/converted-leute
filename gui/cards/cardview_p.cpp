/*
    This file is part of the Leute Gui library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#include "cardview_p.h"

// lib
#include "card.h"
#include <propertyallactionservicemenufiller.h>
#include <propertyalldataactionservicemenufiller.h>
#include <allpropertiesglobalactionservicemenufiller.h>
#include <allpropertiesglobaldataactionservicemenufiller.h>
// Leute core
#include <services.h>
#include <propertyadapter.h>
#include <allpropertiesglobalactionserviceclient.h>
#include <statusserviceclient.h>
// KDE
#include <KLocale>
#include <KIconLoader>
#include <KGlobalSettings>
#include <KGlobal>
// Qt
#include <QtGui/QMenu>
#include <QtGui/QDragMoveEvent>
#include <QtGui/QDropEvent>
#include <QtGui/QDragEnterEvent>
#include <QtGui/QContextMenuEvent>


namespace Leute
{
static const int FaceWidth = 50;
static const int FaceHeight = 70;

class GlobalServiceClientCardProxy : public AllPropertiesGlobalActionServiceClient
{
public:
    GlobalServiceClientCardProxy( CardView* view ) : mView( view ) {}
    virtual ~GlobalServiceClientCardProxy() {}

public: // interface
    /** returns the person for which the services are requested */
    virtual const KABC::Addressee& person() const { return mView->person(); }

public: // slots interface
    /** called if the service switched */
    virtual void onActionServiceStateChange( const PropertyActionService &/*Service*/, int /*Change*/,
                                             int /*ItemIndex*/ ) { mView->updateView(); }
    virtual void onPropertyManagerChange() { mView->updateView(); }
    virtual void onGlobalActionServiceSwitch( const QString &/*PropertyId*/ ) { mView->updateView(); }

protected:
    CardView* mView;
};


class StatusClientCardProxy : public PropertyStatusServiceClient
{
public:
    StatusClientCardProxy( CardView* view ) : mView( view ) {}
    virtual ~StatusClientCardProxy() {}

public: // StatusClient interface
    virtual const KABC::Addressee &person() const { return mView->person(); }
    virtual void onStateChange( const PropertyStatusService &Service, const StatusChange &Change,
                                const Status &NewStatus, int ItemIndex );
    virtual void onPropertyManagerChange()         { mView->updateView(); }

protected:
    CardView* mView;
};

void StatusClientCardProxy::onStateChange( const PropertyStatusService &/*Service*/,
                                           const StatusChange &/*Change*/, const Status &/*NewStatus*/, int /*ItemIndex*/ )
{ mView->updateView(); }


CardViewPrivate::CardViewPrivate( CardView* parent )
  : q( parent ),
    mGlobalServicesProxy( new GlobalServiceClientCardProxy(parent) ),
    mPropertyServices( new PropertyAllActionServiceMenuFiller() ),
    mServices( new AllPropertiesGlobalActionServiceMenuFiller() ),
    mPropertyDropServices( new PropertyAllDataActionServiceMenuFiller() ),
    mDropServices( new AllPropertiesGlobalDataActionServiceMenuFiller() ),
    mStatusProxy( new StatusClientCardProxy(parent) )
{
    q->setWordWrapMode( QTextOption::WordWrap );
    //TODO: find replacement for setLinkUnderline( false );
    //setVScrollBarMode( QScrollView::AlwaysOff );
    //setHScrollBarMode( QScrollView::AlwaysOff );

    //styleSheet().item( "a" )->setColor( KGlobalSettings::linkColor() );
    q->setFrameStyle( QTextBrowser::NoFrame );

    q->setAcceptDrops( true );
}

void CardViewPrivate::setPerson( const KABC::Addressee& person )
{
    Services::self()->unregisterClient( mGlobalServicesProxy );
    Services::self()->unregisterClient( mStatusProxy );

    mPerson = person;

    Services::self()->registerClient( mStatusProxy );
    Services::self()->registerClient( mGlobalServicesProxy );

    q->updateView();
}


void CardViewPrivate::setSource( const QUrl& source )
{
    const QStringList L = source.toString().split( ':', QString::KeepEmptyParts );

    Services::self()->execute( mPerson, L[1], L[2].toInt(), L[0] );
}

void CardViewPrivate::contextMenuEvent( QContextMenuEvent* event )
{
    // for card content
    QMenu* menu = q->createStandardContextMenu();

    // add kde icon
//    int MenuId = menu->idAt( 0 );
//    menu->changeItem( MenuId, SmallIconSet("editcopy"), menu->text(MenuId) );

    menu->addSeparator();

    // add type services
    const QString link = q->anchorAt( event->pos() );

    if( ! link.isEmpty() )
    {
        const QStringList L = link.split( ':', QString::KeepEmptyParts );
        mPropertyServices->set( mPerson, L[1], L[2].toInt() );

        mPropertyServices->fillMenu( menu );
    }
    else
    {
        mServices->set( mPerson );
        mServices->fillMenu( menu );
    }

    menu->exec( event->globalPos() );

    delete menu;
}


static QString createLink( const QString &PropertyId, const QString &Text, int Id, const QString &ServiceId )
{
    QString Link = QString::fromLatin1( "%1:%2:%3" ).arg( ServiceId, PropertyId, QString::number(Id) );
    return QString::fromLatin1( "<a %1=\"%2\">%3</a>" ).arg(
        QString::fromLatin1(ServiceId.isEmpty()?"name":"href"), Link, Text );
}

static void fill( Card *Card, const KABC::Addressee &mPerson )
{
    const QString &UID = mPerson.uid();
    const PropertyManagerList &Managers = Services::self()->propertyManagers();

    PropertyManagerList::ConstIterator ManagerIt = Managers.begin();
    for( ; ManagerIt != Managers.end(); ++ManagerIt )
    {
        const PropertyAdapter *Adapter = (*ManagerIt)->propertyAdapter();
        const PropertyActionServiceList &ActionServices = (*ManagerIt)->mainActionServices();
        const PropertyStatusServiceList &StatusServices = (*ManagerIt)->statusServices();
        const QString &PropertyId = Adapter->id();

        const int ItemSize = Adapter->numberOfItems( mPerson );
        for( int ItemIndex = 0; ItemIndex<ItemSize; ++ItemIndex )
        {
            QString ItemId = QString::number( ItemIndex );
            // get data
            PropertyItem PropertyItem = Adapter->propertyItemOf( mPerson, ItemIndex );
            QString LabelText = PropertyItem.data( LabelTextRole ).toString();
            QString ItemText = PropertyItem.data( DisplayTextRole ).toString();

            // find first global action
            QString ActionServiceId;
#if 0
// TODO: before we searched for the first service that supported the item (better for fax or phone with number)
            if( ActionService->isAvailableFor(mPerson,ItemIndex) )
                ActionServiceId = ActionService->id();
#endif
            for( PropertyActionServiceList::ConstIterator ActionServiceIt = ActionServices.begin();
                 ActionServiceIt != ActionServices.end(); ++ActionServiceIt )
                if( (*ActionServiceIt)->supports(mPerson,ItemIndex) )
                {
                    // get id if it's available
                    if( (*ActionServiceIt)->isAvailableFor(mPerson,ItemIndex) )
                        ActionServiceId = (*ActionServiceIt)->id();
                    break;
                }

            ItemText = createLink( PropertyId, ItemText, ItemIndex, ActionServiceId );

            // get status
            QString StatusText;
            PropertyStatusServiceList::ConstIterator StatusServiceIt = StatusServices.begin();
            for( ; StatusServiceIt != StatusServices.end(); ++StatusServiceIt )
            {
                if( !(*StatusServiceIt)->supports(mPerson,ItemIndex) )
                    continue;

                Status Status = (*StatusServiceIt)->status( mPerson, ItemIndex, 0 );
                QString Text = Status.data( DisplayTextRole ).toString();
                QImage Icon = Status.data( DisplayIconRole ).value<QImage>();

                // enrich text with icon
                if( !Icon.isNull() )
                {
//                     QString IconId = QString::fromLatin1( "%1_tipicon_%2_%3_%4" )
//                       .arg( (*StatusServiceIt)->id(), UID, PropertyId, ItemId );
//                     QMimeSourceFactory::defaultFactory()->setImage( IconId, Icon );
//                     Text = QString::fromLatin1( "<img src=\"%1\">&nbsp;%2" ).arg( IconId, Text );
                }

                StatusText.append( Text );
            }

            // add to card
            Card->appendItem( LabelText, ItemText, StatusText );
        }
    }
}

void CardViewPrivate::updateView()
{
    QString text;

    if( ! mPerson.isEmpty() )
    {
        Card card;

        card.initiate();

        const QString imageURL = QString::fromLatin1( "person_image_%1" ).arg( mPerson.uid() );
        QVariant resource;
        KABC::Picture picture = mPerson.photo();
        if ( picture.isIntern() && !picture.data().isNull() ) // TODO: care for extern pictures
            resource = picture.data().scaled( FaceWidth, FaceHeight, Qt::KeepAspectRatio );
        else
            resource = KIconLoader::global()->loadIcon( "personal", KIconLoader::NoGroup, FaceWidth );

//         document->addResource( QTextDocument::ImageResource, imageUrl, resource );

        card.appendHeader( imageURL, mPerson.realName(), mPerson.role(), mPerson.organization() );

        fill( &card, mPerson );

        card.finalize();

        text = card.data();
    }

    q->setText( text );
}


void CardViewPrivate::dragEnterEvent( QDragEnterEvent *Event )
{
    //if( CardDropServices->canHandle(Event) )
        Event->accept();
}


void CardViewPrivate::dragLeaveEvent( QDragLeaveEvent */*Event*/ )
{
}


void CardViewPrivate::dragMoveEvent( QDragMoveEvent* event )
{
    bool isDropable;

    // update drop options
    const QString link = q->anchorAt( event->pos() );
    const QMimeData* data = event->mimeData();
    if( !link.isEmpty() )
    {
        const QStringList L = link.split( ':', QString::KeepEmptyParts );
        mPropertyDropServices->set( mPerson, L[1], L[2].toInt(), data );
        isDropable = mPropertyDropServices->serviceAvailableForData();
    }
    else
    {
        mDropServices->set( mPerson, data );
        isDropable = mDropServices->serviceAvailableForData();
    }

    event->setAccepted( isDropable );
}


void CardViewPrivate::dropEvent( QDropEvent* event )
{
    // for card content
    QMenu menu;

    // add type services
    const QString link = q->anchorAt( event->pos() );
    const QMimeData* data = event->mimeData();

    if( ! link.isEmpty() )
    {
        const QStringList L = link.split( ':', QString::KeepEmptyParts );
        mPropertyDropServices->set( mPerson, L[1], L[2].toInt(), data );
        mPropertyDropServices->fillMenu( &menu );
    }
    else
    {
        mDropServices->set( mPerson, data );
        mDropServices->fillMenu( &menu );
    }

    if( ! menu.isEmpty() )
    {
//         menu.insertSeparator();
//         menu.insertItem( SmallIcon("cancel"), i18n("&Cancel") );

        menu.exec( q->mapToGlobal(event->pos()) );
    }
}

CardViewPrivate::~CardViewPrivate()
{
    Services::self()->unregisterClient( mGlobalServicesProxy );
    Services::self()->unregisterClient( mStatusProxy );

    delete mGlobalServicesProxy;
    delete mPropertyServices;
    delete mPropertyDropServices;
    delete mServices;
    delete mDropServices;
    delete mStatusProxy;
}

}
