/*
    This file is part of the Leute Gui library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#include "cardview.h"
#include "cardview_p.h"


namespace Leute
{

CardView::CardView( QWidget* parent )
  : QTextBrowser( parent ),
    d( new CardViewPrivate(this) )
{
}

const KABC::Addressee& CardView::person() const { return d->person(); }

void CardView::setPerson( const KABC::Addressee& person )
{
    d->setPerson( person );
}

void CardView::updateView()
{
    d->updateView();
}

void CardView::setSource( const QUrl& source )
{
    d->setSource( source );
}

void CardView::contextMenuEvent( QContextMenuEvent* event )
{
    d->contextMenuEvent( event );
}

void CardView::dragEnterEvent( QDragEnterEvent* event )
{
    d->dragEnterEvent( event );
}

void CardView::dragLeaveEvent( QDragLeaveEvent* event )
{
    d->dragLeaveEvent( event );
}

void CardView::dragMoveEvent( QDragMoveEvent* event )
{
    d->dragMoveEvent( event );
}

void CardView::dropEvent( QDropEvent* event )
{
    d->dropEvent( event );
}

CardView::~CardView()
{
    delete d;
}

}
