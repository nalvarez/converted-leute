/*
    This file is part of the Leute Gui library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#ifndef LEUTE_CARDVIEW_H
#define LEUTE_CARDVIEW_H

// lib
#include "leutegui_export.h"
// Qt
#include <QtGui/QTextBrowser>

namespace KABC {
class Addressee;
}


namespace Leute
{
class CardViewPrivate;

class GlobalServiceClientCardProxy;
class PropertyAllActionServiceMenuFiller;
class PropertyAllDataActionServiceMenuFiller;
class AllPropertiesGlobalActionServiceMenuFiller;
class AllPropertiesGlobalDataActionServiceMenuFiller;
class StatusClientCardProxy;

/**
* View of a person with his properties and its' default services.
* Offers also access to all services for each property
*/
class LEUTEGUI_EXPORT CardView : public QTextBrowser
{
  Q_OBJECT

  friend class CardViewPrivate;

public:
    CardView( QWidget* parent = 0 );
    virtual ~CardView();

public: // QTextBrowser API
    virtual void setSource( const QUrl& source );

public:
    void setPerson( const KABC::Addressee& person );
    const KABC::Addressee& person() const;

public Q_SLOTS: // TODO: who uses this?
    virtual void updateView();

protected: // QWidget API
    virtual void contextMenuEvent( QContextMenuEvent* event );

protected: // QAbstractScrollArea API
    virtual void dragEnterEvent( QDragEnterEvent* event );
    virtual void dragLeaveEvent( QDragLeaveEvent* event );
    virtual void dragMoveEvent( QDragMoveEvent* event );
    virtual void dropEvent( QDropEvent* event );

protected:
    CardViewPrivate* const d;
};

}
#endif
