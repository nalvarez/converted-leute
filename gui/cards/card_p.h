/*
    This file is part of the Leute Gui library, part of the KDE project.

    Copyright 2006,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#ifndef LEUTE_CARD_P_H
#define LEUTE_CARD_P_H

// Qt
#include <QtCore/QString>


namespace Leute
{

class CardPrivate
{
public:
    CardPrivate();
    ~CardPrivate();

public:
    const QString& data() const;

public:
    void initiate();
    void appendHeader( const QString& imageUrl, const QString& realName,
                       const QString& role, const QString& organizationName );
    void appendItem( const QString& name, const QString& entry, const QString& status = QString() );
    void finalize();

protected:
    QString mData;
};


inline CardPrivate::CardPrivate()  {}
inline CardPrivate::~CardPrivate() {}

inline const QString& CardPrivate::data() const { return mData; }

}

#endif
