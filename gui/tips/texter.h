/*
    This file is part of the Leute Gui library, part of the KDE project.

    Copyright 2005,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#ifndef LEUTE_PERSON_H
#define LEUTE_PERSON_H

// lib
#include "leutegui_export.h"

namespace KABC { class Addressee; }
class QString;


namespace Leute
{

enum DescStyle
{
  ShowFullName = 1,
  ShowLoginName = 2,
  OnlyFullName = 1,
  OnlyLoginName = 2,
  FirstFullName = 3,
  FirstLoginName = 7,
  NameMask = 7,
  TwoRows = 8,
  ShowFace = 16,
  ShowFaceBefore = 16,
  ShowFaceBehind = 48,
  ShowFaceMask = 48,
};

class LEUTEGUI_EXPORT Texter
{
protected:
    Texter();
public:
    ~Texter();

public:
    static QString createImageUrl( const KABC::Addressee& person/*, int Size = 0*/ );
    static QString createTip( const KABC::Addressee& person, int Style = -1, bool complete = true );
//     static QString createTip( const KABC::Addressee& person, const QString& text, int style = -1, bool complete = true );
};

}

#endif
