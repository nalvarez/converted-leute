/*
    This file is part of the Leute Gui library, part of the KDE project.

    Copyright 2006,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#include "tip.h"
#include "tip_p.h"


namespace Leute
{

Tip::Tip()
  : d( new TipPrivate() )
{}

QString Tip::data() const { return d->data(); }

void Tip::initiate( bool complete )
{
    d->initiate( complete );
}

void Tip::appendHeader( const QString& imageURL, const QString& realName,
                       const QString& role, const QString& organizationName )
{
    d->appendHeader( imageURL, realName, role, organizationName );
}

void Tip::appendStatus( const QString& entry, const QString& status )
{
    d->appendStatus( entry, status );
}

void Tip::finalize( bool complete )
{
    d->finalize( complete );
}

Tip::~Tip() { delete d; }

}
