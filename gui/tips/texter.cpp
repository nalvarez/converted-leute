/*
    This file is part of the Leute Gui library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
*/

#include "texter.h"

// lib
#include "tip.h"
#include "person.h"
// Leute core
#include <services.h>
#include <propertyadapter.h>
// KDE
#include <kabc/addressee.h>
#include <kabc/stdaddressbook.h>
#include <kglobal.h>
#include <kiconloader.h>
// Qt
#include <QtGui/QImage>
// #include <qmime.h>
// #include <qdragobject.h>


namespace Leute
{
static const char PersonPictureProtocol[] = "personpicture";

#if 0
class PersonMimeSourceFactory : public QMimeSourceFactory
{
public:
  const QMimeSource *data( const QString &AbsName ) const;
};

const QMimeSource *PersonMimeSourceFactory::data( const QString &AbsName ) const
{
  // extract and decode arguments
  QStringList Arguments = QStringList::split( QChar(':'), AbsName );
  QImageDrag *Source = 0;

  if ( Arguments[0] == QString::fromLatin1(PersonPictureProtocol) )
  {
    if ( Arguments.size() >= 1 ) //2 )
    {
      KABC::Addressee Person = KABC::StdAddressBook::self()->findByUid( Arguments[1] );
      if( !Person.isEmpty() )
      {
    // get some icon to display in front
    KABC::Picture ABCPicture = Person.photo();
    if( ABCPicture.data().isNull() )
        ABCPicture = Person.logo();

    // any picture?
    if( ABCPicture.isIntern() && !ABCPicture.data().isNull() )
        Source = new QImageDrag( ABCPicture.data() ); // TODO: Size aus Arguments[2].toInt() lesen!
    else
        Source = new QImageDrag( KGlobal::iconLoader()->loadIcon("personal",KIcon::NoGroup,50).convertToImage() );

      }
    }
    else
      kdDebug() << QString::fromLatin1(PersonPictureProtocol) << ": insufficient information in AbsName: " << Arguments << endl;
  }

  return Source;
}
#endif

// static PersonMimeSourceFactory *personMimeSourceFactory = 0;

#if 0
Texter *Texter::self() {
  if( texter == 0 )
    personTexterDeleter.setObject( texter, new Texter() );
  return texter;
}
#endif
Texter::Texter() {}
Texter::~Texter() {}

QString Texter::createImageUrl( const KABC::Addressee& person/*, int Size*/ )
{
  // ensure facefactory
//   if( personMimeSourceFactory == 0 )
  {
//     personMimeSourceFactory = new PersonMimeSourceFactory;
//     QMimeSourceFactory::addFactory( personMimeSourceFactory );
  }

  return QString::fromLatin1(PersonPictureProtocol) + QChar(':') + person.uid();// + QChar(':') + QString::number(Size);
}

static const int DefaultStyle = ShowFullName | ShowFaceBehind /*| TwoRows*/;


/*
static QString createName( const KABC::Addressee &Person, int NameStyle, bool UseTwoRows )
{
  QString S;

  QString Name = NameStyle&ShowFullName ? User.fullName() : User.loginName();
  QString Other = NameStyle==FirstLoginName ? User.fullName() : NameStyle==FirstFullName ? User.loginName()
                  : QString::null;
  if( UseTwoRows )
    S += QString::fromLatin1( "<td><nobr>" );

  S += Name;

  if( !Other.isNull() )
  {
    if( UseTwoRows )
      S += QString::fromLatin1("<br>(");
    else
      S += QString::fromLatin1(" (" );
    S += Other;
    S += QChar( ')' );
  }

  if( UseTwoRows )
    S += QString::fromLatin1( "</nobr></td>" );
  return S;
}
*/
static void fill( Tip* tip, const KABC::Addressee& person )
{
    const QString &UID = person.uid();
    const PropertyManagerList &Managers = Services::self()->propertyManagers();

    PropertyManagerList::ConstIterator ManagerIt = Managers.begin();
    for( ; ManagerIt != Managers.end(); ++ManagerIt )
    {
        const PropertyAdapter *Adapter = (*ManagerIt)->propertyAdapter();
        const QString &PropertyId = Adapter->id();
        const PropertyStatusServiceList &StatusServices = (*ManagerIt)->statusServices();

        // skip entries without status
        if( StatusServices.size() == 0 )
            continue;

        const int ItemSize = Adapter->numberOfItems( person );
        for( int itemIndex = 0; itemIndex<ItemSize; ++itemIndex )
        {
            QString ItemId = QString::number( itemIndex );
            // get status
            QString statusText;
            PropertyStatusServiceList::ConstIterator StatusServicesIt = StatusServices.begin();
            for( ; StatusServicesIt != StatusServices.end(); ++StatusServicesIt )
            {
                if( (*StatusServicesIt)->supports(person,itemIndex) )
                {
                    const Status Status = (*StatusServicesIt)->status( person, itemIndex, 0 );
                    QString text = Status.data(DisplayTextRole).toString();
                    const QImage Icon = Status.data(DisplayIconRole).value<QImage>();

                    if( !Icon.isNull() )
                    {
                        QString iconId = QString::fromLatin1( "%1_tipicon_%2_%3_%4" )
                          .arg( (*StatusServicesIt)->id(), UID, PropertyId, ItemId );
//                         QMimeSourceFactory::defaultFactory()->setImage( IconId, Icon );
                        text = QString::fromLatin1( "<img src=\"%1\">&nbsp;%2" ).arg( iconId, text );
                    }

                    if( !text.isNull() )
                        statusText.append( text );
                }
            }

            // add to card
            if( !statusText.isEmpty() )
            {
                // get data
                PropertyItem PropertyItem = Adapter->propertyItemOf( person, itemIndex );
                const QString text = PropertyItem.data( DisplayTextRole ).toString();

                tip->appendStatus( text, statusText );
            }
        }
    }
}

QString Texter::createTip( const KABC::Addressee& person, int Style, bool Complete )
{
    if( Style < 0 )
        Style = DefaultStyle;

    const QString imageURL = createImageUrl( person );
    /*QString ImageURL = QString::fromLatin1( "person_image_%1" ).arg( Person.uid() );
    KABC::Picture Photo = Person.photo();
    if ( Photo.isIntern() && !Photo.data().isNull() ) // TODO: care for extern pictures
        QMimeSourceFactory::defaultFactory()->setImage( ImageURL, Photo.data() );
    else
        QMimeSourceFactory::defaultFactory()->setPixmap( ImageURL,
            KGlobal::iconLoader()->loadIcon("personal",KIcon::NoGroup,50) );
*/
    Tip tip;
    tip.initiate( Complete );

    tip.appendHeader( imageURL, person.realName(), person.role(), person.organization() );

    fill( &tip, person );

    tip.finalize( Complete );

    return tip.data();
}
#if 0
QString Texter::createTip( const KABC::Addressee& person, const QString& text, int Style, bool complete )
{
    const QString imageURL = createImageUrl( person );
    Tip tip;
    tip.initiate( complete );

    tip.appendHeader( imageURL, person.realName(), person.role(), person.organization() );

    tip.appendStatus( text );

    tip.finalize( complete );

    return tip.data();
}
#endif
}
